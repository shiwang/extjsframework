#!/usr/bin/python
# -*- coding: utf-8 -*-
# uwsgi --socket djangoframework.sock --module djangoframework.wsgi --chmod-socket=777 --master
# pep8 djangoframework/view_dir/auth/authfile.py
# autopep8 djangoframework/view_dir/auth/authfile.py  --recursive --in-place --pep8-passes 2000 --verbose
#ssh-keygen -t rsa
#cat .ssh/id_rsa.pub | ssh g1 \ "mkdir .ssh && chmod 0700 .ssh && cat > .ssh/authorized_keys2“

# git stash; git checkout 1.0029

from django.db import connections
from django.shortcuts import redirect
from django.shortcuts import render_to_response
from django.http import HttpResponseRedirect, HttpResponseNotFound, HttpResponse, HttpResponseBadRequest
from django.contrib import auth
from django.core.context_processors import csrf
from django.template import RequestContext, Context, loader
from django.contrib.auth.decorators import login_required
from pymongo import MongoClient
import MySQLdb as mdb
from sys import exit
from ast import literal_eval
import time
import re
import urllib2, cookielib
from django import http
from django import forms
from django.contrib.auth.models import User
from django.core.exceptions import PermissionDenied
from collections import OrderedDict
import json
import os
from djangoframework import __file__
import datetime
from datetime import timedelta
import tableModel
from django.views.decorators.csrf import csrf_exempt
from django.conf import settings
from zabbix_api import ZabbixAPI
from socket import socket
from logging import StreamHandler, DEBUG, getLogger, root, INFO, ERROR
from logging.handlers import RotatingFileHandler
from colorlog import ColoredFormatter
import pprint
#### defined modules
# contains the autherisation functions
from djangoframework.view_dir.auth import authfile
from djangoframework.view_dir.serverCheckUp import servercheckfile
from djangoframework.view_dir.table.getTableList import gettablelistfile
from djangoframework.view_dir.table.getTableData import getuserauthdata
from djangoframework.view_dir.table.getTableData import gettabledatafile
from djangoframework.view_dir.table.getTableData import addtablefeature
from djangoframework.view_dir.table.getTableData.httpRequestMethod import httprequestmethod
from djangoframework.view_dir.table.refillPanel import refillpanelfile

### Angularjs start
from oauth2_provider.decorators import protected_resource
import itertools
from rest_framework.renderers import JSONRenderer
from rest_framework.parsers import JSONParser
from models import *
from djangoframework.serializers import *
from rest_framework import viewsets
from rest_framework_mongoengine.generics import ListCreateAPIView, RetrieveUpdateDestroyAPIView
### Angularjs end

au = authfile.Auth()
sl = gettablelistfile.ServerList()
ud = getuserauthdata.UserAuthData()
getmodel = gettabledatafile.Table()
tabfeature = addtablefeature.GridFeature()
refp = refillpanelfile.RefillPanel()
httreq = httprequestmethod.HTTPRequestMethod()

LOGFORMAT = (
    "%(log_color)s%(levelname)s%(reset)s:"
    "%(bold_black)s%(name)s:%(reset)s%(message)s"
)

logger = getLogger('basicConfig')
if settings.DEBUG:
    logger.setLevel(DEBUG)
    formatter = ColoredFormatter(LOGFORMAT)
    stream = StreamHandler()
    stream.setFormatter(formatter)
    logger.addHandler(stream)
#    stream.setLevel(40)
else:
    formatter = ColoredFormatter(LOGFORMAT)
    handler = RotatingFileHandler(settings.LOGGER_FILENAME, maxBytes=2048,
                                      backupCount=5)
    logger.addHandler(handler)
    stream = StreamHandler()
    stream.setFormatter(formatter)
    logger.addHandler(stream)
    logger.setLevel(INFO)
    #logger.setFormat('%(asctime)s %(levelname)s %(message)s')
    #print('--> '+string)
def loggi(string):
    logger.info('--> ' + str(string))
def loggw(string):
    logger.warning('--> ' + str(string))

djangoRoot = os.path.dirname(os.path.realpath(__file__))

if settings.DEBUG:
    loggi(
        djangoRoot +
        "###################################### DJANGO DEVELOPMENT ###########################"
    )
else:
    loggi(
        djangoRoot +
        "###################################### DJANGO PRODUCTION ###########################"
    )

# the framework starts - changed comment to test, a new test 4
# now connection to mongodb and finding server in it

#tableModel.getTableModel("serverg1")

client = MongoClient('mongodb://localhost:27017/')
db = client['UserInfoMongoDB']
db_server = client['serverInfoMongoDB']
db_refill = client['balancePanelMongoDB']
db_tabProp = client['djangoGridTableProp']
db_chargeFromRemoteSettings = client['chargeFromRemoteSettings']
db_globalDjangoSettings = client['globalDjangoSettings']

#config_server = db_globalDjangoSettings.settings.find({"mysql": "history_server_all"})[0]
#con_with_g1 = mdb.connect('localhost', config_server['user'], config_server['pass'], 'history_server_all')
#cur_with_g1 = con_with_g1.cursor()

cur_with_g1 = connections['serverg1'].cursor()


# intial values
TotalRecordsStore=[0]
collectioninfo=[0, 0]
tablesOfDBStore = [0 for i in range(6)]
conStore = [0]
serverName = [0]
count = [1]
dublicate=[False]
id_row = [None]
body_row = [None]
user_row = [None]

refillTypeArr = []
for i in db_refill.refillServer.find():
    s = i["cardType"]
    mo = re.search(r'\d+',s)
    refillTypeArr.append(mo.group())

loggi( "refillTypeArr %s"%(refillTypeArr))

######### Zabbix
user_pass_zabbix = db_globalDjangoSettings['settings'].find()[0]
zapi = ZabbixAPI(server='http://localhost/zabbix', log_level=0)
try:
    zapi.login(user_pass_zabbix['zUser'], user_pass_zabbix['zPass'])
except:
    print "zabbix server is not reachable: %s" % ('localhost')

c = cookielib.CookieJar()
ch = urllib2.HTTPCookieProcessor(c)
op = urllib2.build_opener(ch)
r = op.open('http://localhost/zabbix/index.php',"name=%s&password=%s&enter=Sign+in" % (user_pass_zabbix['zUser'],user_pass_zabbix['zPass'] ))
########################################################### DONE


def login(request):# DONE
    loggi("func login")
    #au = authfile.Auth(request)
    return render_to_response('examples/desktop/login.html', au.login(request, csrf))

def logout(request):# DONE
    #auth.logout(request)
    loggi( "func logout:{}:".format(request.GET) )
    #au = authfile.Auth(request)
    au.logout(request, auth, db, datetime)
    return HttpResponseRedirect(settings.MIMPATH + '/login')

def auth_view(request):# DONE
    #au = authfile.Auth(request)
    loggi("func desktop")
    user=au.auth_view(request, auth, db, datetime)
    if user is not None:
        loggi( "redirecting to the path:"+settings.MIMPATH + '/desktop')
        return HttpResponseRedirect(settings.MIMPATH + '/desktop')
    else:
        return HttpResponse('Unauthorized', status=401)

@login_required
def desktop(request):
    loggi("function desktop(request):")
    #au = authfile.Auth(request)
    return render_to_response('examples/desktop/desktop.html', au.login(request, csrf))
###########################################################

####Angular starts here

### help functions
def dictfetchall(cursor):
    """Returns all rows from a cursor as a list of dicts"""
    desc = cursor.description
    return [dict(itertools.izip([col[0] for col in desc], row))
            for row in cursor.fetchall()]

class DateTimeEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, datetime.datetime):
            return obj.isoformat()
        elif isinstance(obj, datetime.date):
            return obj.isoformat()
        elif isinstance(obj, datetime.timedelta):
            return (datetime.datetime.min + obj).time().isoformat()
        else:
            return super(DateTimeEncoder, self).default(obj)
### /help functions
# Reload model of a specific server
# /worj/server/server_206/reloadModel
@login_required
def reloadModel(request, server_name):
    loggi("reload model of: " + server_name)
    tableModel.getTableModel(server_name)
    return HttpResponse(json.dumps({'data': 'done'}),
                        content_type='application/json')
# List all available server services
#/work/server_services
@protected_resource()
def server_services(request):
    username = request.resource_owner.username

        # getting mysql rights from mongdb
        #mysql_opts = db_server.posts.find({
            #'servername': server_name
        #})[0]

    # getting user authorization parameters
    server_services = db.server_services.find({
        'user':username,
    },{"_id":0,"services":"1"})
    if server_services.count() > 0:
        result=server_services[0]['services']
    else:
        result = []

    return HttpResponse(json.dumps({'data': list(set(result))}),
                        content_type='application/json')

# List all block servers
#/work/server_services/block_servers
@protected_resource()
def block_servers(request):
    username = request.resource_owner.username

        # getting mysql rights from mongdb
        #mysql_opts = db_server.posts.find({
            #'servername': server_name
        #})[0]

    # getting user authorization parameters
    block_servers = db.block_server.find({
        'user':username,
    },{"_id":0,"server":"1"})
    result = []
    for u in block_servers:
        result.append(u['server'])

    return HttpResponse(json.dumps({'data': list(set(result))}),
                        content_type='application/json')

# Get block_server table
#/work/server_services/block_servers/{block_server}
@protected_resource()
def block_server(request, block_server):
    username = request.resource_owner.username

        # getting mysql rights from mongdb
        #mysql_opts = db_server.posts.find({
            #'servername': server_name
        #})[0]

    # getting user authorization parameters
    block_servers = db.block_server.find({
        'user':username,'server': block_server})

    if block_servers.count() == 0:
        return HttpResponseNotFound('user: {} does not have rights for this server'.format(username))

    columns = []
    columns.append({'field':'number','displayName':'Number','width': '20%'})
    columns.append({'field':'count','displayName':'Count','width':'10%','sort': {'direction': 'desc'}})
    columns.append({'field':'calls','displayName':'Calls'})

    return HttpResponse(json.dumps({'data': getDataFrom_Block_Server(block_server,2),'columns':columns}),
                        content_type='application/json')

# Get all calls for a specific mobile number
#/work/server_services/block_servers/{block_server}/check-mobile/{mobileNumber}
@csrf_exempt
@protected_resource()
def check_mobile(request, block_server, mobileNumber):
    username = request.resource_owner.username

        # getting mysql rights from mongdb
        #mysql_opts = db_server.posts.find({
            #'servername': server_name
        #})[0]

    # getting user authorization parameters
    block_servers = db.block_server.find({
        'user':username,'server': block_server})

    if block_servers.count() == 0:
        return HttpResponseNotFound('user: {} does not have rights for this server'.format(username))

    if request.method == "GET":
        columns = []
        columns.append({'field':'start','displayName':'Date','width': '20%'})
        columns.append({'field':'billsec','displayName':'Duration','width':'10%','sort': {'direction': 'desc'}})
        columns.append({'field':'disposition','displayName':'Disposition'})

        cursor = connections[block_server].cursor()
        cursor.execute("""SELECT DATE_FORMAT(start,%s) start,billsec,disposition FROM cdr WHERE userfield LIKE %s""",('%Y-%m-%d',mobileNumber,))
        results = dictfetchall(cursor)
        return HttpResponse(json.dumps({'data': results,'columns':columns}),
                            content_type='application/json')
    elif request.method == "POST":
        try:
            cursor = connections[block_server].cursor()
            cursor.execute("""INSERT INTO block SET number=%s,time=%s""",(mobileNumber,datetime.datetime.now(),))
            return HttpResponse(json.dumps({'data': 'Mobile number '+mobileNumber+' has been blocked'}),
                content_type='application/json')
        except Exception, e:
            return HttpResponseNotFound(json.dumps({'error': str(e)}))

# List all billing servers
#/work/billing/
@protected_resource()
def billing(request):
    username = request.resource_owner.username

        # getting mysql rights from mongdb
        #mysql_opts = db_server.posts.find({
            #'servername': server_name
        #})[0]

    # getting user authorization parameters
    userBillingRights = db.billing.find({
        'user':username,
    },{"_id":0,"server":"1"})
    result = []
    for u in userBillingRights:
        result.append(u['server'])

    return HttpResponse(json.dumps({'data': list(set(result))}),
                        content_type='application/json')
#/work/billing/server_name
@protected_resource()
def billing_server(request,server_name):
    username = request.resource_owner.username
    dataToGo = []


    # getting user authorization parameters
    userBillingRights = db.billing.find({
        'user':username,
        'server':server_name
    })

    if userBillingRights.count() > 0:
        userBillingRights = userBillingRights[0]
    else:
        return HttpResponseNotFound('user: {} does not have rights for this server'.format(username))

    # getting mysql rights from mongdb
    mysql_opts = db_server.posts.find({
        'servername': server_name
    })[0]
    # connecting to my sql
    conN = mdb.connect(
        mysql_opts['host'],
        mysql_opts['user'],
        mysql_opts['pass'],
        mysql_opts['billing_db']
    )

    arrCodes = userBillingRights['codes']

    for p in xrange(int(userBillingRights['period'])):
        toDate = (datetime.date.today() - datetime.timedelta(days=p-1)).isoformat()
        fromDate = (datetime.date.today() - datetime.timedelta(days=p)).isoformat()

        dicVal = {}
        for code in arrCodes:

            with conN:
                curnz = conN.cursor()

                if code == 'all':
                    cmd = "SELECT sum(sessionbill)  \
                            FROM cc_call WHERE starttime >= '{}' \
                            AND starttime < '{}';".format(
                        fromDate + ' 00:00:00',
                        toDate + ' 00:00:00',
                    )
                else:
                    cmd = "SELECT sum(sessionbill)  \
                            FROM cc_call WHERE starttime >= '{}' \
                            AND starttime < '{}' AND \
                            calledstation LIKE '{}';".format(
                        fromDate + ' 00:00:00',
                        toDate + ' 00:00:00',
                        str(code)+'%'
                    )

                curnz.execute(cmd)
                vr = curnz.fetchall()
                #loggi(type(vr[0][0]) )
                if(vr[0][0] is None):
                    dicVal[code] = '0$'
                else:
                    dicVal[code] = str(int(vr[0][0])) + '$'

        dicVal['date'] = fromDate
        dataToGo.append(dicVal)
                # creating the table
        #loggi('dispval ->' + str(dicVal) )

    #loggi('dataToGo ->' + str(dataToGo) )

    conN.close()

    #except:
    #    pass
    columns = []
    columns.append({'field':'date','displayName':'Date','width': '20%'})
    for cd in arrCodes:
        columns.append({'field':cd,'displayName':cd})

    return HttpResponse(json.dumps({'data': dataToGo,"columns":columns}),
                        content_type='application/json')

####Angular ends here


# connection of the mysql database
######!!!!!!!!!!!!!!!!!!!!!!!!!!!! OUT
def mysQLTable(server):
    loggi("function mysQLTable(server):")
    mysql_opts = {}
    for c in server:
        for (k, v) in c.items():
            mysql_opts[k] = v

    loggi( "mysql_opts:{}".format(mysql_opts))
    try:
        con = mdb.connect(mysql_opts['host'], mysql_opts['user'],
                          mysql_opts['pass'], mysql_opts['db'])
        with con:
            cur = con.cursor()
            cur.execute('show tables;')
            vr = cur.fetchall()
            tablesOfDB = {0: 'Choose Table'}
            tablesOfDBtmp = ['Choose Table']
            for s, i in enumerate(vr):
                for cn in i:
                    tablesOfDB[s] = str(cn)
                    tablesOfDBtmp.append(str(cn))

            # print tablesOfDB
            return (tablesOfDB, tablesOfDBtmp, con)
    except mdb.Error, e:

        loggi('Error %d: %s' % (e.args[0], e.args[1]))
        exit(1)
# /work/server handler

@login_required
def servers(request): # DONE
    loggi("func servers")
    if request.method == 'GET':
        if request.GET.get('option') == 'withTableList':
            data = sl.get_servers_with_table_list(request, db)
            return HttpResponse(json.dumps({'data': data}),
                                content_type='application/json')
        else:
            data = sl.get_server_list(request, db)
            return HttpResponse(json.dumps({'data': data}),
                                content_type='application/json')
@login_required
def tables(request, server_name): # DONE

    return HttpResponse(
        json.dumps(
            {'data': sl.tables(request, server_name, db)}
            ),
        content_type='application/json'
    )

# /work/server/{server_name} handler
def internet_on(IP):
    #return True
    if IP == '10.1.0.1':
        return True
    s = socket()
    port = 4321
    try:
        s.settimeout(1)
        s.connect((IP, port))
        return True
    except Exception, e:
        loggi('internet_on %s:%d. Exception type is %s' % (IP, port, e))
        return False

def internet_on_special(IP):
    return True

    try:
        s = socket()
        port = 4321
        s.settimeout(1)
        s.connect((IP, port))
        return True
    except Exception, e:
        loggi('internet_on %s:%d. Exception type is %s' % (IP, port, e))
        return False

def internet_msql_on(IP):

    return True
    myq = socket()
    port = 3306
    loggi( "function internet_msql_on:")
    try:
        myq.settimeout(1)
        myq.connect((IP, port))
        return True
    except Exception, e:
        loggi('internet_msql_on %s:%d. Exception type is %s' % (IP, port, e))
        return False

@login_required
def hostip(request, server_name, table_name, hostip):

    give_IP_TO_nodeserver = db_server.posts.find({
        'servername': server_name
    })[0]['g_IP']
    try:
        aufladenip = db_refill.posts.find({'servername':server_name,
            'partner':db_server.posts.find({
                'servername': server_name})[0]['partner']
            })[0]['aufladenip']
    except IndexError as e:
        loggi( e )
        aufladenip = ''
    loggi( "IP:%s,username:%s"%(give_IP_TO_nodeserver, request.user.username))

    return HttpResponse(
                json.dumps({
                    'give_IP_TO_nodeserver':give_IP_TO_nodeserver,
                    'username':request.user.username,
                    'aufladenip':aufladenip
                }),
                content_type='application/json'
            )

@login_required
def server(request, server_name):
    loggi( server_name )
    return HttpResponse(server_name)

def get_time_for_display(timeStamp, hour_offset):
    start = datetime.datetime.fromtimestamp(float(timeStamp))

    print 'start hous {}'.format(start.hour)
    add_hrs = datetime.datetime(year=start.year,
                       month=start.month,
                       day=start.day,
                       hour=start.hour-hour_offset,
                       minute= start.minute)
    #mod_start = datetime.datetime(year=start.year,
    #                   month=start.month,
    #                   day=start.day,
    #                   hour=start.hour,
    #                   minute= start.minute)

    time_from=time.mktime(datetime.datetime.strptime(str(add_hrs), "%Y-%m-%d %H:%M:%S").timetuple())

    #print add_hrs
    return time_from
    #print mod_start
    #time_till=time.mktime(datetime.datetime.strptime(str(add_hrs), "%Y-%m-%d %H:%M:%S").timetuple())
    #return time_from, time_till

# get the data from the zabbix server
######!!!!!!!!!!!!!!!!!!!!!!!!!!!! OUT


def getDataZabbix(hostname, key, ncount_axis):

    itemid = zapi.item.get({"filter":{"key_":key, "host":hostname}})
    print "itemid {}".format(itemid)
    if len(itemid)==0:
        return 0
    #print "time now:{}".format(str(datetime.datetime.now()))
    #time_now = time.time()
    #time_from= get_time_for_display(time.time(), 1)
    #end_time = time.time()
    #print "time, get_time_for_display function: {}".format(end_time-start_time)

    #print time_from, time_till

    start_time = time.time()
    history = zapi.history.get({
        "history":str(itemid[0]["value_type"]),
        "itemids":str(itemid[0]["itemid"]),
        "sortorder": "DESC",
        "sortfield": "clock",
        #"output":"extend",
        "time_from": time.mktime((datetime.datetime.now()-timedelta(minutes=int(ncount_axis))).timetuple()),
        "time_till": time.time()})

    end_time = time.time()

    print "time, accesing zapi lib second time: {}".format(end_time-start_time)
    return history
######!!!!!!!!!!!!!!!!!!!!!!!!!!!! OUT
@login_required
def listOfGraphNames(request):

    if request.method == "GET":
        charts = db['charts'].find({"user":request.user.username})
        if charts.count() == 0:
            logger.warning("user: %s does not have any rights in charts collection",request.user.username)
            return HttpResponseNotFound('user: {} does not have any rights in charts collection'.format(request.user.username))
        chartsRights = charts[0]['chartRights']


        childArray = []
        for h in chartsRights:
            childdic = {}
            childdic['text'] = h
            childdic['expanded'] = True
            #childdic['checked'] = True
            childdic['children'] = []
            for k in chartsRights[h]:
                childdic['children'].append({'text':k, "leaf": True, "checked": False, "parentId":h})
            childArray.append(childdic)
        return HttpResponse(json.dumps({'children':childArray}),content_type='application/json')

@login_required
def chartsData(request):
    if request.method == "GET":
        # checking authorization
        charts = db['charts'].find({"user":request.user.username})
        if charts.count() == 0:
            logger.warning("user: %s does not have any rights in charts collection",request.user.username)
            return HttpResponseNotFound('user: {} does not have any rights in charts collection'.format(request.user.username))
        chartsRights = charts[0]['chartRights']
        server = request.GET.get("server")
        graphName = request.GET.get("graphName")
        if server is None or graphName is None:
            return HttpResponseBadRequest("parameter server or graphName is not defined")
        try:
            graph = zapi.graph.get({"filter":{"name":graphName, "host":server}})
        except Exception, e:
            logger.warning("zabbix instance zapi is not logged in trying to login, error: {}".format(e))
        try:
            zapi.login(user_pass_zabbix['zUser'], user_pass_zabbix['zPass'])
            graph = zapi.graph.get({"filter":{"name":graphName, "host":server}})
        except Exception, e:
            logger.error("tried to login again to zabbix and get the graph again but without success, error: {}".format(e))
        if len(graph) == 0:
            return HttpResponseBadRequest("graphName: {} is not defined in server: {}".format(graphName, server))
        graphId = graph[0]['graphid']
        stime = request.GET.get("stime",str(time.time()))
        period = request.GET.get("period","3600")
        zoom = request.GET.get("zoom","1")
        zoom_size = db_globalDjangoSettings["settings"].find()[0]['zabbix_zoom'][zoom]
        graphUrl = 'http://localhost/zabbix/chart2.php?graphid={}&width={}&height={}&period={}&stime={}'.format(graphId,int(zoom_size['width']),int(zoom_size['height']),period,stime)
        r = op.open(graphUrl)
        image_resp=r.read()
        if("You must login to view this page" in image_resp):
            # cookie timeout
            logger.warning("zabbix gui is logged out, trying to login again")
            r = op.open('http://localhost/zabbix/index.php',"name=%s&password=%s&enter=Sign+in" % (user_pass_zabbix['zUser'],user_pass_zabbix['zPass'] ))
            r = op.open(graphUrl)
            image_resp=r.read()
        return HttpResponse(image_resp, content_type="image/jpeg")
def zabbix(request, server_name, table_name):
    #loggi( request.GET )
    if table_name == "charts":
        dbZ = db.posts.find({'user': request.user.username,'server': server_name})[0]
        hostname = dbZ['server']
        #username = dbZ['zUser']
        #password = dbZ['zPass']
        loggi( "ICH HOLE CHARTS#########################################################")
        loggi( request.GET.get('option'))
        if request.method == 'GET' and request.GET.get('option') == 'childeren/root':
            loggi( "ich bin in childeren")
            childArray = []
            childdic = {}
            childdic['text'] = server_name
            childdic['expanded'] = True
            childdic['children'] = []
            for k in dbZ['chartsRights']:
                childdic['children'].append({'text':k, "leaf": True, "checked": False})

            childArray.append(childdic)
            loggi(childArray)
            return HttpResponse(json.dumps({'children':childArray}),content_type='application/json')

        elif request.method == 'GET' and request.GET.get('option') == 'data':

            if'url_values' in request.GET:
                loggi("charts have url_values="+request.GET.get('url_values'))
                #if(request.GET.get('url_values')=='20'):
                #    ncount_axis = 20
                #else:
                ncount_axis = request.GET.get('url_values')
            else:
                ncount_axis = 20

            loggi( "ich bin in data")
            objectsAp = []
            for k in dbZ['chartsRights']:

                key = dbZ['chartsRights'][k]
                start_time = time.time()
                zabbCall = getDataZabbix(hostname, key, ncount_axis)
                end_time = time.time()

                loggi("time take by getDataZabbix:{}".format(end_time-start_time))
                if zabbCall ==0:
                    print "itemid is empty"
                else:
                    zabbCall = zabbCall[::-1]
                #loggi('zabbCall:'+str(zabbCall))
                objectsAp.append({k:zabbCall})

            #for i in zabbCall:
            #    loggi(datetime.datetime.fromtimestamp(float(i['clock'])))
            loggi('length of zabbix data: ' + str(len(zabbCall)))
            chartsDict = []
            # restructering the keys
            start_time = time.time()
            for ix in  xrange(int(len(zabbCall))):
                chartsDictin ={}
                for iv in objectsAp:
                    if iv.keys()[0] == 'upcalls':
                        chartsDictin['clock'] = iv.values()[0][ix]['clock']
                        chartsDictin['upcalls'] = iv.values()[0][ix]['value']
                    # create
                    if iv.keys()[0] == 'ringcalls':
                        chartsDictin['ringcalls'] = iv.values()[0][ix]['value']
                    chartsDict.append(chartsDictin)
            end_time = time.time()
            loggi("time take by loop:{}".format(end_time-start_time))

            #print totalcalls
            return HttpResponse(json.dumps({'chartsDict': chartsDict}),content_type='application/json')
        else:
            return HttpResponse('',content_type='application/json')
    else:
        return HttpResponse('',content_type='application/json')
######!!!!!!!!!!!!!!!!!!!!!!!!!!!! OUT
def testIndex(request):
    #c = {}
    #c.update(csrf(request))
    #c['CSRF_TOKEN'] = csrf(request)['csrf_token']
    #response = render_to_response('nodeTest.html', c)
    #response.set_cookie('logged_in_status', 'never_use_this_ever')
    histr = zabbix()

    tmphistr = histr
    loggi( histr )
    cn = 1
    for ix, i in enumerate(tmphistr):
        if ix is 0:
            cn = i['ns']
            histr[ix]['ns'] = str(1)
        else:
            histr[ix]['ns'] = str(float(tmphistr[ix]['ns'])/float(cn))
    return HttpResponse(json.dumps({'history': histr[::-1]}),content_type='application/json')
"""
def testPhase(request):
    obj = refillPanel(refillval, refill_chargeVal)
    obj.refill(request, server_name, table_name, refillType)

"""

def checkServer(request, server_name, table_name, refillType):
    if table_name=='checkServer':
        loggi( "ich bin checkserver..." )
        checkServer = internet_on(
            db_server.posts.find({
                'servername': server_name
            })[0]['g_IP'])
        return HttpResponse(
            json.dumps({'checkServer': checkServer}),
            content_type='application/json'
        )

def refill(request, server_name, table_name, refillType):

    loggi( "function refill...")

    if request.GET.get('CHECHSERVERSTATUS')=='SERVERSTATUS':
        serverStatus =internet_on(
            db_server.posts.find({
                'servername': server_name
            })[0]['g_IP']
        )
        return HttpResponse(
            json.dumps({
                'serverStatus': serverStatus
            }),
            content_type='application/json'
        )
    if request.GET.get('USER_ACTIONS') == 'USER_ACTIONS':
        loggi('INSIDEEEEEEE')
        if True:#db.posts.find({'user': "shiwang",'server': "serverd190", "table":"cost"})[0]['rights']['user_Actions']:
            #cur_with_g1 = connections['serverg1'].cursor()
            cur_with_g1.execute("""INSERT INTO user_actions SET user=%s,action=%s, channel=%s, parentserver=%s, childserver=%s, time=%s""",(
                request.GET.get('username'),
                request.GET.get('action'),
                int(request.GET.get('sim')),
                '',
                request.GET.get('childserver'),
                datetime.datetime.now()
            ,))
            loggi("action:{}".format(request.GET.get('action')))
            if "transfer" in str(request.GET.get('action')):
                loggi('INSIDEEEEEEE_table : TRANSFER ')
                cur_with_g1.execute("""INSERT INTO user_transfer_details SET time=%s,user=%s, parentserver=%s, childserver=%s, From_SIM=%s, To_SIM=%s, amount=%s""",(
                    datetime.datetime.now(),
                    request.GET.get('username'),
                    request.GET.get('sim'),
                    '',
                    request.GET.get('childserver'),
                    request.GET.get('action').split(',')[1].split(':')[1],
                    request.GET.get('action').split(',')[3].split(':')[1]
                ,))
        return HttpResponse(
            json.dumps({}),
            content_type='application/json'
        )

    if request.GET.get('REFILLBALANCEWINDOW')=='CHARGE':
        loggi( "Charing from remote server" )
        checkserver = True #internet_on(db_server.posts.find({'servername': request.GET.get('remoteHostname')})[0]['g_IP'])
        if checkserver:
            cur_remote = connections[request.GET.get('remoteHostname')].cursor()
            if request.GET.get('remoteHostname') == "serverj140":
                loggi( "command: select number,cardid from cost where channel ={}".format("SIP/"+request.GET.get('remoteSIM')))
                cur_remote.execute("select number,cardid from cost where channel = %s", ("SIP/"+request.GET.get('remoteSIM')))
                result_NC = cur_remote.fetchone()
                loggi( result_NC )
                number = result_NC[0]
                receiver_cardid = result_NC[1]
                loggi( "number:{}".format(number))
                loggi( "number:{}".format(receiver_cardid))
                if len(number)<=5:
                    return HttpResponse(json.dumps({'message':'cannot refill, because the number has length 5'}), content_type='application/json')
                elif len(receiver_cardid)<=4:
                    return HttpResponse(json.dumps({'message':'cannot refill, because the cardid length is less than 5'}), content_type='application/json')
                else:
                    number = re.findall('\d+',number)[0]
                    return HttpResponse(json.dumps({'message':"ok", 'number':number, "receiver_cardid":receiver_cardid}), content_type='application/json')
            else:
                loggi(  "command: select number from cost where channel ={}".format("SIP/"+request.GET.get('remoteSIM')))
                cur_remote.execute("select number from cost where channel = %s", ("SIP/"+request.GET.get('remoteSIM'),))
                number = cur_remote.fetchone()[0]
                number = re.findall('\d+',number)
                if number:
                    number = number[0]
                if len(number)<=5:
                    if len(number)==0:
                        return HttpResponse(json.dumps({'message':'cannot refill, mobile number: has length: 0'}), content_type='application/json')
                    else:
                        return HttpResponse(json.dumps({'message':'cannot refill, mobile number:{} has length:'.format(str(number)) + str(len(number))}), content_type='application/json')
                else:
                    return HttpResponse(json.dumps({'message':"ok", 'number':number}), content_type='application/json')

        else:
             return HttpResponse(json.dumps({'message':'{} is down, cannot refill, try to resend'.format(request.GET.get('remoteHostname'))}), content_type='application/json')

    if (request.method == 'GET' and
        table_name != "charts" and
        request.GET.get('option')!='cmdKey' and
        request.GET.get('option')!='checkServer'
    ):# DONE

        dic = refp.refill_data(
            request,
            server_name,
            table_name,
            db, db_server,
            db_refill,
            refillType,
            connections,
            loggi
        )
        return HttpResponse(
            dic,
            content_type='application/json'
        )
        #return HttpResponse(json.dumps({'randKey':random.getrandbits(128), 'states':[{'name':tab} for tab in storeCurForRefill[0]]}), content_type='application/json')

    else:

        try:

            if request.GET.get('value') in refillTypeArr:

                loggi( "Getting the cardnumber -------- ")

                sabakey = request.GET.get('refillType') + request.GET.get('value');

                #saba = request.GET.get('refillType') + request.GET.get('value')
                partner = db_server.posts.find({'servername': server_name})[0]['partner']

                # find the refill charge server
                refillcharge_server_partner = db_refill.refillServer.find_one({'partner':partner, 'cardType': sabakey})

                loggi( "refillcharge_server_partner %s"%(refillcharge_server_partner['server']))


                dbU= db_server.posts.find( { "partner":partner, sabakey : { "$exists" : True } } )[0]
                loggi('table of cardnumber:{}'.format( dbU[sabakey]))

                refillcharge_server_partner_checkServer = internet_msql_on(
                    db_server.posts.find({'servername': refillcharge_server_partner['server']})[0]['host']
                )

                loggi( "refillcharge_server_partner_checkServer:%s"%(refillcharge_server_partner_checkServer))

                if refillcharge_server_partner_checkServer:
                    try:
                        curn = connections[dbU['servername']].cursor()
                        loggi('refillcharge_server_partner {}'.format(refillcharge_server_partner['server']))
                        loggi('dbu {}'.format(dbU['servername']))
                        #curn = connections[refillcharge_server_partner['server']].cursor() # added: 24/9/2014
                        #string = "select cardnumber from %s where used=0 order by id limit 1"%(dbU[sabakey])
                        #cardnumber= mysqlQuery(string, cur)
                        string = "select id,cardnumber from " + dbU[sabakey] + " where used=%s order by id limit %s"%(0, 1)
                        loggi('executing string->{}'.format(string))
                        curn.execute(string)
                        #print curn.fetchall()[0]
                        fetches = curn.fetchall()
                        idno, cardnumber= fetches[0]
                        if len(fetches) == 0:
                            idno='no_cardnumber'
                            cardnumber='no_cardnumber'

                        loggi('idno, cardnumber {}, {}'.format(idno, cardnumber))
                        loggi('no of cards in {} are: {}'.format(dbU[sabakey], len(fetches)))
                        string = "update %s set used=%s,channel=%s,user=%s,time_used=%s where id=%s"%(dbU[sabakey], 9,   request.GET.get('channel'), '"' + request.user.username + '"', '"'+str(datetime.datetime.now())+ '"',  idno)
                        #print string
                        curn.execute(string)
                    except:
                        idno='no_cardnumber'
                        cardnumber='no_cardnumber'
                else:
                    loggi( "Refill "+dbU['servername']+" down")
                    idno='no_cardnumber'
                    cardnumber='no_cardnumber'

                # UPDATE `asteriskcdrdb`.`saba_30` SET `used` = '9' WHERE `saba_30`.`id` =7207;
                return HttpResponse(json.dumps({
                    'crdNum':cardnumber,
                    'idno':idno,
                    'refillcharge_server_partner':{
                        'server_refillcharge_checkServer': refillcharge_server_partner_checkServer,
                        'server_refillcharge':refillcharge_server_partner['server']
                    }
                }), content_type='application/json')

        except Exception, e:

            loggi( "request.GET.get('value'), the value is not found");
            loggi( "ERORRRRRRRRRRRRRR: " + e);
            return HttpResponse(
                json.dumps({
                    'crdNum':'no_cardnumber'
                }),
                content_type='application/json'
            )

        try:
            if request.GET.get('value') == "YES":
                loggi(str(request.GET));
                loggi( "ich update masql table with  used=1");
                getRefillcardNo(request.GET.get('sabKey'), 1, request.GET.get('idno'), request.GET.get('channel'), request.user.username, server_name);
                return HttpResponse(json.dumps({'messageRefill':'you have set used=0 to used=1'}), content_type='application/json');

            if request.GET.get('value') == "NO":
                loggi( "ich update masql table with  used=3");
                getRefillcardNo(request.GET.get('sabKey'), 3, request.GET.get('idno'), request.GET.get('channel'), request.user.username, server_name);
                return HttpResponse(json.dumps({'messageRefill':'you have set used=0 to used=3'}), content_type='application/json');

            if request.GET.get('value') == "DONTKNOW":
                loggi( "ich update masql table with  used=8");
                getRefillcardNo(request.GET.get('sabKey'), 8, request.GET.get('idno'), request.GET.get('channel'), request.user.username, server_name);
                return HttpResponse(json.dumps({'messageRefill':'you have set used=0 to used=8'}), content_type='application/json')

            if request.GET.get('option')=='checkServer':
                loggi( "ich bin checkserver...");
                checkServer = internet_on(db_server.posts.find({'servername': server_name})[0]['g_IP']);
                mysqlcheckServer = internet_msql_on(db_server.posts.find({'servername': server_name})[0]['host']);
                return HttpResponse(json.dumps({'checkServer': checkServer, 'mysqlcheckServer':mysqlcheckServer}),content_type='application/json');
        except Exception, e:
            loggi( "YES, NO, DONTKNOW, problem")

# commented: 24/09/2014
def getRefillcardNo(sabKey, no, idno, channel, user, server_name):
        partner = db_server.posts.find({'servername': server_name})[0]['partner']
        dbU= db_server.posts.find( { "partner":partner, sabKey : { "$exists" : True } } )[0]
        curn = connections[dbU['servername']].cursor()
        string = "update %s set used=%s,channel=%s,user=%s,time_used=%s where id=%s"%(dbU[sabKey], no, channel,  '"' + user + '"',  '"' + str(datetime.datetime.now()) + '"', idno)
        curn.execute(string)



def getCommandDic(dic, username, server_name, table_name, refillType):
        dbpost = db.posts.find({'user': username,'server': server_name, 'table':table_name})[0]['rights'][refillType]
        dic['command'] = [{'name':tab.split('____')[1]} for tab in dbpost]
        dic['command_key'] = [{tab.split('____')[1]:tab.split('____')[0]} for tab in dbpost]
        loggi('dbpost:{}'.format(dbpost))
        dic['command_func'] = [{tab.split('____')[1]:tab.split('____')[2]} for tab in dbpost]
        return dic

def balanceRefill(request, server_name, table_name, storeObj):
    if request.method == 'GET':
        loggi( "balanceRefill objects inserted")
        conn = mdb.connect('localhost', 'testuser', 'test623', 'testdb')
        table_name = "refillBalance"
        curr = conn.cursor()
        array = request.GET.get('array').split('-')
        loggi('array: {}'.format(array))
        array[0] = int(array[0])

        try:
            with conn:
                if request.GET.get('opt')=='success':
                    loggi( 'ich bin here...success.')
                    array[2] = array[2].split('.')[0]
                    array[3] = float(array[3])
                    array[4] = bool(int(array[4]))
                    array[5] = bool(int(array[5]))
                    curr.execute("""insert into refillBalance (id, source, destination, amount, success, confirmed) values (%s, %s, %s, %s, %s, %s )""", array)
                if request.GET.get('opt')=='update':
                    loggi( 'ich bin here in balanceRefill update.')
                    curr.execute("UPDATE "+"refillBalance"+" SET " + 'confirmed=%s' + " WHERE id = %s", (True, array[0]))
        except mdb.Error:
            exit(1)

        #if request.GET.get('option')=='update':
        #    print 'ich bin here im update BAKHFUIS'
        #menu = {}
        #menu['items']=[]
        #menu['items'].append({'text':tab, 'handler':"handler"})
        #if request.GET.get('option')=='getRefill':


        return HttpResponse(json.dumps({}), content_type='application/json')

def mysqlQuery_off(string, cur):
    return dummyData()


@login_required
def table_put_delete(request, server_name, table_name, idProperty):
    loggi('table_put..........')
    cur = connections[server_name].cursor()
    if request.method == 'PUT':
        userTab = db_server.model.find({
            'server': server_name,
            'table': table_name})[0]
        dispval, Httpreq_string = httreq.PUT(
            request, server_name,
            table_name, cur, userTab['model'], loggi
        )
    if request.method == 'DELETE':
        dispval, Httpreq_string = httreq.DELETE(
            request,
            table_name,
            cur, loggi
        )
    if Httpreq_string == 'HttpResponse':
        return HttpResponse(
            dispval,
            content_type='application/json'
        )
    else:
        return HttpResponseBadRequest(
            dispval, content_type='application/json'
        )


def calls(request):
    if request.method == "GET":
        exten = request.GET.get("exten")
        accountcode = request.GET.get("accountcode")
        serverlist = db_globalDjangoSettings.settings.find({'access':'djangoSettings'})[0]['call_prefix']
        #serverlist={"11" : "serverg5" , "22" : "serverg3" , "33" : "serverg8"}
        call = db.call
        try:
            allowed_numbers = call.find({"user": accountcode } ,{"_id":0,"allowed_numbers":1})[0]['allowed_numbers']
            allowed_prefixes = call.find({"user": accountcode },{"_id":0,"allowed_prefixes":1})[0]['allowed_prefixes']
        except:
            return HttpResponse(
                json.dumps({'message':"block,"}),
                content_type='application/json'
            )
        numbers=[]
        for value in  allowed_numbers.itervalues():
            if(type(value) is list):
                value = [x.encode('UTF8') for x in value]
                numbers.extend(value)
            else:
                numbers.append(str(value))

        if(exten[5:] not in numbers):
            return HttpResponse(
                json.dumps({'message':"block,"}),
                content_type='application/json'
            )

        if(exten[0:2] not in allowed_prefixes):
            return HttpResponse(
                json.dumps({'message':"block,"}),
                content_type='application/json'
            )
        else:
            return HttpResponse(
                json.dumps({'message':"ok,"+serverlist[exten[0:2]]}),
                content_type='application/json'
            )
def dateParse(date):
    date = date.split('/')
    date = '-'.join([date[2], date[0], date[1]])
    return date

def userInsert(user, server, table, userLike):
    #user = 'arun'
    #server = 'serverd190'
    #table = 'cdr'
    #userLike = 'harpal'
    if server == 'Select Server':
        server = ''
    if table == 'Select Table':
        table = ''
    if userLike == 'Select a UserLike':
        userLike = ''
    if user and server and table and userLike:
        # check wether user is aviliable
        if db.posts.find({
                'user':user, 
                'server':server, 
                'table':table}, 
                {"_id" : 1}
            ).count() > 0:
            return '{} with {},{} is already inserted, \n Choose other options !!!'.format(user, server, table), False
        else:
            
            try:
                # find a document with userLike 
                docUserLike = db.posts.find({
                    'user':userLike, 
                    'server':server, 
                    'table':table}
                )[0]
                docUserLike['user'] = user
                del docUserLike['_id'] 
                db.posts.insert(docUserLike)
                return 'new user:{} with {},{} is inserted, DONE !!!'.format(user, server, table), True
            except:
                return 'no document for UserLike: {}'.format(userLike), False
    else:
        s = []
        s.append('please insert:')
        if not server:
            s.append('server,')
        if not table:
            s.append('table,')
        if not user:
            s.append('user,')
        if not userLike:
            s.append('userLike')
        return ' '.join(s), False
def oneUserDelete(user, server, table):
    # delete a user from mongodb
    #return 'lalala', True
    toRemoveUserDoc= db.posts.remove({
                    'user':user, 
                    'server':server, 
                    'table':table}
    )
    if toRemoveUserDoc['n'] > 0:
        return 'user:"{}" with {}, {} is successfully removed'.format(user, server, table), True
    else:
        return 'user:"{}" cannot be found'.format(user), False   
def userDelete(toRemoveUser):
    # delete a user from mongodb
    #return 'lalala', True
    toRemoveUserDoc= db.posts.remove({'user':toRemoveUser})
    if toRemoveUserDoc['n'] > 0:
        return 'user:"{}" is successfully removed'.format(toRemoveUser), True
    else:
        return 'user:"{}" cannot be found'.format(toRemoveUser), False   
def userInserALL(userToInsert, userLike):
    # delete a user from mongodb
    #return 'lalala', True
    if db.posts.find({
            'user':userToInsert}, 
            {"_id" : 1}
        ).count() > 0:
        return '{} is already inserted for all servers, \n Choose other options !!!'.format(userToInsert), False

    allUserDoc= db.posts.find({'user':userLike})
    newUserDoc = []
    if allUserDoc.count() > 0:
        for ix, key in enumerate(allUserDoc):
            indoc = {}
            for k in key:
                if k != '_id':
                    if k == 'user':
                        indoc[k] = userToInsert
                    else:
                        indoc[k] = key[k]
            newUserDoc.append(indoc)
        
        db.posts.insert(newUserDoc)
        return 'user:"{}" is successful integrated for all servers'.format(userToInsert), True
    else:
        return 'userLike:"{}" cannot be found'.format(userLike), False  
@login_required
def table(request, server_name, table_name):
    # clsmembers = inspect.getmembers(sys.modules[serversh1.__name__], inspect.isclass)
    # print clsmembers
    loggi('def table: {}'.format(str(request.GET)))

    authAdmin = ['shiwang', 'abdul7383', 'harpal']

    if(table_name=='cost'):
        checkServer = internet_on(
            db_server.posts.find({
                'servername': server_name
                })[0]['g_IP']
            )
    else:
        checkServer = True
    loggi(
        "inside the table with user: {}".format(
            request.user.username
        )
    )
    mysqlcheckServer = True
    #mysqlcheckServer = internet_msql_on(
    #    db_server.posts.find({
    #        'servername': server_name
    #        })[0]['host']
    #)

    if (table_name != "charts" and
        table_name != "checkServerMid" and
        checkServer and mysqlcheckServer
    ):
        #print "inside the table........."
        try:
            cur = connections[server_name].cursor()
        except Exception, e:
            return HttpResponseBadRequest(
                json.dumps({
                    'message': str(e)
                }),
                content_type='application/json'
            )
        if request.method == 'GET':
            if request.GET.get('USER_MANAGER') == 'USER_MANAGER' and (
                request.user.username in authAdmin):
                if request.GET.get('ALL') == 'ALL':
                    confirmation, conf_bool = userInserALL(
                        str(request.GET.get('user')), 
                        str(request.GET.get('selcUser')))
                    return HttpResponse(
                        json.dumps({
                            'confirmation': confirmation,
                            'conf_bool':conf_bool
                        }),
                        content_type='application/json'
                    )
                if request.GET.get('DELETE') == 'DELETE':
                    if request.user.username in authAdmin:
                        confirmation, conf_bool = userDelete(str(request.GET.get('selcUser')))
                    else:
                        confirmation = ' You cannot delete it, contact admin'
                        conf_bool = False
                    return HttpResponse(
                        json.dumps({
                            'confirmation': confirmation,
                            'conf_bool':conf_bool
                        }),
                        content_type='application/json'
                    )
                if request.GET.get('DELETEUSER') == 'DELETEUSER':
                    if request.user.username in authAdmin:
                        confirmation, conf_bool = oneUserDelete(
                            str(request.GET.get('user')), 
                            str(request.GET.get('selcServer')), 
                            str(request.GET.get('selcTable'))
                        )
                    else:
                        confirmation = ' You cannot delete User, contact admin'
                        conf_bool = False
                    return HttpResponse(
                        json.dumps({
                            'confirmation': confirmation,
                            'conf_bool':conf_bool
                        }),
                        content_type='application/json'
                    )
                if request.GET.get('INSERT') == 'INSERT':
                    if request.user.username in  authAdmin:    
                        confirmation, conf_bool = userInsert(
                            str(request.GET.get('user')), 
                            str(request.GET.get('selcServer')), 
                            str(request.GET.get('selcTable')), 
                            str(request.GET.get('selcUser'))
                        )
                    else:
                        confirmation = ' You cannot Insert, contact admin'
                        conf_bool = False
                    return HttpResponse(
                        json.dumps({
                            'confirmation': confirmation,
                            'conf_bool':conf_bool
                        }),
                        content_type='application/json'
                    )
                else:
                    # for MENU LIST
                    # LIST 1
                    mylist = []
                    for k in db_server.posts.find({'servername':{ "$exists": True } }):
                        mylist.append( k['servername'])
                    myset_server = list(set(mylist))
                    myset_server.sort()
                    # LIST 2
                    mylist = []
                    for k in db.posts.find({'table':{ "$exists": True } }):
                        mylist.append(k['table'])
                    myset_table = list(set(mylist))
                    myset_table.sort()
                    if request.user.username == 'harpal':
                        try:
                            myset_user= db.posts.find(
                                {'user':request.user.username, 'table':'user_actions'}
                            )[0]['rights']['users_group']
                            myset_user.remove('name')
                        except:
                            mylist = []
                            for k in db.posts.find({'user':{ "$exists": True } }):
                                mylist.append(k['user'])
                            myset_user = list(set(mylist))
                    if request.user.username == 'shiwang' or request.user.username == 'abdul7383':
                        mylist = []
                        for k in db.posts.find({'user':{ "$exists": True } }):
                            mylist.append(k['user'])
                        myset_user = list(set(mylist))
                    myset_user.sort()
                    return HttpResponse(
                        json.dumps({
                            'users':myset_user,
                            'servers':myset_server,
                            'tables':myset_table
                        }),
                        content_type='application/json'
                    )
            if request.GET.get('USER_ACTIONS_CHARTS') == 'USER_ACTIONS_CHARTS' :

                try:
                    allUsers= db.posts.find(
                        {'user':request.user.username, 'table':'user_actions'}
                    )[0]['rights']['users_group']

                except:
                    allUsers = ['name']
                    for u in User.objects.all():
                        allUsers.append(u.username)

                #loggi('users from django = {}'.format(User.objects.all()))
                loggi('date1:{}'.format(request.GET.get('date1')))
                loggi('date2:{}'.format(request.GET.get('date2')))
                #allUsers = ['name', 'salman', 'shiwang', 'abdul', 'vinay', 'harpal']
                dataUser = []
                #if server_name != 'user_actions':
                #serverOption = """ AND childserver LIKE '{}'""".format(server_name)
                #else:
                serverOption = ''

                for k in allUsers:
                    if k != 'name':
                        if not request.GET.get('date1') and not request.GET.get('date2'):
                            cur.execute("""SELECT COUNT( id ) FROM """ +  request.GET.get('table') +
                                """ WHERE user LIKE  %s """ + serverOption +
                                """ AND  time LIKE %s""",
                                (
                                    k,
                                    str(datetime.datetime.now().date())+'%'
                                ,)
                            )

                        if request.GET.get('date1') and request.GET.get('date2'):
                            cur.execute("""SELECT COUNT( id ) FROM """ +  request.GET.get('table') +
                                """ WHERE
                                user LIKE  %s """ + serverOption +
                                """ AND  time > %s and time < %s """,
                                (
                                    k,
                                    '{} {}'.format(
                                        str(request.GET.get('date1')),
                                        str(request.GET.get('time1'))
                                    ),
                                    '{} {}'.format(
                                        str(request.GET.get('date2')),
                                        str(request.GET.get('time2'))
                                    )
                                ,)
                            )

                        if request.GET.get('date1') and not request.GET.get('date2'):

                            cur.execute("""SELECT COUNT( id ) FROM """ +  request.GET.get('table') +
                                """ WHERE
                                user LIKE  %s """ + serverOption +
                                """ AND  time LIKE %s""",
                                (
                                    k,
                                    '{} {}'.format(
                                        str((request.GET.get('date1'))),
                                        str((request.GET.get('time1')))
                                    )
                                ,)
                            )
                        dataUser.append({'name':k, 'data1':cur.fetchone()[0]})
                loggi('alluser:{}, dataUser:{}'.format(allUsers,dataUser))
                return HttpResponse(
                    json.dumps({
                        'allUsers':allUsers,
                        'dataUser':dataUser
                    }),
                    content_type='application/json'
                )
            if request.GET.get('ACTIVE_7') == 'ACTIVE_7':
                if True:#db.posts.find({'user': username,'server': server_name, "table":table_name})[0]['rights']['user_Actions']:
                    loggi('ACTIVE_7-----------------------------------------------------------------')
                    cur.execute("""show columns from cdr""")
                    valarr=(datetime.datetime.now(),"123","{}-00000000".format(request.GET.get('sim')),2)
                    structure = cur.fetchall()
                    starr = [str(st[0])+'=%s'  for st in structure]
                    starr.pop(0)
                    for ix in xrange(len(starr)-len(valarr)):
                       valarr = valarr + ('',)
                    loggi("Describe cdr --->{}".format(','.join(starr)))
                    cur.execute("""INSERT INTO cdr SET {}""".format(','.join(starr)),valarr)
                return HttpResponse(
                    json.dumps({'message':'Active=7 Successful'}),
                    content_type='application/json'
                )
            if request.GET.get('USER_ACTIONS') == 'USER_ACTIONS':
                if True:#db.posts.find({'user': username,'server': server_name, "table":table_name})[0]['rights']['user_Actions']:
                    loggi('INSIDEEEEEEE_table')
                    parentserver = db_server.posts.find({'servername': request.GET.get('childserver')})[0]['parentserver']
                    cur_with_g1.execute("""INSERT INTO user_actions SET user=%s,action=%s,channel=%s, parentserver=%s, childserver=%s, time=%s""",(
                        request.GET.get('username'),
                        request.GET.get('action'),
                        int(request.GET.get('sim')),
                        parentserver,
                        request.GET.get('childserver'),
                        datetime.datetime.now()
                    ,))
                    loggi("action:{}".format(request.GET.get('action')))
                    if "transfer" in str(request.GET.get('action')):
                        loggi('INSIDEEEEEEE_table : TRANSFER ')
                        cur_with_g1.execute("""INSERT INTO user_transfer_details SET time=%s,user=%s,From_SIM=%s, To_SIM=%s, amount=%s""",(
                            datetime.datetime.now(),
                            request.GET.get('username'),
                            request.GET.get('sim'),
                            request.GET.get('action').split(',')[1].split(':')[1],
                            request.GET.get('action').split(',')[3].split(':')[1]
                        ,))

                return HttpResponse(
                    json.dumps({}),
                    content_type='application/json'
                )
            if request.GET.get('TOTALSERVERCALLS')=='TOTALSERVERCALLS':
                if server_name == 'servers20':
                    key = 'grpsum[["server_142", "server_131"],total_calls_servers20,last,0]'
                else:
                    key = 'total_calls_{}'.format(server_name)

                try:
                    parentserver = db_server.posts.find({'servername': server_name})[0]['parentserver']
                    itemid = zapi.item.get({"filter":{"key_":key, "host":parentserver}})
                    if len(itemid)==0:
                        return 0
                    history = zapi.history.get({
                        "history":str(itemid[0]["value_type"]),
                        "itemids":str(itemid[0]["itemid"]),
                        "sortorder": "DESC",
                        "sortfield": "clock",
                        #"output":"extend",
                        "time_from":time.mktime((datetime.datetime.now()-timedelta(seconds=30)).timetuple()),
                        "time_till": time.time()}
                    )
                    val = history[0]['value']
                except:
                    val = '-1'
                return HttpResponse(
                    json.dumps({'child_server_call_value':val}),
                    content_type='application/json'
                )
            # start the node server
            if request.GET.get('NODE')=='NODESERVER':
                #link = '/var/www/djangoframework/djangoframework/static/examples/desktop/node_modules/'
                #loggi('All the node are going to restarted, it they are off')
                strNode, nodeBool = servercheckfile.nodeServer_start(
                    'nodeServer{}'.format(request.GET.get('tableBelongnodePort')),
                    request.GET.get('tableBelongnodePort')
                )
                return HttpResponse(
                    json.dumps({'message':strNode}),
                    content_type='application/json'
                )
            if request.GET.get('CHANGE_SIM_STATUS')=='CHANGE_SIM_STATUS':

                loggi('CHANGE_SIM_STATUS: {}'.format(request.GET['value']))
                loggi(json.loads(request.GET['value']))
                obj = json.loads(request.GET['value'])

                obj = obj[0]
                del obj['id'][-1]
                del obj['number'][-1]
                if obj["toSetActive"] is 0:
                    for nb in list(set(obj["number"])):
                        arr = []
                        for s, number in enumerate(obj["number"]):
                            if number == nb:
                                arr.append(obj['id'][s])
                        if len(arr) == 1:
                            string = "UPDATE cost SET active={}, number='{}' WHERE Id={}".format(obj['toSetActive'], nb, arr[0])
                        else:
                            string = "UPDATE cost SET active={}, number='{}' WHERE Id IN {}".format(obj['toSetActive'], nb, tuple(arr))
                        #print string
                        #arr.append(obj['id'][obj['number'].index(number)])

                else:
                    if len(obj["number"]) is 1:
                        string = "UPDATE cost SET active={} WHERE Id={}".format(obj['toSetActive'], obj["id"][0])
                    else:
                        string = "UPDATE cost SET active={} WHERE Id IN {}".format(obj['toSetActive'], tuple(obj["id"]))

                loggi('string->' + string)
                #del obj[0]['id'][-1]
                #string = "UPDATE cost SET active={} WHERE Id IN {}".format(obj[0]['toSetActive'], tuple(obj[0]['id']))
                #print string
                cur.execute(string)

                return HttpResponse(
                    json.dumps({
                        'message': 'Change Active Cases: Completed'
                    }),
                    content_type='application/json'
                )
            if request.GET.get('GETSIMMFREE')=='GETSIMmree':
                # find sims of mfree less than 15 mysql

                string = "select channel, mfree from {} where mfree<{} ORDER BY mfree ASC".format(table_name, request.GET.get('value'))
                loggi("GETSIMMFREE request.GET:{}".format(string))
                cur.execute(string)
                mfree = []

                for row in cur.fetchall():
                    mfree.append({'channel':row[0], 'mfree':row[1]})

                return HttpResponse(
                    json.dumps({
                        'message': mfree
                    }),
                    content_type='application/json'
                )
            if request.GET.get('BLOCKEDSIM')=='BLOCKEDSIM':

                try:
                    parentserver = db_server.posts.find({'servername': server_name})[0]['parentserver']
                    loggi('paretserver of {} is {}'.format(server_name, parentserver))
                    curN = connections[parentserver].cursor()
                    loggi(db_globalDjangoSettings.settings.find({'access':'djangoSettings'})[0]['serviceType'][request.GET.get('serviceType')])
                    val = db_globalDjangoSettings.settings.find({'access':'djangoSettings'})[0]['serviceType'][request.GET.get('serviceType')]
                    curN.execute("""INSERT INTO """ + val  + """ SET channel=%s, time=%s, user=%s, cost=%s, subcost=%s, mfree=%s""",( str(request.GET.get('SIM')),datetime.datetime.now(),request.user.username,str(request.GET.get('cost')),str(request.GET.get('subcost')),str(request.GET.get('mfree'))))
                    return HttpResponse(
                        json.dumps({
                            'message':"SIM {} is set to blocked".format(request.GET.get('SIM'))
                        }),
                        content_type='application/json'
                    )
                except:
                    return HttpResponse(
                        json.dumps({
                            'message': "cannot store in parentserver, but sim is set to blocked"
                        }),
                        content_type='application/json'
                    )
            if request.GET.get('RESETOPTION')=='RESETactive':

                return HttpResponse(
                    tabfeature.reset_active_button(
                        server_name,
                        table_name,
                        cur, loggi, json
                    ),
                    content_type='application/json'
                )

            if request.GET.get('BILLSECOPTION')=='billsecQuery':

                loggi('BILLSECOPTION SIM --->' + request.GET.get('value'))
                #return HttpResponse('')

                return HttpResponse(
                    tabfeature.billsec_button(
                        request, server_name,
                        table_name, cur,
                        loggi, json
                    ),
                    content_type='application/json'
                )
            if request.GET.get('BILLSECOPTIONCOST')=='BILLSECOPTIONCOST':

                loggi('BILLSECOPTION COST SIM --->' + request.GET.get('value'))
                #return HttpResponse('')

                return HttpResponse(
                    tabfeature.billsec_button_cost(
                        request, server_name,
                        table_name, cur,
                        loggi, json
                    ),
                    content_type='application/json'
                )

            # getting the values of Col Form in grid
            if request.GET.get('COMBOBOXOPTION')=='comboBox':
                return HttpResponse('', content_type='application/json')

            if request.GET.get('USER_HIST')=='USER_HISTORY':

                loggi('USER_HISTORY-------------------')
                server_name = 'serverg1'
                table_name = "history_users_active"
                cur = connections[server_name].cursor()
                data, TotalRecords = tabfeature.mysqlQuery_serverg1(
                    request, table_name, cur, loggi
                )
                return HttpResponse(
                    json.dumps({
                        'data': data,'totalCount': TotalRecords
                    }),
                    content_type='application/json'
                )
            if request.GET.get('option') == 'getModel':
                loggi( "iI am getting Model.........")
                tableData = getmodel.create_table(
                    request,
                    server_name,
                    table_name,
                    ud,
                    db,
                    db_server,
                    db_refill,
                    db_tabProp,
                    db_chargeFromRemoteSettings,
                    db_globalDjangoSettings,
                    loggi,
                    checkServer,
                    json,
                    settings
                )

                return HttpResponse(tableData, content_type='application/json')

            if (request.GET.get('DUPLICATEOPTION') != 'dublicate' and
                request.GET.get('option') != 'getModel' and
                request.GET.get('RESETOPTION')!='RESETactive' and
                request.GET.get('BILLSECOPTION')!='billsecQuery' and
                request.GET.get('USER_HIST')!='USER_HISTORY'
            ):

                if (request.GET.get('sort') is not None and
                    request.GET.get('filter') is None
                ):
                    data, TotalRecords = tabfeature.sort_icon(
                        request, table_name, cur, loggi
                    )
                else:
                    data, TotalRecords = tabfeature.filter_menu(
                        request, table_name, cur, loggi
                    )

                #if type(data) is str:
                #    return HttpResponseBadRequest(
                #        json.dumps({'message': 'lost connection to mysql server'}),
                #        content_type='application/json'
                #)

                return HttpResponse(
                    json.dumps({
                        'data': data,'totalCount': TotalRecords
                    }),
                    content_type='application/json'
                )


            # E 3/5/2014
        if request.method == 'POST':
            loggi( "POST: {}".format(request.body))
            #body_row[0] = json.loads(request.body)
            loggi(
                "++++Ich habe post bekommen, aber ich gehe weg ++++"
            )
            return HttpResponse('', content_type='application/json')
            # update


    else:
        return HttpResponse(
            json.dumps({
                'checkServer':checkServer,
                'mysqlcheckServer':mysqlcheckServer
            }),
            content_type='application/json'
        )

# @login_required
# def table_user_history():
#     """
#     Access the user data from serverg1
#     """
#     loggi('def table_user_history')
#     server_name = 'serverg1'
#     table_name = "history_users_active"
#     cur = connections[server_name].cursor()
#     data, TotalRecords = tabfeature.mysqlQuery_serverg1(
#         request, table_name, cur, loggi
#     )
#     return HttpResponse(
#         json.dumps({
#             'data': data,'totalCount': TotalRecords
#         }),
#         content_type='application/json'
#     )
def dummyData():
    #data = [{"mfree": "226", "ASR40": "25.0", "ACD60": "4.8", "y": "0", "subcost": "-195", "lastcheck": "2013-12-20 19:18:01+00:00", "ymobile": "0", "ASR20": "10.0", "number": "m738413972", "ACD20": "3.9", "ACD40": "5.6", "proper": "0", "cost": "-195", "mtn": "1", "active": "1", "saba": "0", "id": "1", "channel": "SIP/900", "ASR60": "26.7"}, {"mfree": "289", "ASR40": "42.5", "ACD60": "5.6", "y": "0", "subcost": "-195", "lastcheck": "2013-12-20 19:18:02+00:00", "ymobile": "0", "ASR20": "20.0", "number": "m738352800", "ACD20": "5.3", "ACD40": "4.7", "proper": "0", "cost": "-195", "mtn": "1", "active": "1", "saba": "0", "id": "2", "channel": "SIP/901", "ASR60": "45.0"}, {"mfree": "219", "ASR40": "30.0", "ACD60": "3.7", "y": "0", "subcost": "-179", "lastcheck": "2013-12-20 19:18:04+00:00", "ymobile": "0", "ASR20": "20.0", "number": "m738253894", "ACD20": "3.4", "ACD40": "2.7", "proper": "0", "cost": "-179", "mtn": "1", "active": "1", "saba": "0", "id": "3", "channel": "SIP/902", "ASR60": "33.3"}, {"mfree": "192", "ASR40": "25.0", "ACD60": "6.2", "y": "0", "subcost": "-155", "lastcheck": "2013-12-20 19:19:07+00:00", "ymobile": "0", "ASR20": "20.0", "number": "m736917686", "ACD20": "3.2", "ACD40": "4.6", "proper": "0", "cost": "-155", "mtn": "1", "active": "1", "saba": "0", "id": "4", "channel": "SIP/903", "ASR60": "35.0"}, {"mfree": "153", "ASR40": "32.5", "ACD60": "6.3", "y": "0", "subcost": "-375", "lastcheck": "2013-12-20 19:19:08+00:00", "ymobile": "0", "ASR20": "25.0", "number": "m737674849", "ACD20": "4.1", "ACD40": "3.0", "proper": "0", "cost": "-375", "mtn": "1", "active": "1", "saba": "0", "id": "5", "channel": "SIP/904", "ASR60": "35.0"}, {"mfree": "214", "ASR40": "20.0", "ACD60": "5.2", "y": "0", "subcost": "-215", "lastcheck": "2013-12-20 19:19:09+00:00", "ymobile": "0", "ASR20": "10.0", "number": "m738491147", "ACD20": "3.2", "ACD40": "3.3", "proper": "0", "cost": "-215", "mtn": "1", "active": "1", "saba": "0", "id": "6", "channel": "SIP/905", "ASR60": "31.7"}, {"mfree": "0", "ASR40": "42.5", "ACD60": "3.4", "y": "0", "subcost": "0", "lastcheck": "2013-12-20 09:13:06+00:00", "ymobile": "0", "ASR20": "40.0", "number": "m735533097", "ACD20": "5.2", "ACD40": "3.4", "proper": "0", "cost": "0", "mtn": "1", "active": "0", "saba": "0", "id": "7", "channel": "SIP/906", "ASR60": "36.7"}, {"mfree": "175", "ASR40": "32.5", "ACD60": "4.6", "y": "0", "subcost": "-255", "lastcheck": "2013-12-20 19:19:12+00:00", "ymobile": "0", "ASR20": "30.0", "number": "m738234600", "ACD20": "4.5", "ACD40": "5.0", "proper": "0", "cost": "-255", "mtn": "1", "active": "1", "saba": "0", "id": "0", "channel": "SIP/907", "ASR60": "40.0"}, {"mfree": "0", "ASR40": "25.0", "ACD60": "5.0", "y": "0", "subcost": "-245", "lastcheck": "2013-12-20 19:24:15+00:00", "ymobile": "0", "ASR20": "10.0", "number": "m739049297", "ACD20": "3.2", "ACD40": "4.7", "proper": "0", "cost": "-245", "mtn": "1", "active": "1", "saba": "0", "id": "9", "channel": "SIP/908", "ASR60": "35.0"}, {"mfree": "168", "ASR40": "27.5", "ACD60": "2.7", "y": "0", "subcost": "-255", "lastcheck": "2013-12-20 19:20:16+00:00", "ymobile": "0", "ASR20": "30.0", "number": "m738491820", "ACD20": "3.5", "ACD40": "3.0", "proper": "0", "cost": "-255", "mtn": "1", "active": "1", "saba": "0", "id": "10", "channel": "SIP/909", "ASR60": "26.7"}, {"mfree": "192", "ASR40": "12.5", "ACD60": "5.3", "y": "0", "subcost": "-255", "lastcheck": "2013-12-20 19:20:17+00:00", "ymobile": "0", "ASR20": "10.0", "number": "m736150393", "ACD20": "5.5", "ACD40": "5.4", "proper": "0", "cost": "-255", "mtn": "1", "active": "1", "saba": "0", "id": "11", "channel": "SIP/910", "ASR60": "16.7"}, {"mfree": "235", "ASR40": "30.0", "ACD60": "2.9", "y": "0", "subcost": "-165", "lastcheck": "2013-12-20 19:25:20+00:00", "ymobile": "0", "ASR20": "25.0", "number": "m733742154", "ACD20": "4.0", "ACD40": "2.7", "proper": "0", "cost": "-165", "mtn": "1", "active": "1", "saba": "0", "id": "12", "channel": "SIP/911", "ASR60": "38.3"}, {"mfree": "281", "ASR40": "27.5", "ACD60": "5.3", "y": "0", "subcost": "-182", "lastcheck": "2013-12-20 19:25:21+00:00", "ymobile": "0", "ASR20": "15.0", "number": "m736959537", "ACD20": "5.5", "ACD40": "6.3", "proper": "0", "cost": "-182", "mtn": "1", "active": "1", "saba": "0", "id": "13", "channel": "SIP/912", "ASR60": "31.7"}, {"mfree": "203", "ASR40": "35.0", "ACD60": "6.0", "y": "0", "subcost": "-215", "lastcheck": "2013-12-20 19:25:22+00:00", "ymobile": "0", "ASR20": "40.0", "number": "m738459141", "ACD20": "4.5", "ACD40": "4.5", "proper": "0", "cost": "-215", "mtn": "1", "active": "1", "saba": "0", "id": "14", "channel": "SIP/913", "ASR60": "40.0"}, {"mfree": "362", "ASR40": "32.5", "ACD60": "2.6", "y": "0", "subcost": "-150", "lastcheck": "2013-12-20 19:26:26+00:00", "ymobile": "0", "ASR20": "15.0", "number": "m736079975", "ACD20": "2.2", "ACD40": "2.2", "proper": "0", "cost": "-150", "mtn": "1", "active": "1", "saba": "0", "id": "15", "channel": "SIP/914", "ASR60": "35.0"}, {"mfree": "342", "ASR40": "40.0", "ACD60": "6.2", "y": "0", "subcost": "-145", "lastcheck": "2013-12-20 19:26:27+00:00", "ymobile": "0", "ASR20": "35.0", "number": "m734685160", "ACD20": "6.5", "ACD40": "5.9", "proper": "0", "cost": "-145", "mtn": "1", "active": "1", "saba": "0", "id": "16", "channel": "SIP/915", "ASR60": "43.3"}, {"mfree": "0", "ASR40": "12.5", "ACD60": "4.1", "y": "0", "subcost": "-360", "lastcheck": "2013-12-20 19:26:28+00:00", "ymobile": "0", "ASR20": "20.0", "number": "m733984804", "ACD20": "4.9", "ACD40": "4.1", "proper": "0", "cost": "-360", "mtn": "1", "active": "1", "saba": "0", "id": "17", "channel": "SIP/916", "ASR60": "8.3"}, {"mfree": "179", "ASR40": "22.5", "ACD60": "2.9", "y": "0", "subcost": "-375", "lastcheck": "2013-12-20 19:26:29+00:00", "ymobile": "0", "ASR20": "25.0", "number": "m735872248", "ACD20": "1.6", "ACD40": "2.5", "proper": "0", "cost": "-375", "mtn": "1", "active": "1", "saba": "0", "id": "18", "channel": "SIP/917", "ASR60": "33.3"}, {"mfree": "287", "ASR40": "47.5", "ACD60": "4.5", "y": "0", "subcost": "-150", "lastcheck": "2013-12-20 19:26:30+00:00", "ymobile": "0", "ASR20": "40.0", "number": "m734502304", "ACD20": "4.7", "ACD40": "3.7", "proper": "0", "cost": "-150", "mtn": "1", "active": "1", "saba": "0", "id": "19", "channel": "SIP/918", "ASR60": "43.3"}, {"mfree": "198", "ASR40": "32.5", "ACD60": "7.6", "y": "0", "subcost": "-215", "lastcheck": "2013-12-20 19:26:31+00:00", "ymobile": "0", "ASR20": "35.0", "number": "m736034125", "ACD20": "7.6", "ACD40": "7.1", "proper": "0", "cost": "-215", "mtn": "1", "active": "1", "saba": "0", "id": "20", "channel": "SIP/919", "ASR60": "35.0"}, {"mfree": "282", "ASR40": "20.0", "ACD60": "4.1", "y": "0", "subcost": "-208", "lastcheck": "2013-12-20 19:27:35+00:00", "ymobile": "0", "ASR20": "15.0", "number": "m736180283", "ACD20": "5.5", "ACD40": "3.4", "proper": "0", "cost": "-208", "mtn": "1", "active": "1", "saba": "0", "id": "21", "channel": "SIP/920", "ASR60": "28.3"}, {"mfree": "336", "ASR40": "35.0", "ACD60": "5.7", "y": "0", "subcost": "-255", "lastcheck": "2013-12-20 19:27:36+00:00", "ymobile": "0", "ASR20": "10.0", "number": "m738356795", "ACD20": "5.8", "ACD40": "5.2", "proper": "0", "cost": "-255", "mtn": "1", "active": "1", "saba": "0", "id": "22", "channel": "SIP/921", "ASR60": "35.0"}, {"mfree": "309", "ASR40": "35.0", "ACD60": "4.9", "y": "0", "subcost": "-230", "lastcheck": "2013-12-20 19:27:37+00:00", "ymobile": "0", "ASR20": "20.0", "number": "m739051439", "ACD20": "3.8", "ACD40": "4.6", "proper": "0", "cost": "-230", "mtn": "1", "active": "1", "saba": "0", "id": "23", "channel": "SIP/922", "ASR60": "31.7"}, {"mfree": "214", "ASR40": "45.0", "ACD60": "4.6", "y": "0", "subcost": "-230", "lastcheck": "2013-12-20 19:27:38+00:00", "ymobile": "0", "ASR20": "30.0", "number": "m733078215", "ACD20": "3.3", "ACD40": "4.5", "proper": "0", "cost": "-230", "mtn": "1", "active": "1", "saba": "0", "id": "24", "channel": "SIP/923", "ASR60": "51.7"}, {"mfree": "0", "ASR40": "37.5", "ACD60": "5.2", "y": "0", "subcost": "-250", "lastcheck": "2013-12-20 19:27:39+00:00", "ymobile": "0", "ASR20": "35.0", "number": "m736130467", "ACD20": "3.5", "ACD40": "5.0", "proper": "0", "cost": "-250", "mtn": "1", "active": "1", "saba": "0", "id": "25", "channel": "SIP/924", "ASR60": "35.0"}, {"mfree": "378", "ASR40": "42.5", "ACD60": "4.4", "y": "0", "subcost": "-243", "lastcheck": "2013-12-20 19:27:40+00:00", "ymobile": "0", "ASR20": "45.0", "number": "m735032714", "ACD20": "4.5", "ACD40": "3.9", "proper": "0", "cost": "-243", "mtn": "1", "active": "1", "saba": "0", "id": "26", "channel": "SIP/925", "ASR60": "40.0"}, {"mfree": "276", "ASR40": "42.5", "ACD60": "3.7", "y": "0", "subcost": "-171", "lastcheck": "2013-12-20 18:14:42+00:00", "ymobile": "0", "ASR20": "35.0", "number": "m738165733", "ACD20": "3.8", "ACD40": "3.6", "proper": "0", "cost": "-160", "mtn": "1", "active": "1", "saba": "0", "id": "27", "channel": "SIP/926", "ASR60": "43.3"}, {"mfree": "192", "ASR40": "30.0", "ACD60": "4.0", "y": "0", "subcost": "-390", "lastcheck": "2013-12-20 18:14:43+00:00", "ymobile": "0", "ASR20": "25.0", "number": "m736751523", "ACD20": "3.7", "ACD40": "3.8", "proper": "0", "cost": "-374", "mtn": "1", "active": "1", "saba": "0", "id": "28", "channel": "SIP/927", "ASR60": "21.7"}, {"mfree": "185", "ASR40": "42.5", "ACD60": "6.6", "y": "0", "subcost": "-390", "lastcheck": "2013-12-20 18:14:44+00:00", "ymobile": "0", "ASR20": "40.0", "number": "m735573049", "ACD20": "6.3", "ACD40": "6.2", "proper": "0", "cost": "-370", "mtn": "1", "active": "1", "saba": "0", "id": "29", "channel": "SIP/928", "ASR60": "45.0"}, {"mfree": "193", "ASR40": "27.5", "ACD60": "4.2", "y": "0", "subcost": "-390", "lastcheck": "2013-12-20 18:14:45+00:00", "ymobile": "0", "ASR20": "25.0", "number": "m733153952", "ACD20": "3.5", "ACD40": "4.6", "proper": "0", "cost": "-376", "mtn": "1", "active": "1", "saba": "0", "id": "30", "channel": "SIP/929", "ASR60": "35.0"}, {"mfree": "154", "ASR40": "30.0", "ACD60": "4.7", "y": "0", "subcost": "-390", "lastcheck": "2013-12-20 18:14:46+00:00", "ymobile": "0", "ASR20": "50.0", "number": "m733461823", "ACD20": "4.7", "ACD40": "4.3", "proper": "0", "cost": "-370", "mtn": "1", "active": "1", "saba": "0", "id": "31", "channel": "SIP/930", "ASR60": "31.7"}, {"mfree": "154", "ASR40": "30.0", "ACD60": "5.2", "y": "0", "subcost": "-270", "lastcheck": "2013-12-20 18:14:47+00:00", "ymobile": "0", "ASR20": "35.0", "number": "m736833682", "ACD20": "4.9", "ACD40": "5.2", "proper": "0", "cost": "-267", "mtn": "1", "active": "1", "saba": "0", "id": "32", "channel": "SIP/931", "ASR60": "31.7"}, {"mfree": "0", "ASR40": "45.0", "ACD60": "3.8", "y": "1", "subcost": "-2646", "lastcheck": "2013-12-20 19:10:56+00:00", "ymobile": "1", "ASR20": "45.0", "number": "y", "ACD20": "3.4", "ACD40": "4.0", "proper": "0", "cost": "-2646", "mtn": "0", "active": "1", "saba": "0", "id": "33", "channel": "SIP/941", "ASR60": "43.3"}, {"mfree": "0", "ASR40": "15.0", "ACD60": "3.5", "y": "1", "subcost": "-3337", "lastcheck": "2013-12-20 19:10:57+00:00", "ymobile": "1", "ASR20": "5.0", "number": "y", "ACD20": "2.4", "ACD40": "2.7", "proper": "0", "cost": "-3337", "mtn": "0", "active": "1", "saba": "0", "id": "34", "channel": "SIP/942", "ASR60": "23.3"}, {"mfree": "0", "ASR40": "30.0", "ACD60": "6.2", "y": "1", "subcost": "-2490", "lastcheck": "2013-12-20 19:10:57+00:00", "ymobile": "1", "ASR20": "30.0", "number": "y", "ACD20": "7.4", "ACD40": "5.8", "proper": "0", "cost": "-2490", "mtn": "0", "active": "1", "saba": "0", "id": "35", "channel": "SIP/943", "ASR60": "33.3"}]
    TotalRecords = 399604
    return data, TotalRecords


def getDataFrom_Block_Server(block_server,days_count):
    timenow=datetime.datetime.now()
    today=datetime.date.today()
    t = timedelta(days=days_count)
    today=today-t

    cursor = connections[block_server].cursor()

    lasttrys=21     ####################################
    blockIfmore=20  ####################################


    cursor.execute("""SELECT userfield,billsec FROM cdr WHERE start > %s and disposition not LIKE %s and disposition not LIKE %s""",(today,"FAILED","BUSY"))
    cdrs = cursor.fetchall()

    numbers={}
    calls={}
    str_out=[]
    for cdr in cdrs:
        if(cdr[0] in calls):
            calls[cdr[0]].append(int(cdr[1]))
        else:
            calls[cdr[0]]=[]
            calls[cdr[0]].append(int(cdr[1]))
    for n in calls.iterkeys():
        inr1=0
        less40=0
        morethan120=False
        for c in calls[n]:
            if(c==0):
                    inr1=inr1+1
            if(c>63):
                    morethan120=True
            if(c<40 and c != 0 ):
                    less40=less40+1
        if(inr1>10 and not morethan120 and n!=""):
            try:
                cursor.execute("""Select number from block WHERE number LIKE %s""",(n,))
                foundone = cursor.fetchall()
                if(len(foundone)==0):
                    str_out.append({'number':str(n), 'count':len(calls[n]), 'calls': str(calls[n])})
                    continue
            except Exception,e:
                print e

        if(less40>3 and n!="" and not morethan120):
            try:
                cursor.execute("""Select number from block WHERE number LIKE %s""",(n,))
                foundone = cursor.fetchall()
                if(len(foundone)==0):
                    str_out.append({'number':str(n), 'count':len(calls[n]), 'calls': str(calls[n])})
            except:
                pass


    return str_out

### Angularjs start
class JSONResponse(HttpResponse):
    """
    An HttpResponse that renders its content into JSON.
    """
    def __init__(self, data, **kwargs):
        content = JSONRenderer().render(data)
        kwargs['content_type'] = 'application/json'
        super(JSONResponse, self).__init__(content, **kwargs)

@csrf_exempt
@protected_resource()
def ng_server_list(request):
    """
    List all servers, or create a new server.
    """
    username = request.resource_owner.username
    if request.method == 'GET':
        server = Server.objects(user=username).all().to_json()
        serializer = ServerSerializer(server)
        return JSONResponse(serializer.data)

    elif request.method == 'POST':
        data = JSONParser().parse(request)
        serializer = ServerSerializer(data=data)
        if serializer.is_valid():
            serializer.save()
            return JSONResponse(serializer.data, status=201)
        return JSONResponse(serializer.errors, status=400)

def ng_server_detail(request, server_name):
    print "abdul"
    """
    Retrieve, update or delete a server.
    """
    try:
        server = Server.objects.get(server=server_name)
    except Server.DoesNotExist:
        return HttpResponse(status=404)

    if request.method == 'GET':
        serializer = ServerSerializer(server)
        return JSONResponse(serializer.data)

    elif request.method == 'PUT':
        data = JSONParser().parse(request)
        serializer = ServerSerializer(server, data=data)
        if serializer.is_valid():
            serializer.save()
            return JSONResponse(serializer.data)
        return JSONResponse(serializer.errors, status=400)

    elif request.method == 'DELETE':
        server.delete()
        return HttpResponse(status=204)

class ServerViewSet(ListCreateAPIView):
    """
    API endpoint that allows users to be viewed or edited.
    """
    serializer_class = ServerSerializer
    queryset = Server.objects().all()

### Amgularjs end
