# -*- coding: utf-8 -*-
class Table:

    """ GetTableData class is used for the inserting
    the data from mysql to client side table """

    def __init__(self):
        pass

    """    
    def create_table_user(self, request, server_name, table_name, ud_obj, db, 
        db_server, db_refill, db_tabProp, db_chargeFromRemoteSettings, 
        logg, checkServer, json, settings, option="getModel"
    ):
            userTab = db.posts.find(
                {'user': request.user.username, 'server': server_name, 'table': table_name})[0]
            userModel = db_server.model.find({'server': server_name,
                                              'table': table_name})[0]['model']
            if userTab is not None:

                (columns, model, goModelHeader) = ud_obj.get_user_auth_data(
                    userTab['rights']['select'],
                    userTab['rights'][
                        'update'],
                    userTab['rights'][
                        'delete'],
                    userTab['rights'][
                        'insert'],
                    userTab['rights'][
                        'refill'],
                    userTab['rights'][
                        'refill_charge'],
                    userModel, db_tabProp) 
                return  columns, model, goModelHeader  
    """
    def create_table(self, request, server_name, table_name, ud_obj, db, 
        db_server, db_refill, db_tabProp, db_chargeFromRemoteSettings, db_globalDjangoSettings,
        logg, checkServer, json, settings, option="getModel"
    ):
        # it contains the def getUserAuthData()
        """ creating the table """
            #db = client[DB_name_User]
            #db_server = client[DB_name]
        if request.GET.get('option') == option:

            logg( "creating the table ")
            
            userTab = db.posts.find({
                'user': request.user.username, 
                'server': server_name, 
                'table': table_name}
            )[0]
            userModel = db_server.model.find({
                'server': server_name,
                'table': table_name
            })[0]['model']

            Ipofserver = db_server.posts.find(
                {'servername': server_name})[0]['host']

            logg("checkServer:%s" % (checkServer))
            if userTab is not None and checkServer:

                columns, model, goModelHeader = ud_obj.get_user_auth_data(
                    userTab['rights']['select'],
                    userTab['rights'][
                        'update'],
                    userTab['rights'][
                        'delete'],
                    userTab['rights'][
                        'insert'],
                    userTab['rights'][
                        'refill'],
                    userTab['rights'][
                        'refill_charge'],
                    userModel, db_tabProp
                )
                # print "columns:%s"%(columns)
                # print "model:%s"%(model)

                give_IP_TO_nodeserver = db_server.posts.find(
                    {'servername': server_name})[0]['g_IP']

                try:
                    aufladenip = db_refill.posts.find({
                        'servername': server_name, 
                        'partner': db_server.posts.find({
                            'servername': server_name
                        })[0]['partner']
                    })[0]['aufladenip']

                except IndexError as e:
                    logg(str(e))
                    aufladenip = ''

                try:
                    mouseOverFunction = userTab['rights']['mouseOverFunction']
                except:
                    mouseOverFunction = "give mouseOverFunction rights in db "
                    logg(mouseOverFunction)
                try:
                    htmlWinDimension = db_server.posts.find(
                        {'servername': server_name})[0]['htmlWinDimension']
                except:
                    htmlWinDimension = ''
                    logg('htmlWinDimension cannot to be shown on this server')

                logg("IP:%s,username:%s" %
                     (give_IP_TO_nodeserver, request.user.username))

                logg("The partner of {} is {}".format(
                    server_name, db_server.posts.find({
                        'servername': server_name
                })[0]['partner']))

                # db charge remote
                dbChargeRemote = []
                for i in db_chargeFromRemoteSettings.chargeFromRemoteServer.find({
                    'partner': db_server.posts.find(
                        {'servername': server_name})[0]['partner']
                }):
                    i.pop("_id", None)
                    i.pop("partner", None)
                    dbChargeRemote.append(i)

                logg("dbChargeRemote:{}".format(dbChargeRemote))

                try:
                    prefix = db_server.posts.find({
                        'servername': server_name
                    })[0]['prefix']

                except:
                    prefix = 'no prefix, add into serverinfoMongoDB'

                try:
                    allowed_numbers = db.call.find({
                        'user': request.user.username
                    })[0]['allowed_numbers']

                except:
                    allowed_numbers = 'no allowed_numbers'

                try:
                    passCall = db.call.find({
                        'user': request.user.username
                    })[0]['pass']

                except:
                    passCall = 'no pass'
                model_history_users = db_server.model.find({   
                    "server": "serverg1",
                    "table": "history_users_active" 
                })[0]['model']
                
            # for user history
                #(columns_user, model_user, goModelHeader_user) = self.create_table_user(request, 'serverg1', 'history_users_active', ud_obj, db, db_server, db_refill, db_tabProp, db_chargeFromRemoteSettings, logg, checkServer, json, settings, option="getModel")
                
                menuFeaturesItem = []
                try:
                    arrAuthFeature = userTab['rights']['features']
                    for i in arrAuthFeature:
                        menuFeaturesItem.append(db_globalDjangoSettings.settings.find({'access':'djangoSettings'})[0]['features'][i])
                except Exception as e:
                    menuFeaturesItem.append('KeyError, ' + str(e))

                logg('model_history_users {}'.format(model_history_users))
                jsondata = json.dumps(
                    {
                    'tableObjects': {   
                                        'model': model,
                                        'columns': columns,
                                        'columnsheader': goModelHeader,
                                        'tbarInsert': userTab['rights']['insert'],
                                        #'model_user': model_user,
                                        #'columns_user': columns_user,
                                        #'columnsheader_user': goModelHeader_user,
                                        'checkServer': checkServer,
                                        'give_IP_TO_nodeserver': give_IP_TO_nodeserver,
                                        'username': request.user.username,
                                        'mouseOverFunction': mouseOverFunction,
                                        'aufladenip': aufladenip,
                                        'port': db_server.posts.find({'servername': server_name})[0]['port'],
                                        'portdebug': db_server.posts.find({'servername': server_name})[0]['portdebug'],
                                        'prefix': prefix,
                                        'passCall': passCall,
                                        'allowed_numbers':allowed_numbers,
                                        'debug': settings.DEBUG,
                                        #'model_history_users':db_server.model.find({   
                                        #    "server": "serverg1",
                                        #    "table": "history_users_active" 
                                        #})[0]['model'],
                                        'htmlWinDimension': htmlWinDimension,
                                        'menuFeaturesItem':menuFeaturesItem,
                                        #'features':db_globalDjangoSettings.settings.find({'access':'djangoSettings'})[0]['serviceType'][request.GET.get('serviceType')],
                                        'username': request.user.username,
                                        'ForRefillBalanceWindowSIM_IP': dbChargeRemote
                                    }
                    }
                )
                # logg('Columsszzzzzzz:{}'.format(columns))
                return jsondata
                # return HttpResponse(json.dumps({'model': model, 'columns': columns,
                # 'columnsheader':goModelHeader, 'checkServer':checkServer,
                # 'Ipofserver':Ipofserver}), content_type='application/json')

    def update_table():
        pass

    def put_table():
        pass

    def delete_table():
        pass
