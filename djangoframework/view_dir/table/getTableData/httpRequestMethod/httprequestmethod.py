# -*- coding: utf-8 -*-
import json
import datetime
import re

class HTTPRequestMethod:

    def __init(self):
        pass

    def bodydict_ele(self, request, server_name, loggi):
        # print request.body
        bodyDict = json.loads(request.body)
        if server_name == 'serverd190' or server_name == 'serverd200':
            if 'number' in bodyDict.keys():
                bodyDict['user'] = request.user.username

        dtime = datetime.datetime.now()
        loggi("bodyDict {}".format(bodyDict))
        vs = [];cs = [];vsu = [];val = []
        for s, cole in enumerate(bodyDict):

            key = bodyDict.keys()[s]
            value = bodyDict[key]

            if key == "number":
                numberBool = True
            if key == "active" and bodyDict[key] == 0:
                activeBool = True
            if key == "lastcheck":
                loggi("lastcheck %s" % (value))

            # spliting the time
            # print key
            if key == 'id':
                key = 'Id'  # for my sql
            cs.append(key)
            vs.append("%s")
            val.append(value)
            vsu.append(key + "=%s")

        if "Id" in cs:
            del vs[cs.index('Id')]
            cs.remove('Id')

        if None in val:
            val[val.index(None)] = "%sT%s" % (
                dtime.strftime("%y-%m-%d"), dtime.strftime("%H:%M:%S"))
        return cs, vs, val, vsu

    def INSERT(
        self, cs, vs, val, cur,
        table_name, userTab, bodyDict, username, loggi
    ):

        loggi('userTab: ' + str(userTab))

        # creating the default values for storage
        vs = [];cs = [];create_bodyDict = {}
        # creating the body dic for storage
        for i in userTab.keys():

            if userTab[i]['type'] == 'int':
                create_bodyDict[i] = 0
            if userTab[i]['type'] == 'float':
                create_bodyDict[i] = 0
            if userTab[i]['type'] == 'string':
                if i == 'user':
                    create_bodyDict[i] = username
                else:
                    create_bodyDict[i] = ''
            if userTab[i]['type'] == 'date':
                #create_bodyDict[i] = "0000-00-00 00:00:00"#datetime.datetime.now()
                create_bodyDict[i] = datetime.datetime.now()

            vs.append('%s')

        # setting the value recieved in dict form
        for j in bodyDict:
            create_bodyDict[j] = bodyDict[j]

        val = []
        for k in create_bodyDict:
            val.append(create_bodyDict[k])
            cs.append(k)

        if "id" in cs:
            del vs[cs.index('id')]
            cs.remove('id')

        cs = ','.join(cs)
        vs = ','.join(vs)
        val.remove(-1)

        loggi(
            "insert into " + table_name
            + " (" + cs + ") values (" + str(val) + ")"
        )
        try:
            cur.execute(
                "insert into " + table_name
                + " (" + cs + ") values (" + vs + ")",
                tuple(val)
            )
        except:
            print "PROBLEMMMMMM"
        return '', 'HttpResponse'
        #try:
        #    pass
        #    # return HttpResponse('', content_type='application/json')
        #except Exception, e:
        #    loggi('ERROR MYSQL BEI INSERT {}'.format(e))
        #    dublicate = False
        #    mssg = ''
        #    for i in e:
        #        if 'Duplicate' in str(i):
        #            dublicate = True
        #            mssg = i
        #    return json.dumps({'message': str(e)}), 'HttpResponseBadRequest'
            # return HttpResponseBadRequest(json.dumps({'message': str(e)}),
            # content_type='application/json')

    ################### authorization ????????
    def UPDATE(
        self, val, vsu, server_name,
        table_name, numberBool, activeBool,
        bodyDict, cur, loggi
    ):

        loggi("###### updating the row ###### ".upper())
        val.append(bodyDict['id'])
        loggi(
            "UPDATE " + table_name +
            " SET " + ','.join(vsu) +
            " WHERE Id = %s" % (val)
        )

        try:
            cur.execute(
                "UPDATE " + table_name +
                " SET " + ','.join(vsu) +
                " WHERE Id = %s", tuple(val)
            )
        except Exception, e:  # 3/5/2014
            loggi(
                "one column is not inserted, but the value are saved"
            )
            return json.dumps({'message': str(e)}), 'HttpResponseBadRequest'
            # return HttpResponse(json.dumps({'message': e}),
            # content_type='application/json')


        if table_name == "cost":

            # delete the the prefix l,m,m from the number column
            if 'active' in  bodyDict:
                if bodyDict['active']==0:
                    cur.execute(
                        "select number from {} where Id = {}".format(
                            table_name, bodyDict['id']
                        ),
                    )
                    bodyDict['number'] = cur.fetchone()[0]
                    loggi('accessed number {}'.format(bodyDict['number']))
                    # find the number in bodyDict, then using /w/
                    # expersion, search for l, m, y and set in number field
                    numbMatch = re.match('\w', bodyDict['number'])
                    loggi(
                        "now I am changing the number from d190 Id={}".format(
                            val[len(val) - 1])
                    )
                    try:
                        if(server_name == "serverj140"):
                            cur.execute(
                                "UPDATE " + table_name + " SET " +
                                "number = %s, cardid = %s, " +
                                "c5201 = %s, c5202 = %s, " +
                                "c1236 = %s, c1237 = %s " +
                                "WHERE Id = %s",
                                (numbMatch.group(0), "", 0,
                                 0, 0, 0, bodyDict['id'])
                            )
                        else:
                            cur.execute(
                                "UPDATE " + table_name + " SET " + "number = %s" +
                                " WHERE Id = %s",
                                (numbMatch.group(0), bodyDict['id'])
                            )
                    except Exception, e:
                        loggi("set number= where id FAILED ")
        return '', 'HttpResponse'

        # S 3/5/2014
        # return HttpResponse('', content_type='application/json')
    def PUT(self, request, server_name, table_name, cur, userTab, loggi):
        localBool = False
        activeBool = False
        numberBool = False

        loggi('def PUT, requestGET: {}'.format(request.GET))

        bodyDict = json.loads(request.body)

        cs, vs, val, vsu = self.bodydict_ele(
            request,
            server_name,
            loggi
        )

        if bodyDict['id'] is -1:
            # S 3/5/2014
            loggi("###### Inersting values ".upper())
            dispval, Httpreq_string = self.INSERT(
                cs, vs, val, cur, table_name,
                userTab, bodyDict, request.user.username, loggi
            )
            # return HttpResponse('', content_type='application/json')
            # E 3/5/2014
        else:

            dispval, Httpreq_string = self.UPDATE(
                val, vsu, server_name,
                table_name, numberBool, activeBool,
                bodyDict, cur, loggi
            )
        return dispval, Httpreq_string
        #try:
        #    1
        #except Exception, e:
        #    return json.dumps({
        #        'message': str(e)}
        #    ), 'HttpResponseBadRequest'

    def DELETE(self, request, table_name, cur, loggi):
        # print request.body
        bodyDict = json.loads(request.body)
        #
        if bodyDict['id'] is not -1:
            loggi("#### DELETE from Mysql ###########")
            cur.execute(
                'DELETE FROM ' + table_name +
                ' WHERE ' + table_name +
                ' . ' + 'id' + '=' + str(bodyDict['id'])
            )
        else:
            loggi("#### DELETE the content from local grid ###########")

        return '', 'HttpResponse'
