# -*- coding: utf-8 -*-
class UserAuthData:

	def __init__(self):
		pass

	def get_user_auth_data(self, select, update, delete, insert, refill, refill_charge, userModel, db_tabProp):
	    #fields = ['id', 'userfield', 'dstchannel', 'billsec']
	    # {'float':'float','int':'int','time':'date','char':'string','text':'string'}
	    goColumns = []
	    goModel = []
	    goModelHeader = []

	    columns = {}
	    columns['xtype'] = 'actioncolumn'
	    columns['width'] = 20
	    columns['sortable'] = False
	    columns['menuDisabled'] = True
	    columns['items'] = []
	    items = {}
	    if delete =="True":
	        items['text'] = 'delete'
	        items['iconCls'] = 'remove'
	    items['tooltip'] = 'delete'
	    items['handler'] = 'handler'
	    columns['items'].append(items)
	    goColumns.append(columns)

	    columns = {}
	    columns['xtype'] = 'actioncolumn'
	    columns['width'] = 20
	    columns['sortable'] = False
	    columns['menuDisabled'] = True
	    columns['items'] = []
	    items = {}
	    # show the refill if it is contained in UserInfoMongoDB
	    if refill=="True":
	        items['text'] = 'reffel'
	        items['iconCls'] = 'reffel'
	    if refill_charge=="True":
	        items['text'] = 'reffel_charge'
	        items['iconCls'] = 'reffel'
	    #items['xtype'] = 'splitbutton'
	    #items['renderTo']='button-ct'
	    items['tooltip'] = 'reffel'
	    items['handler'] = 'handler'
	    items['menu'] = 'menu'

	    columns['items'].append(items)
	    goColumns.append(columns)
	    
	    for v in select:
	        # getting model for the auth user

	        model = userModel[v]
	        goModel.append(model)
	        goModelHeader.append({'fieldLabel': str(v), 'name': 'name', 'name': 'name'})

	        columns = {}
	        columns['text'] = str(v)
	        columns['dataIndex'] = str(v)
	        columns['width'] = 50#model['width']
	        columns['filterable'] = True
	        if model['type']=='date':
	            columns['xtype'] = 'datecolumn'
	            columns['format'] = 'Y-m-d H:i:s'  # n/j h:ia
	        # width shld be based on type of column header?????????????????????????????????????????????????

	        for i in db_tabProp.columnWidth.find({'column': 'columnWidth'},{'_id': 0})[0]['col_dim']:
	            if v == i['header']:columns.update({'width': i['dim']})

	        if v in update:
	            columns['editor'] = {}
	            columns['editor']['allowBlank'] = True
	            if model['type'] == 'int':
	                columns['editor']['xtype'] = 'numberfield'
	                columns['editor']['minValue'] = -1.0*10**12
	                columns['editor']['maxValue'] = 1.0*10**12
	                #if v == 'active':
	                #columns['editor']['maxValue'] = 1
	                columns['editor']['minValue'] = -1.0*10**12
	                columns['editor']['maxValue'] = 1.0*10**12
	            elif model['type'] == 'float':
	                columns['editor']['xtype'] = 'numberfield'
	                #columns['editor']['floating'] = True
	                columns['editor']['minValue'] = -1.0*10**12
	                columns['editor']['maxValue'] = 1.0*10**12
	            elif model['type'] == 'date':
	                columns['editor']['xtype'] = 'datetimefield'
	                columns['editor']['format'] = 'Y-m-d H:i:s'  # n/j h:ia
	            elif model['type'] == 'string':
	                columns['editor']['xtype'] = 'textfield'

	        goColumns.append(columns)



	    goModelHeader.append({
	                            'xtype':'button',
	                            'text': '<center><b> click OK</b></center>',
	                            'handler': 'handler'
	                        })

	    return (goColumns, goModel, goModelHeader)