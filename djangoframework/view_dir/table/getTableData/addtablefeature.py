# -*- coding: utf-8 -*-
import json
from datetime import datetime

class GridFeature:

    """ features are integrated in Grid table """

    def __init__(self):
        pass

    def reset_active_button(self, server_name, table_name,
                            cur, logg, json):
        """ check the nodeserver connectivity on/off """

        if (server_name == "serverd190" or server_name == "serverd200") and table_name == "cost":
            logg("setting ACTIVE=7 to ACTIVE = 1")
            cur.execute("UPDATE " + table_name + " SET " +
                        "active = %s" + " WHERE active = %s", (1, 7))
            return json.dumps(
                {'message': 'all ACTIVE=7 is now ACTIVE =1'})
        else:
            return json.dumps({'message': 'operation not allowed for '
                               + server_name});

    def billsec_button(self, request, server_name, table_name,
                       cur, logg, json):
        """ this button delete all sims having billsec < 10 """


        if table_name == "cdr":
            logg("SELECT * from {} where dstchannel LIKE '{}'".format(
                    table_name, 'SIP/{}%'.format(request.GET.get('value'))
                )+" and billsec > 0 order by id DESC limit 10")
            cur.execute(
                "SELECT * from {} where dstchannel LIKE '{}'".format(
                    table_name, 'SIP/{}%'.format(request.GET.get('value'))
                )+" and billsec > 0 order by id DESC limit 10"
            )

            billsecVal = cur.fetchall()
            logg("billsecVal: {}".format(billsecVal))
            for k in billsecVal:
                if k[4] < 14:
                    logg("deleted billsec=%s , with id=%s" % (k[4], k[0]))
                    cur.execute(
                        "DELETE FROM " + table_name + " WHERE id={}".format(k[0]))
            if not billsecVal:
                logg("Nothing to be deleted")
                return json.dumps({'message': 'Nothing to be deleted !!!'})
            else:
                logg("all 10 sims having billsec < 14 are delete")
                return json.dumps(
                    {'message': 'all sims having billsec < 14 are deleted'})
        else:
            return json.dumps({'message': 'operation not allowed for '
                               + server_name})
    def billsec_button_cost(self, request, server_name, table_name,
                       cur, logg, json):
        """ this button delete all sims having billsec < 10 """
        table_name = 'cdr'
        
        logg("SELECT * from {} where dstchannel LIKE '{}'".format(
                table_name, 'SIP/{}%'.format(request.GET.get('value'))
        ) + " and billsec > 0 order by id DESC limit 10")
        
        cur.execute(
            "SELECT * from {} where dstchannel LIKE '{}'".format(
                table_name, 'SIP/{}%'.format(request.GET.get('value'))
            ) + " and billsec > 0 order by id DESC limit 10"
        )

        billsecVal = cur.fetchall()
        logg("billsecVal: {}".format(billsecVal))
        for k in billsecVal:
            if k[4] < 14:
                logg("deleted billsec=%s , with id=%s" % (k[4], k[0]))
                cur.execute(
                    "DELETE FROM " + table_name + " WHERE id={}".format(k[0]))
        if not billsecVal:
            logg("Nothing to be deleted")
            return json.dumps({'message': 'Nothing to be deleted !!!'})
        else:
            logg("all {} sims having billsec < 14 are delete".format(len(billsecVal)))
            return json.dumps(
                {'message': "all {} sims having billsec < 14 are delete".format(len(billsecVal))})


    def mysqlQuery(self, string, cur, string_count):
        try:
            cur.execute(string)
            desc = cur.description
            data = []
            for row in cur.fetchall():
                dict1 = {}
                for (i, col) in enumerate(row):
                    if isinstance(col, unicode ):
                        dict1[desc[i][0]] = col.encode('utf-8')
                    else:
                        dict1[desc[i][0]] = unicode(str(col), 'utf-8')

                    #print unicode(col, 'utf-8')

                data.append(dict1)

            cur.execute(string_count)
            TotalRecords = cur.fetchone()[0]
            
            return data, TotalRecords
        except Exception, e:
            return str(e.message) , "error"

    def sort_icon(self, request, table_name, cur, logg):
        # this is for the when u clicked sort, the filtered elements
        """ clicked on sort icon """

        logg( '########## sorting the table ###### '.upper())
        limit = request.GET.get('limit')
        start = request.GET.get('start')
        sort = request.GET.get('sort')
        dirr = request.GET.get('dir')

        string = "select SQL_CALC_FOUND_ROWS *"+ \
                " from {}".format(table_name)+ \
                " order by {} {}".format(sort, dirr)+ \
                " limit {}, {}".format(str(start), str(limit))
        #string_count = "select count(*)"+ \
        #        " from {}".format(table_name)
        #string_count = "select count(*)"+ \
        #       " from {}".format(table_name)
        string_count = "SELECT FOUND_ROWS()"

        logg( string) ####################################### 1 code
        data, TotalRecords = self.mysqlQuery(string, cur, string_count)
        return data, TotalRecords

    # validating date time
    def validate_date(self, d):
        try:
            datetime.strptime(d, '%m/%d/%Y')
            return True
        except ValueError:
            return False

    def filter_menu(self, request, table_name, cur, logg):

        """ modified searching feature in filter """

        limit = request.GET.get('limit')
        start = request.GET.get('start')
        sort = request.GET.get('sort')
        dirr = request.GET.get('dir')
        # checking the presence of filter
        try:
            filter_json = json.loads(request.GET.get('filter'))

            for c, k in enumerate(filter_json):
                try:
                    if len(k['value'].split('/'))==3:# for data, changing it format for mysql
                        if self.validate_date(k['value']):
                            sfj = k['value'].split('/')
                            filter_json[c]['value'] = '/'.join([sfj[2], sfj[0], sfj[1]])
                            logg('filter_json:{}'.format(filter_json))
                except:
                    pass

            indicate = True
        except:
            indicate = False


        if indicate:
            # if request.GET.get('sort')
            logg('########## filtering the row value ###### '.upper())
            string = 'select SQL_CALC_FOUND_ROWS *' + \
                    ' from {}'.format(table_name) + \
                    ' where '
            for c, k in enumerate(filter_json):
                if k['type'] != "string":
                    if k['comparison'] == 'eq':
                        wert = ' like '
                    if k['comparison'] == 'gt':
                        wert = ' > '
                    if k['comparison'] == 'lt':
                        wert = ' < '
                else:
                    wert = " like "

                string += str(k['field']) + wert + "'" + \
                        str(k['value']) + "'"

                if c is not len(filter_json) - 1:
                    lasts = ' and '
                else:
                    lasts = ''

                string += lasts

            if request.GET.get('sort') is not None:

                # making exception for cdr table
                string += " order by {}".format(sort) + \
                    " {} limit".format(dirr) + \
                    " {}, {}".format(str(start), str(limit))
            else:
                string += " limit {}, {}".format(str(start), str(limit))

            logg(string)  # 1 code wiederholt
        else:
            logg("accessing data from server")

            #string = 'select *' + \
            #        ' from {}'.format(table_name) + \
            #        ' limit {}, {}'.format(str(start), str(limit))
            string = 'select SQL_CALC_FOUND_ROWS *' + \
                    ' from {}'.format(table_name) + \
                    ' limit {}, {}'.format(str(start), str(limit))
        #string_count = "select count(*)"+ \
        #       " from {}".format(table_name)
        string_count = "SELECT FOUND_ROWS()"
        data, TotalRecords = self.mysqlQuery(string, cur, string_count)
        logg(' ########################## TotalRecords:{}'.format(TotalRecords))
        #print 'data0:{}'.format(data[0])
        return data, TotalRecords

    def mysqlQuery_serverg1(self, request, table_name, cur, logg):

        """
        Accesing data from serverg1
        """
        limit = request.GET.get('limit')
        start = request.GET.get('start')


        # authoriazation ????????

        string = 'select SQL_CALC_FOUND_ROWS *' + \
                ' from {}'.format(table_name) + \
                ' limit {}, {}'.format(str(start), str(limit))

        curh = connections[server_name].cursor()
        curh.execute(string)
        desc = curh.description
        data = []
        for row in curh.fetchall():
            dict1 = {}
            for (i, col) in enumerate(row):
                if isinstance(col, unicode ):
                    dict1[desc[i][0]] = col.encode('utf-8')
                else:
                    dict1[desc[i][0]] = unicode(str(col), 'utf-8')

                #print unicode(col, 'utf-8')

            data.append(dict1)

        curh.execute('SELECT FOUND_ROWS()')
        TotalRecords = curh.fetchone()[0]
        return data, TotalRecords
