# -*- coding: utf-8 -*-
class ServerList:

    """ ServerList is used for the creation
    of menu items of server in Desktop start """

    def __init__(self):
        pass

    def get_servers_with_table_list(self, request, db):
        """ get the server list from the tabele """

        data = {}
        for row in db.posts.find({'user': request.user.username},
                                 {'server': 1, '_id': 0}).sort('server', 1):
            if 'server' in row.iterkeys():
                if row['server'] not in data.iterkeys():
                    tables = []
                    for table in \
                        db.posts.find({'user': request.user.username,
                                       'server': row['server']}, {'table': 1,
                                                                  '_id': 0}).sort('table', 1):
                        if 'table' in table.iterkeys():
                            if table['table'] not in tables:
                                tables.append(table['table'])
                    if row['server'] not in data.iterkeys():
                        data[row['server']] = tables
        data['user'] = request.user.username
        return data

    def get_server_list(self, request, db):
        """ get server list """

        data = []
        for row in db.posts.find({'user': request.user.username},
                                 {'server': 1, '_id': 0}).sort('server', 1):
            if 'server' in row.iterkeys():
                if row['server'] not in data:
                    data.append(row['server'])
        return data

    def tables(self, request, server_name, db):
        """ get tables """
        #db = client[DB_name_User]
        data = []
        for row in db.posts.find({'user': request.user.username,
                                 'server': server_name}, {'table': 1,
                                 '_id': 0}).sort('table', 1):
            if 'table' in row.iterkeys():
                if row['table'] not in data:
                    data.append(row['table'])
        return data
