# getrefilldatafile.py
# autopep8 djangoframework/view_dir/table/refillPanel/refilldata/getrefilldatafile.py  --reself.cursive --in-place --pep8-passes 2000 --verbose
# pep8
# djangoframework/view_dir/table/refillPanel/refilldata/getrefilldatafile.py
import json


class RefillData:

    def __init__(self, request, server_name, table_name, db, db_server, db_refill, refillType, con, logg):
        self.request = request 
        self.server_name = server_name 
        self.table_name = table_name 
        self.db = db 
        self.db_server = db_server 
        self.db_refill = db_refill 
        self.con = con 
        self.logg = logg
        self.refillType = refillType

    def get_refill_data(self):
        self.logg("func get_refill_data")
        #self.logg("option {}".format(self.request.GET))
        #//////// USEFUL COMMAND ///////////////////
        # SELECT * FROM table WHERE id > 100 LIMIT 10;
        # SELECT COUNT(*) FROM table WHERE id > 100;
        # self.self.con.execute(string)
        #////////////////////////////////////////////
        dic = {}
        #if self.server_name == 'serverd190':
        #    curn = self.con["serverd190"].cursor()
        #    curn.execute(
        #        "select COUNT(*) from " + 'cards_m_10' + " where used=%s" % (0))
        #    dic['no_of_cards'] = curn.fetchall()[0][0]
        #    self.logg("no of cards: {} in {}".format(
        #        dic['no_of_cards'], 'cards_m_10'))
        #else:
        dic['no_of_cards'] = 'nothing'

        # shibu
        # refill_chargeVal and refillval are stored in "UserInfoMongoself.db" for a
        # user by the admin

        # refill rights
        refillval = self.db.posts.find({'user': self.request.user.username,
                                   'server': self.server_name, 'table': self.table_name})[0]['rights']['refill']
        refill_chargeVal = self.db.posts.find({'user': self.request.user.username,
                                          'server': self.server_name, 'table': self.table_name})[0]['rights']['refill_charge']
        partnerNameInServer = self.db_server.posts.find(
            {'servername': self.server_name})[0]['partner']
        # print "hkdfgjd %s, %s, %s"%(refill_chargeVal[0], refillval[0], partnerNameInServerself.db[0])
        # charge the admin
        if refill_chargeVal == "True" and refillval == "False":
            self.logg(
                "refill(self.request, self.server_name, table_name, refillType): I AM CHARGING REFILL")
            dict_self.db = self.db_refill.posts.find_one(
                {'servername': self.server_name, 'partner': partnerNameInServer})
            if dict_self.db is not None:
                if (dict_self.db['type'] == refillType):

                    dic['balance'] = [{'name': dict_self.db['balance']}]
                    dic['amounts'] = [{'name': bl}
                                      for bl in dict_self.db['amounts']]
                    dic['command'] = dict_self.db['command']
                    dic['devicenumber'] = dict_self.db['devicenumber']
                    dic['aufladenip'] = dict_self.db['aufladenip']
                    dic['klass'] = 'c2'
        # show the refill
        if refillval == "True" and refill_chargeVal == "False":
            self.logg(
                "refill(self.request, self.server_name, table_name, refillType): I AM SHOWING THE CONTENTS IN REFILL")
            dic['klass'] = 'c1'
        # for new panel
        try:
            Refillclass = self.db.posts.find({'user': self.request.user.username,
                                         'server': self.server_name, 'table': self.table_name})[0]['rights']['Refillclass']

            if refillval == "True" and refill_chargeVal == "False" and Refillclass == "c3":
                self.logg("I am in refillclass c3")
                dic['klass'] = Refillclass

        except:
            self.logg("No Refillclass is added to mongoself.db, skipping ")

        if self.table_name == "cost":
            
            try:
                dic = self.getCommandDic(
                    dic, self.request.user.username, self.server_name, self.table_name, self.refillType)
            except:
                dic['command'] = [{'':''}]
                dic['command_key'] = [{'':''}]
                dic['command_func'] = [{'':''}]                    


        return json.dumps(dic)
        # return HttpResponse(json.dumps(dic), content_type='application/json')
        # return HttpResponse(json.dumps({'randKey':random.getranself.dbits(128),
        # 'states':[{'name':tab} for tab in storeself.curForRefill[0]]}),
        # content_type='application/json')
    def getCommandDic(self, dic, username, server_name, table_name, refillType):
        self.logg("func get_refill_data")
        dbpost = self.db.posts.find({'user': username,'server': server_name, 'table':table_name})[0]['rights'][refillType]
        dic['command'] = [{'name':tab.split('____')[1]} for tab in dbpost]
        dic['command_key'] = [{tab.split('____')[1]:tab.split('____')[0]} for tab in dbpost]
        self.logg('dbpost:{}'.format(dbpost))
        dic['command_func'] = [{tab.split('____')[1]:tab.split('____')[2]} for tab in dbpost]
        return dic
