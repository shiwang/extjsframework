# -*- coding: utf-8 -*-
from refilldata import getrefilldatafile

class RefillPanel:
	""" RefillPanel class is used for
	giving data to client and it is also
	used for storing the values in msql as per
	the client command """

	def __init__(self):
		pass

	def refill_data(self, request, server_name, table_name, db, db_server, db_refill, refillType, con, logg):
		
		ref = getrefilldatafile.RefillData(request, server_name, table_name, db, db_server, db_refill, refillType, con, logg)
		return ref.get_refill_data()

	def charging_from_remote_server():
		pass

	def get_refill_cardno():
		pass

	def balance_refill():
		pass

	def refill():
		""" this has to split into other functions
		in other files in the same dir (should be deleted)"""
		pass 
