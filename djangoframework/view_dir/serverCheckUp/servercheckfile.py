# -*- coding: utf-8 -*-
import os
import sys
import subprocess
# node 'nodeServer26190'
def nodeServer_start(node_name, port):

  print 'Hallo {}'.format(node_name)
  stopVar = True
  nodeName = 'default'
  #print sys.args[1]
  try:
      nodeName = node_name#sys.argv[1]
  except:
     # print "missing one argument,choose one from [nodeServer26200, nodeServer26100, nodeServer26190, nodeServer26140, nodeServer26050]"
      stopVar = False
  if nodeName not in ['nodeServer26200', 'nodeServer26100', 'nodeServer26190', 'nodeServer26140', 'nodeServer26050']:
      #print "not valid arg, please choose one from [nodeServer26200, nodeServer26100, nodeServer26190, nodeServer26140, nodeServer26050]"
     pass# topVar = False

  if nodeName == 'nodeServer26200':
      link = '/opt/framework/djangoframework/djangoframework/static/examples/desktop/node_modules/'
  else:
      link = '/var/www/djangoframework/djangoframework/static/examples/desktop/node_modules/'

  if stopVar:
      bothconnectioncount = 0
      try:
          # pid of the second process
          val = os.popen("ps -eo pid,command | grep "+ "'node {}{}.js'".format(link, nodeName) \
                         +" | grep -v grep | awk '{print $1}'")
          pid= val.next()
          print "pid:{}".format(pid) 
          bothconnectioncount += 1

          return "{} is on, may be your Internet is off, click yes to refresh your page".format(nodeName), True

      except:
          pass

      # when the node is starting for the first time    
      if (bothconnectioncount == 0):
          #print "{} has crashed".format(nodeName)

          #try:
              # killing the process 
              #os.popen("ps -eo pid,command | grep "+ "'node {}{}.js'".format(link, nodeName) \
              #               +" | grep -v grep | awk '{print $1}' | xargs kill -9")
             # print "{} is killed".format(nodeName)
          #except:
          #    pass #print "No process to kill"

          # starting node, if never started
          print "/usr/bin/node {}{}.js".format(link, nodeName)
          #subprocess.call("node {}{}.js &".format(link, nodeName), shell=True)
          #subprocess.call("crontab /root/checkServer{}.txt".format(port), shell=True)
          return "{} is off, wait for 1 minute and try again".format(nodeName), False
          #print "{} is now started".format(nodeName)


     # if (bothconnectioncount == 1):
          #print "{} working....".format(nodeName)