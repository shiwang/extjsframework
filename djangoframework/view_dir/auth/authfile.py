# -*- coding: utf-8 -*-
class Auth:

    """ Auth Class contains importants
        functions to login into the desktop
        login(), logout(), auth_view() """

    def __init__(self):
        pass
        #request = request

    def hello(self):
        print "hello Auth"

    def login(self, request, csrf):
        csrf_dic = {}
        csrf_dic.update(csrf(request))
        csrf_dic['CSRF_TOKEN'] = csrf(request)['csrf_token']
        return csrf_dic

    def logout(self, request, auth, db, datetime):
        auth.logout(request)
        dmy = datetime.datetime.now().strftime("%d/%m/%y")
        hm = datetime.datetime.now().strftime("%H:%M")
        if request.GET.get('logOutTime') is not None:
            dateTime = request.GET.get('logOutTime')
            userName = request.GET.get('username')
        else:
            dateTime = str(dmy) + ' ' + hm
            userName = request.user.username
        db.logInfo.insert({
            'username': userName,
            'login_datetime': dateTime,
            'login': False
        })

    def auth_view(self, request, auth, db, datetime):
        username = request.POST.get('username_name', '')
        password = request.POST.get('password_name', '')
        user = auth.authenticate(username=username, password=password)
        dmy = datetime.datetime.now().strftime("%d/%m/%y")
        hm = datetime.datetime.now().strftime("%H:%M")
        if user is not None:
            auth.login(request, user)
            db.logInfo.insert({
                'username': username,
                'login_datetime': str(dmy) + ' ' + hm,
                'login': True
            })
        return user
