var serverName;

var tables;

var src;

var desktop;

var socket;

var MIMPATH;

var index = 0;

var serverTableStr;

var serverNameTablesSocket = {};

var serverNameTableObjects = {};

var serverNameTablesPort = {};

var serverNameTablesIpofServer = {};

var socket_obj;

var gridCount = 0;

var keepServeractivate = [];

var arrIPofserver = {};

var arrcreateKeyValueForGoip = {};

var tableNodePortarr = {};

var win;

var gridMeM = {};

var simHistory = {};

var message_all =''

var winIddic = {};

Ext.define("MyDesktop.frameworkGridEngine.GridEngine", {
    extend: "Ext.ux.desktop.Module",
    id: "grid-win",
    requires: [ "Ext.button", "Ext.JSON", "Ext.Msg", "Ext.Ajax" ],
    init: function() {
        serverName = this.serverName;
        tables = this.tables;
        src = this.src;
        desktop = this.desktop;
        win = this.win;
        MIMPATH = this.MIMPATH;

        function onButtonClick(btn) {
            Ext.example.msg("Button Click", 'You clicked the "{0}" button.', btn.text);
        }
        function onItemClick(item) {
            Ext.example.msg("Menu Click", 'You clicked the "{0}" menu item.', item.text);
        }
        var butt = new Ext.button.Split({
            text: "Split Button",
            handler: onButtonClick,
            tooltip: {
                text: "This is a an example QuickTip for a toolbar item",
                title: "Tip Title"
            },
            iconCls: "blist",
            menu: {
                items: [ {
                    text: "<b>Bold</b>",
                    handler: onItemClick
                }, {
                    text: "<i>Italic</i>",
                    handler: onItemClick
                }, {
                    text: "<u>Underline</u>",
                    handler: onItemClick
                } ]
            }
        });
        if (gridCount == 0) {
            try {
                Ext.create("Ext.Window", {
                    id: "serverTableStr",
                    title: serverName
                });
            } catch (err) {
                Ext.log("dublicate id not allowed by sencha, that is why skipping");
            }
            try {
                Ext.create("Ext.Window", {
                    id: "Console_Refill_win", 
                    title:''
                });
            } catch (err) {
                Ext.log("dublicate id not allowed by sencha, that is why skipping");
            }
            try {
                Ext.create("Ext.Window", {
                    id: "refillPanelStr",
                    title: ''
                });
            } catch (err) {
                Ext.log("dublicate id not allowed by sencha, that is why skipping");
            }
            try {
                Ext.create("Ext.Window", {
                    id: "ForshowDevice",
                    title: serverName
                });
            } catch (err) {
                Ext.log("dublicate id not allowed by sencha, that is why skipping");
            }
            try {
                Ext.create("Ext.Window", {
                    id: "checkServer",
                    title: "checkServer"
                });
            } catch (err) {
                Ext.log("dublicate id not allowed by sencha, that is why skipping");
            }
        }
        Ext.log("gridcount", gridCount);

        var stats = new MyDesktop.frameworkGridEngine.GridTableStatistics()
        Ext.Ajax.request({
            url: MIMPATH + "/server/" + serverName + "/table/" + tables + "?option=getModel",
            success: function(response) {
                var tableObjects = Ext.JSON.decode(response.responseText)["tableObjects"];

                try{
                    tableObjects['username']
                }catch(r){
                    Ext.Msg.alert('status', serverName + ' down, try after sometime');
                    return
                }

                logg('getuserName:'+getuserName)
                Ext.getCmp("ForshowDevice")["title"] = serverName;
                serverTableStr = src.windowId + "_" + getuserName;
                Ext.getCmp("serverTableStr")["title"] = serverTableStr ;

                // storing the table objects globally
                serverNameTableObjects[Ext.getCmp("serverTableStr")["title"]] = tableObjects;


                serverNameTableObjects[Ext.getCmp("serverTableStr")["title"]]['username'] = getuserName

                var menuFeaturesItem = serverNameTableObjects[Ext.getCmp("serverTableStr")["title"]]["menuFeaturesItem"];
                logg('menuFeaturesItem:', menuFeaturesItem)
                try {
                    if(menuFeaturesItem[0].indexOf("KeyError") > -1){
                        //Ext.Msg.alert("Error",menuFeaturesItem[0])
                    }
                } catch (err) {

                }
                
                try {
                    var checkServer = true;
                    var IPofserver = tableObjects["give_IP_TO_nodeserver"];
                    
                } catch (err) {
                    Ext.getCmp("pwinStatus" + Ext.getCmp("serverTableStr")["title"]).updateText('<span style="color:orange"><b>text</b><span>'.replace('text', serverName + ' down try later'))
                    //Ext.Msg.alert("Status", "Server down, try later".replace("Server", serverName));
                    return;
                }
                
                var createKeyValueForGoip = getuserName + "_" + serverName;
                var aufladenIP = serverNameTableObjects[Ext.getCmp("serverTableStr")["title"]]["aufladenip"];
                var mouseOverFunction = serverNameTableObjects[Ext.getCmp("serverTableStr")["title"]]["mouseOverFunction"];
                var htmlWinDimension = serverNameTableObjects[Ext.getCmp("serverTableStr")["title"]]["htmlWinDimension"];
                var username = serverNameTableObjects[Ext.getCmp("serverTableStr")["title"]]["username"];
                var ForRefillBalanceWindowSIM_IP = serverNameTableObjects[Ext.getCmp("serverTableStr")["title"]]["ForRefillBalanceWindowSIM_IP"];
                var tbarInsertBool = serverNameTableObjects[Ext.getCmp("serverTableStr")["title"]]["tbarInsert"];
                // FOR ZOIPER
                var prefix = serverNameTableObjects[Ext.getCmp("serverTableStr")["title"]]["prefix"];
                var allowed_numbers = serverNameTableObjects[Ext.getCmp("serverTableStr")["title"]]["allowed_numbers"];
                var passCall = serverNameTableObjects[Ext.getCmp("serverTableStr")["title"]]["passCall"];
                var successBalanceRefillArray;
                
                logg('ForRefillBalanceWindowSIM_IP_GRID', ForRefillBalanceWindowSIM_IP)
                if (1) {
                    Ext.log({
                        msg: "tableObjects-------------",
                        dump: tableObjects
                    });
                    if (mouseOverFunction) {
                        var mouseDown = [];
                        document.body.onmousedown = function() {
                            if (mouseDown.length > 1) {
                                var newTime = new Date();
                                if (newTime - mouseDown[0] > 20 * 60 * 1e3) {
                                    window.location.href = MIMPATH + "/logout?logOutTime=" + newTime.getDate() + "/" + newTime.getMonth() + "/" + newTime.getFullYear() + " " + newTime.getHours() + ":" + newTime.getMinutes() + "&username=" + username;
                                } else {
                                    mouseDown[0] = new Date().getTime();
                                }
                            } else {
                                mouseDown.push(new Date().getTime());
                            }
                        };
                    }

                    logg('serverNameTablesSocket',serverNameTablesSocket)


                    if (!(serverTableStr in serverNameTablesSocket)) {
                        Ext.log("Debug:", tableObjects["debug"]);
                        arrIPofserver[Ext.getCmp("serverTableStr")["title"]] = IPofserver;

                        arrcreateKeyValueForGoip[Ext.getCmp("serverTableStr")["title"]] = serverNameTableObjects[Ext.getCmp("serverTableStr")["title"]]["username"] + "_" + serverName;

                        serverNameTablesPort[Ext.getCmp("serverTableStr")["title"]] = tableObjects["port"]

                        logg('tableNodePortarr',tableNodePortarr)

                        var reconnectBool = true
                        if (tableObjects["debug"]) {
                            
                            //if (!(tableObjects["port"]+1 in tableNodePortarr)) {

                                logg('port FOUND IN tableNodePortarr **********************************************')
                                if(document.URL.split("//")[0] =="http:"){

                                    serverNameTablesSocket[Ext.getCmp("serverTableStr")["title"]] = io.connect(
                                        "http://" + document.URL.split("//")[1].split("/")[0].split(":")[0] + ":" + tableObjects["portdebug"], {
                                        resource: "socket.io",
                                        "force new connection": true,
                                        reconnect: reconnectBool
                                    });
                                }else{
                                    port_https=String(parseInt(tableObjects["portdebug"])+1)
                                    serverNameTablesSocket[Ext.getCmp("serverTableStr")["title"]] = io.connect(
                                        "https://" + document.URL.split("//")[1].split("/")[0].split(":")[0] + ":" + port_https, {
                                        resource: "socket.io",
                                        "force new connection": true,
                                        reconnect: reconnectBool
                                    });
                                }

                                tableNodePortarr[tableObjects["port"]+1] = serverNameTablesSocket[Ext.getCmp("serverTableStr")["title"]]
                            //}else{
                            //    serverNameTablesSocket[Ext.getCmp("serverTableStr")["title"]] = tableNodePortarr[tableObjects["port"]+1]

                            //}
                        } else {
                           
                            //if (!(tableObjects["port"]+1 in tableNodePortarr)) {
                                if(document.URL.split("//")[0] =="http:"){
                                    serverNameTablesSocket[Ext.getCmp("serverTableStr")["title"]] = io.connect(
                                        "http://" + document.URL.split("//")[1].split("/")[0] + ":" + tableObjects["port"], {
                                        resource: "socket.io",
                                        "force new connection": true,
                                        reconnect: reconnectBool
                                    });
                                }else{
                                    port_https_prod=String(parseInt(tableObjects["port"])+1)
                                    serverNameTablesSocket[Ext.getCmp("serverTableStr")["title"]] = io.connect(
                                        "https://" + document.URL.split("//")[1].split("/")[0] + ":" + port_https_prod, {
                                        resource: "socket.io",
                                        "force new connection": true,
                                        reconnect: reconnectBool
                                    });
                                }
                                tableNodePortarr[tableObjects["port"]+1] = serverNameTablesSocket[Ext.getCmp("serverTableStr")["title"]]
                            //}else{
                            //    serverNameTablesSocket[Ext.getCmp("serverTableStr")["title"]] = tableNodePortarr[tableObjects["port"]+1]

                            //}
                        }
                        index += 1;
                    }
                    /*try {
                        serverNameTablesSocket[Ext.getCmp("serverTableStr")["title"]].on("test_mssg", function(message) {
                            Ext.Msg.alert("Info Admin", message);
                        });

                    } catch (err) {}*/
                    if (checkServer) {
                        var model = tableObjects["model"];
                        var columns = tableObjects["columns"];

                        var model_user = tableObjects["model_user"];
                        var columns_user = tableObjects["columns_user"];

                        var filters = {
                            ftype: "filters",
                            encode: true,
                            local: false,
                            enableKeyEvents: true,
                            autoReload: false
                        };

                        var store_obj = new MyDesktop.frameworkGridEngine.StoreTable({
                            serverName: serverName,
                            tables: tables,
                            model: model,
                            store: store,
                            MIMPATH: MIMPATH
                        });
    /*
    var store_obj_user = new MyDesktop.frameworkGridEngine.StoreUserTable({
        serverName: serverName,
        tables: tables,
        model: model_user,
        store: store,
        columns: columns_user,
        MIMPATH: MIMPATH
    });*/

                        serverNameTablesIpofServer[serverName] = IPofserver;
                        try{


                            var grid_Obj = new MyDesktop.frameworkGridEngine.Grid({
                                username: username,
                                serverNameTableObjects:serverNameTableObjects,
                                menuFeaturesItem:serverNameTableObjects[Ext.getCmp("serverTableStr")["title"]] ,
                                store: store_obj.store,
                                columns: columns,
                                filters: filters,
                                grid: grid,
                                prefix:prefix,
                                allowed_numbers:allowed_numbers,
                                passCall:passCall,
                                windowID: serverTableStr,
                                socket: serverNameTablesSocket[Ext.getCmp("serverTableStr")["title"]],
                                serverNameTablesSocket: serverNameTablesSocket,
                                serverNameTablesPort:serverNameTablesPort,
                                arrIPofserver: arrIPofserver,
                                serverNameTablesIpofServer: serverNameTablesIpofServer,
                                arrcreateKeyValueForGoip: arrcreateKeyValueForGoip,
                                tables: tables,
                                MIMPATH: MIMPATH,
                                serverName: serverName,
                                stats:stats,
                                tbarInsertBool:serverNameTableObjects[Ext.getCmp("serverTableStr")["title"]]["tbarInsertBool"]
                            });
                        }catch (err){
                            logg(err)
                        }
                        logg('columns', columns)
                        logg('columns', model)

                        columns[0]["items"][0]["handler"] = function(grid, rowIndex, colIndex) {
                            /*isOnline(function() {
                                Ext.Msg.alert("status", "Internet connection is lost, please refresh the page");
                            }, function() {});*/
                            rec = Ext.getCmp("grid_" + Ext.getCmp("serverTableStr")["title"]).getStore().getAt(rowIndex);
                            /*Ext.Ajax.request({
                                url: MIMPATH + "/server/" + serverName + "/table/" + tables + "?UPDATEID=ID&id_row="+rec.get("id")+'&body_row=None'
                            });*/
                            Ext.Msg.confirm("Delete", "Do you want to delete id: " + rec.get("id") + " ?", function(btn) {
                                if (btn == "yes") {
                                    Ext.getCmp("grid_" + Ext.getCmp("serverTableStr")["title"]).getStore().removeAt(rowIndex);
                                }
                            });
                        };
                        /* this class contains all the function for refillPanel*/
                        var fRBWF = new MyDesktop.frameworkGridEngine.gridFeatures.refillPanel.RefillBalanceWindowFUNC();
                        var fRBWF_handler = new MyDesktop.frameworkGridEngine.gridFeatures.refillPanel.RefillBalanceWindowFUNC_handler();

                        var refillPanelBool = false
                        var columnindex = 0;
                        columns[1]["items"][0]["handler"] = function(grid, rowIndex, colIndex) {

                            
                            rec = Ext.getCmp("grid_" + Ext.getCmp("serverTableStr")["title"]).getStore().getAt(rowIndex);
                            logg('columnindex ------------------------ ' + columnindex)
                            if (columnindex == 0) {
                                new MyDesktop.frameworkSocket.SocketRecv({
                                    socket: serverNameTablesSocket
                                });
                            }
                            Ext.Ajax.request({////
                                url: MIMPATH + "/server/" + Ext.getCmp("serverTableStr")["title"].split('/')[0] + "/table/" + Ext.getCmp("serverTableStr")["title"].split('/')[1].split('_')[0] + "/refill/" + rec.get("number")[0],
                                success: function(response) {

                                   if(!(rec.get("channel") in simHistory)){
                                        simHistory[rec.get("channel")] = ''
                                   }
                                   for (channel in simHistory){
                                        if(Object.keys(simHistory).length >2){
                                            simHistory = {}
                                        }else{

                                            if(channel!=rec.get("channel")){
                                                logg('channelllllll' + channel)
                                                try{
                                                    serverNameTablesSocket[Ext.getCmp("serverTableStr")["title"]].emit("subscribe_balanceRefill", "UNSUBSCRIBE " + channel);

                                                }catch(r){}
                                                
                                            }
                                        }
                                    }
                                    logg('CHANNEL :' , simHistory)
                                    Ext.getCmp('Console_Refill_win')["title"] = 'Console' + rec.get("channel");

                                    //logg('1kkkk',serverNameTablesSocket[Ext.getCmp("serverTableStr")["title"]].io.engine.id)
                                    if (Ext.JSON.decode(response.responseText)["klass"] == "c1") {
                                        new MyDesktop.frameworkGridEngine.gridFeatures.RefillWindow({
                                            MIMPATH: MIMPATH,
                                            serverNameTablesSocket: serverNameTablesSocket,
                                            //socket: serverNameTablesSocket[Ext.getCmp("serverTableStr")["title"]],
                                            response: response,
                                            rec: rec,
                                            serverName: Ext.getCmp("serverTableStr")["title"].split('/')[0],
                                            tables: Ext.getCmp("serverTableStr")["title"].split('/')[1].split('_')[0],
                                            createKeyValueForGoip: arrcreateKeyValueForGoip[Ext.getCmp("serverTableStr")["title"]],
                                            IPofserver: arrIPofserver[Ext.getCmp("serverTableStr")["title"]],
                                            aufladenIP: serverNameTableObjects[Ext.getCmp("serverTableStr")["title"]]["aufladenip"],
                                            htmlWinDimension: serverNameTableObjects[Ext.getCmp("serverTableStr")["title"]]["htmlWinDimension"],
                                            gridTable: grid_Obj

                                        });
                                    } else if (Ext.JSON.decode(response.responseText)["klass"] == "c3") {
                                        new MyDesktop.frameworkGridEngine.gridFeatures.RefillBalanceWindow({
                                            MIMPATH: MIMPATH,
                                            serverNameTablesSocket: serverNameTablesSocket,
                                            columnindex:columnindex,
                                            //socket: serverNameTablesSocket[Ext.getCmp("serverTableStr")["title"]],
                                            response: response,
                                            rec: rec,
                                            serverName: serverName,
                                            tables: tables,
                                            createKeyValueForGoip: arrcreateKeyValueForGoip[Ext.getCmp("serverTableStr")["title"]],
                                            IPofserver: arrIPofserver[Ext.getCmp("serverTableStr")["title"]],
                                            aufladenIP: serverNameTableObjects[Ext.getCmp("serverTableStr")["title"]]["aufladenip"],
                                            htmlWinDimension: serverNameTableObjects[Ext.getCmp("serverTableStr")["title"]]["htmlWinDimension"],
                                            //gridTable: grid_Obj,
                                            ForRefillBalanceWindowSIM_IP: serverNameTableObjects[Ext.getCmp("serverTableStr")["title"]]["ForRefillBalanceWindowSIM_IP"],
                                            fRBWF:fRBWF,
                                            fRBWF_handler:fRBWF_handler,
                                            stats:stats
                                        });
                                    } else if (Ext.JSON.decode(response.responseText)["klass"] == "c2") {
                                        new MyDesktop.frameworkGridEngine.gridFeatures.RefillWindow2({
                                            MIMPATH: MIMPATH,
                                            socket: serverNameTablesSocket[Ext.getCmp("serverTableStr")["title"]],
                                            response: response,
                                            rec: rec,
                                            serverName: serverName,
                                            tables: tables,
                                            createKeyValueForGoip: arrcreateKeyValueForGoip[Ext.getCmp("serverTableStr")["title"]],
                                            IPofserver: arrIPofserver[Ext.getCmp("serverTableStr")["title"]],
                                            aufladenIP: serverNameTableObjects[Ext.getCmp("serverTableStr")["title"]]["aufladenip"]
                                        });
                                    }
                                    columnindex += 1;
                                },
                                failure: function() {
                                    Ext.getCmp("pwinStatus" + Ext.getCmp("serverTableStr")["title"]).updateText(
                                        '<span style="color:red"><b>text</b><span>'.replace('text', "No data recieved from django for Refill")
                                    )
                                    //Ext.Msg.alert("status", "No data recieved from django for Refill");
                                }
                            });
                        };

         

                    // var pwin;
                    // internetConnectivitySocket.on('connect', function () {
                    //     try{


                    //         /*pwin.close()
                    //         var alert2 = Ext.Msg.show({
                    //             title: 'ALERT!!!',
                    //             msg: ' Your Internet is now <span style="color:green"><b>'  + 'connected</b></span>',
                    //             buttons: Ext.Msg.OK,
                    //             icon: Ext.Msg.WARNING,
                    //             cls: Ext.Msg.styleHtmlCls
                    //         });
                    //         Ext.getCmp("grid_" + Ext.getCmp("serverTableStr")["title"]).store.loadPage(1);
                    //         setTimeout(function(){
                    //             alert2.close()
                    //         }, 5000)*/
                    //         Ext.getCmp("pwinStatus" + Ext.getCmp("serverTableStr")["title"]).updateText(
                    //             '<span style="color:orange"><b>text</b><span>'.replace(
                    //                 'text', ' Your Internet is now <span style="color:green"><b>'  + 'connected</b></span>'
                    //             )
                    //         )

                    //         Ext.getCmp("grid_" + Ext.getCmp("serverTableStr")["title"]).store.loadPage(1);

                    //         Ext.getCmp("pwin" + Ext.getCmp("serverTableStr")["title"]).reset();
                    //         Ext.getCmp("pwin" + Ext.getCmp("serverTableStr")["title"]).updateText(
                    //             Ext.getCmp("serverTableStr")["title"].split('/')[0] + ' Node1 is <span style="color:green"><b>'  + 'connected</b></span>'
                    //         );
                    //     }catch (e){
                    //         //
                    //     }
                    // });
                    
                    // internetConnectivitySocket.on('disconnect', function () {
                    //     try{
                    //         Ext.getCmp("pwin" + Ext.getCmp("serverTableStr")["title"]).wait(
                    //             {
                    //                 text: Ext.getCmp("serverTableStr")["title"].split('/')[0] + ' Node1 is disconnected, \n reconnecting please wait ...'
                    //             }
                    //         );
                    //         /*pwin = Ext.create(
                    //             "Ext.window.Window", 
                    //             {
                    //                 width: 300,
                    //                 header: false,
                    //                 border: false,
                    //                 layout: "fit",
                    //                 modal:true,
                    //                 items:[{
                    //                     xtype:"progressbar",
                    //                     cls:'custom',
                    //                     text: ' Your Internet is <span style="color:red"><b>'  + 'diconnected</b></span><br> please wait for your internet connection !!!'  
                    //                 }]

                    //         });
                    //         pwin.show()*/

                    //         Ext.getCmp("pwinStatus" + Ext.getCmp("serverTableStr")["title"]).updateText(
                    //             '<span style="color:orange"><b>text</b><span>'.replace(
                    //                 'text', ' Your Internet is <span style="color:red"><b>'  + 'diconnected</b></span><br> please wait for your internet connection !!!'
                    //             )
                    //         )
                    //         Ext.getCmp("grid_" + Ext.getCmp("serverTableStr")["title"]).store.loadData([],false);
                    //     }catch (e){
                    //         //
                    //     }
                    // });



                    
                    ////////////////////////////////////////////////////////// SOCKET DICONNECT HANDELLING 
                    /*node51.on('connect', function () {


                        logg('reconnected ',serverNameTablesSocket[Ext.getCmp("serverTableStr")["title"]].io.engine.id)
                        try{
                            Ext.getCmp("pwin" + Ext.getCmp("serverTableStr")["title"]).reset();
                            Ext.getCmp("pwin" + Ext.getCmp("serverTableStr")["title"]).updateText(serverName + ' Node1 is now <span style="color:green"><b>'  + 'connected</b></span>');
                            pwin.close()
                        }catch (e){
                            //
                        }
                    });

                    node51.on('disconnect', function () {
                        try{
                         Ext.getCmp("pwin" + Ext.getCmp("serverTableStr")["title"]).wait({text: serverName + ' Node1 is disconnected, \n reconnecting please wait ...'});
                        }catch (e){
                            //
                        }
                        //logg('socket is disconnected')
                    });*/
                    serverNameTablesSocket[Ext.getCmp("serverTableStr")["title"]].on('connect', function () {


                        logg('reconnected ',serverNameTablesSocket[Ext.getCmp("serverTableStr")["title"]].io.engine.id)
                        try{
                            Ext.getCmp("pwin" + Ext.getCmp("serverTableStr")["title"]).reset();
                            Ext.getCmp("pwin" + Ext.getCmp("serverTableStr")["title"]).updateText(serverName + ' Node1 is now <span style="color:green"><b>'  + 'connected</b></span>');
                            //pwin.close()
                        }catch (e){
                            //
                        }
                    });

                    serverNameTablesSocket[Ext.getCmp("serverTableStr")["title"]].on('disconnect', function () {
                        try{
                         Ext.getCmp("pwin" + Ext.getCmp("serverTableStr")["title"]).wait({text: serverName + ' Node1 is disconnected, \n reconnecting please wait ...'});
                        }catch (e){
                            //
                        }
                        //logg('socket is disconnected')
                    });
                    ////////////////////////////////////////////////////////// SOCKET DICONNECT HANDELLING 
                    ///
                        new MyDesktop.frameworkSocket.SocketRecv_modul({
                            socket: serverNameTablesSocket
                        });
                        //var stats= new MyDesktop.frameworkGridEngine.GridTableStatistics()
                        //new MyDesktop.frameworkGridEngine.ChartStat()
                        //
                        //
                        var gBar = []
                        if(tables=='cost'){
                            gBar.push("-");
                            gBar.push({
                                xtype: "form",
                                id: "statusform"+Ext.getCmp("serverTableStr")["title"],
                                items: [ {
                                    xtype: "numberfield",
                                    fieldLabel: '<span style="color:red"><b>Value</b><span>',
                                    name: "numField",
                                    blankText: "number is required"
                                } ]
                            });
                            gBar.push({
                                xtype: "button",
                                text: '<span style="color:red"><b>Find SIMS</b><span>',
                                pressed: true,
                                enableToggle: true,
                                toggleHandler: function(btn, pressed) {
                                    Ext.Ajax.request({
                                        url: MIMPATH + "/server/" + Ext.getCmp("serverTableStr")["title"].split('/')[0] + "/table/" + Ext.getCmp("serverTableStr")["title"].split('/')[1].split('_')[0] + "?GETSIMMFREE=GETSIMmree&value=" + Ext.getCmp("statusform"+Ext.getCmp("serverTableStr")["title"]).getForm("form").findField("numField").getValue(),
                                        success: function(response) {
                                            
                                            messg = Ext.JSON.decode(response.responseText)["message"]
                                            var mfreeT = []
                                            mfreeT.push('<table style="width:100%">')
                                            mfreeT.push('<tr>')
                                            mfreeT.push('<th>')
                                            mfreeT.push('<span style="color:green"><b>channel<span>')
                                            mfreeT.push('</th>')
                                            mfreeT.push('<th>')
                                            mfreeT.push('<span style="color:green"><b>mfree<span>')
                                            mfreeT.push('</th>')
                                            mfreeT.push('</tr>')
                                            for (var val in messg){
                                                mfreeT.push('<tr>')

                                                mfreeT.push('<th>')
                                                mfreeT.push(messg[val]['channel'])
                                                mfreeT.push('</th>')

                                                mfreeT.push('<th>')
                                                mfreeT.push(messg[val]['mfree'])
                                                mfreeT.push('</th>')

                                                mfreeT.push('</tr>')                                            
                                            }
                                            mfreeT.push('</table>')
                                            var mfreehtml = mfreeT.join('')  
                                            Ext.getCmp("fieldMfree"+Ext.getCmp("serverTableStr")["title"]).update(mfreehtml)
                                            //logg('mfreehtml', mfreehtml)
                                            //logg("SIMS OUT: ", messg);

                                        }
                                    });
                                }
                            });
                        }

                        var winitem = [];
                        winitem.push(Ext.getCmp("grid_" + Ext.getCmp("serverTableStr")["title"]))
                        winitem.push({                
                            xtype: "panel",
                            id: 'statPanel'+Ext.getCmp("serverTableStr")["title"], 
                            region: 'south',
                            height: 100,
                            split: true, // enable resizing
                            collapsible: true,   // make collapsible
                            collapsed : true,
                            margins: '0 5 5 5',
                            html:'<html><body><center><table border="1" style="width:800px;border:1px solid black;padding:5px;border-spacing:15px" >Number of active sims<tr><td align="center">0 (active=0)</td><td align="center">0 (active=1)</td><td align="center">0 (active=3)</td><td align="center">0 (active=4)</td><td align="center">0 (active=5)</td><td align="center">0 (active=6)</td><td align="center">0 (active=7)</td><td align="center">0 (active=8)</td><td align="center">0 (active=9)</td></tr></table></center></body></html>'
                
                        })
                        winitem.push({
                            region:'east',
                            layout: 'fit',
                            title:"GOIP WIN",
                            id: "GOIP"+Ext.getCmp("serverTableStr")["title"],
                            xtype: 'panel',
                            margins: '5 0 0 5',
                            width: 500,
                            collapsible: true,   // make collapsible
                            collapsed : true,
                            autoScroll:true,
                            listeners:{
                                expand:function(){
                                    message_all = ''
                                    Ext.getCmp("GOIP"+Ext.getCmp("serverTableStr")["title"]).update('')
                                    try{
                                        serverNameTablesSocket[Ext.getCmp("serverTableStr")["title"]].emit("send_allCommand",  "sendallCommand___" + IPofserver);
                                    }catch(r){}
                                    //
                                }
                            },
                            /*
                            items:[{
                                //title: "Get SIM Cards for mfree < value ",
                                xtype:"panel",
                                height: 700,
                                animCollapse: false,
                                border: false,
                                maximized:true,
                                hideMode: "offsets",
                                layout: {
                                    type: "hbox",
                                    align: "stretch"
                                },
                                items:[ {
                                    flex: 4,
                                    xtype: "container",
                                    layout: {
                                        type: "vbox",
                                        align: "stretch"
                                    },
                                    items: [ 
                                        xtype:"panel",
                                        flex:1,
                                        scope:this,
                                        tbar:gBar
                                    },
                                    {
                                        flex:1,
                                        xtype:'panel',
                                        autoScroll:true,
                                        id:"fieldMfree"+Ext.getCmp("serverTableStr")["title"],
                                        html:""
                                    } ]
                                }]
                            }]*/
                            //html:'<p>&nbsp;</p><h2 style="text-align: center;"><span style="font-size: 14pt;">What is new in this framework?</span></h2><table style="height: 191px; width: 457px; background-color: #cccc99;" border="0" align="center"><tbody><tr><td><p style="font-size: 11.3333330154419px;"><span style="font-size: 10pt;">(1) on your left hand side screen, you will see a show device window.In this window, you will see the status of SIM CARDS. When you click&nbsp;REFRESH, it will refresh you SHOW DEVICES.</span></p><p style="font-size: 11.3333330154419px;"><span style="font-size: 10pt;">(2) When u select any row in the table, the sim card on the left window&nbsp;will we highlighted, which gives exactly the information about the SIM CARD (Real Time)</span></p><p style="font-size: 11.3333330154419px;"><span style="font-size: 10pt;">(3) Sorting features are imporved&nbsp;</span></p></td></tr></tbody></table><p>&nbsp;</p><p style="text-align: center;"><span style="color: #339966; font-size: 18pt;">ENJOY&nbsp;</span></p><p style="text-align: center;"><span style="color: #339966; font-size: 18pt;">Dont Forget to Smile &nbsp;:)&nbsp;</span></p>'
                        })

                        winitem.push({
                            region:'east',
                            layout: 'fit',
                            title:"Refill Panel",
                            id: "RefillPanel"+Ext.getCmp("serverTableStr")["title"],
                            xtype: 'panel',
                            //margins: '5 0 0 5',      
                            listeners:{
                                collapse:function(){
                                    logg('RefillPanel collapsed')
                                    try{
                                        Ext.getCmp("Console"+rec.get("channel")).update("");
                                    }catch(e){
                                        logg("Console +++ something happend")
                                    } 
                                    // clearing all the subscriptions
                                    try{
                                        serverNameTablesSocket[Ext.getCmp("serverTableStr")["title"]].emit("DESTROY_IP", "IP_destroy_token#" + createKeyValueForGoip + "___" + IPofserver);
                                        serverNameTablesSocket[Ext.getCmp("serverTableStr")["title"]].emit("subscribe_balanceRefill", "UNSUBSCRIBE " + rec.get("channel").split("/")[1]);
                                    }catch(e){} 
                                }
                            },                                        
                            width: 420,
                            collapsible: true,   // make collapsible
                            collapsed : true,
                            autoScroll:true, //
                            items:[{
                                //title: "Get SIM Cards for mfree < value ",
                                xtype:"panel",
                                height: 300,
                                animCollapse: false,
                                border: false,
                                maximized:true,
                                hideMode: "offsets",
                                layout: {
                                    type: "hbox",
                                    align: "stretch"
                                },
                                items:[ {
                                    flex: 4,
                                    xtype: "container",
                                    id: "RefillPanelCont"+Ext.getCmp("serverTableStr")["title"],
                                    items: []
                                }]
                            }]
                            //html:'<p>&nbsp;</p><h2 style="text-align: center;"><span style="font-size: 14pt;">What is new in this framework?</span></h2><table style="height: 191px; width: 457px; background-color: #cccc99;" border="0" align="center"><tbody><tr><td><p style="font-size: 11.3333330154419px;"><span style="font-size: 10pt;">(1) on your left hand side screen, you will see a show device window.In this window, you will see the status of SIM CARDS. When you click&nbsp;REFRESH, it will refresh you SHOW DEVICES.</span></p><p style="font-size: 11.3333330154419px;"><span style="font-size: 10pt;">(2) When u select any row in the table, the sim card on the left window&nbsp;will we highlighted, which gives exactly the information about the SIM CARD (Real Time)</span></p><p style="font-size: 11.3333330154419px;"><span style="font-size: 10pt;">(3) Sorting features are imporved&nbsp;</span></p></td></tr></tbody></table><p>&nbsp;</p><p style="text-align: center;"><span style="color: #339966; font-size: 18pt;">ENJOY&nbsp;</span></p><p style="text-align: center;"><span style="color: #339966; font-size: 18pt;">Dont Forget to Smile &nbsp;:)&nbsp;</span></p>'
                        })
                        if(tables=='user_actions'){
                            winitem.push({xtype:'user_detail_charts'})
                            winitem.push({xtype:'user_manager'})
                        }
                        winitem.push({
                            // xtype: 'panel' implied by default
                            region:'west',
                            layout: 'fit',
                            title:"Show Devices",
                            id: 'showdevices'+serverTableStr,
                            xtype: 'panel',
                            margins: '5 0 0 5',
                            width: 400,
                            items: [ {
                                xtype: "panel",
                                layout: {
                                    type: "vbox",
                                    align: "stretch",
                                    padding: 1
                                },
                                width: 400,
                                items: [ {
                                    xtype: "panel",
                                    autoScroll: true,
                                    id: "panelshowdev" + Ext.getCmp("serverTableStr")["title"],
                                    width: 100,
                                    items: [ {
                                        xtype: "button",
                                        width: 100,
                                        text: '<span style="color:orange"><b>Refresh</b><span>',
                                        handler: function() {
                                            serverNameTablesSocket[Ext.getCmp("serverTableStr")["title"]].emit("send_showdevicesTable", "show devall" + "___" + serverNameTablesIpofServer[Ext.getCmp("ForshowDevice")["title"]] + "___" + username);
                                        
                                        }
                                    } ],
                                    flex: 4
                                } ]
                            } ],
                            listeners:{

                                expand:function(p, eOpts ){
                                    

                                    var functionShowD = function(panelId) {
                                        var p = Ext.getCmp("progressbarShowDevice" + Ext.getCmp("serverTableStr")["title"]);
                                        p.wait({
                                            interval: 300,
                                            duration: 5e4,
                                            increment: 5,
                                            scope: this,
                                            fn: function() {
                                                p.updateText("Done!");
                                            }
                                        });
                                        try{
                                            serverNameTablesSocket[Ext.getCmp("serverTableStr")["title"]].emit(
                                                "send_showdevicesTable", "show devall" + "___" + serverNameTablesIpofServer[Ext.getCmp("ForshowDevice")["title"]] + "___" + username
                                            );
                                        }catch(r){}
                                        
                                        setTimeout(function(){ 
                                            new MyDesktop.frameworkSocket.SocketRecvShowTable({
                                                socket: serverNameTablesSocket,
                                                username: username,
                                                serverNameTablesIpofServer: serverNameTablesIpofServer,
                                                panelId:panelId
                                            });
                                        },1000)
                                        logg("The Goip Status->" + Ext.getCmp('sockrev'))
                                        //send_showdevicesTable_Count += 1;
                                    };
                                    functionShowD(this.id)
        
                                }

                            },
                            collapsible: true,   // make collapsible
                            collapsed : true
                        })

                        if (!win) {
                            try{
                                
                                 win = desktop.createWindow({
                                    id: serverTableStr,
                                    title: serverTableStr,
                                    //layout: "fit",
                                    layout:"border",
                                    expandOnShow: true,
                                    width: 1300,
                                    height: 700,
                                    animCollapse: false,
                                    constrainHeader: true,
                                    headerPosition: "top",
                                    maximized:true,
                                    closable: true,
                                    listeners: {
                                        destroy: function() {
                                            try{
                                                delete winIddic[Ext.getCmp("serverTableStr")["title"]]
                                                serverNameTablesSocket[Ext.getCmp("serverTableStr")["title"]].emit("DESTROY_IP", "IP_destroy_token#" + arrcreateKeyValueForGoip[Ext.getCmp("serverTableStr")["title"]] + "___" + arrIPofserver[Ext.getCmp("serverTableStr")["title"]]);

                                            
                                                logg('SERVER CLOSED:'+ Ext.getCmp("serverTableStr")["title"])
                                                serverNameTablesSocket[Ext.getCmp("serverTableStr")["title"]].disconnect();
                                                delete serverNameTablesSocket[Ext.getCmp("serverTableStr")["title"]];
                                            }catch(r){}
                                        },
                                        minimize: function() {
                                            try {
                                                serverNameTablesSocket[Ext.getCmp("serverTableStr")["title"]].emit("DESTROY_IP", "IP_destroy_token#" + arrcreateKeyValueForGoip[Ext.getCmp("serverTableStr")["title"]] + "___" + arrIPofserver[Ext.getCmp("serverTableStr")["title"]]);
                                            } catch (error) {}
                                        },
                                        activate: function(eOpts) {

                                            serverName = eOpts.id.split("/")[0];
                                            tables = eOpts.id.split("/")[1].split("_")[0];
                                            serverTableStr = eOpts.id;
                                            Ext.getCmp("ForshowDevice")["title"] = serverName;
                                            Ext.getCmp("serverTableStr")["title"] = serverTableStr;

                                            logg("  WIN GRID ACTIVATE " + serverName)

                                        }
                                    },
                                    //autoHeight: true,
                                    items: winitem
                                    /*{
                                            region:'east',
                                            layout: 'fit',
                                            title:"Wikipedia",
                                            xtype: 'panel',
                                            margins: '5 0 0 5',
                                            width: 600,
                                            collapsible: true,   // make collapsible
                                            collapsed : true,
                                            html:'<p>&nbsp;</p><h2 style="text-align: center;"><span style="font-size: 14pt;">What is new in this framework?</span></h2><table style="height: 191px; width: 457px; background-color: #cccc99;" border="0" align="center"><tbody><tr><td><p style="font-size: 11.3333330154419px;"><span style="font-size: 10pt;">(1) on your left hand side screen, you will see a show device window.In this window, you will see the status of SIM CARDS. When you click&nbsp;REFRESH, it will refresh you SHOW DEVICES.</span></p><p style="font-size: 11.3333330154419px;"><span style="font-size: 10pt;">(2) When u select any row in the table, the sim card on the left window&nbsp;will we highlighted, which gives exactly the information about the SIM CARD (Real Time)</span></p><p style="font-size: 11.3333330154419px;"><span style="font-size: 10pt;">(3) Sorting features are imporved&nbsp;</span></p></td></tr></tbody></table><p>&nbsp;</p><p style="text-align: center;"><span style="color: #339966; font-size: 18pt;">ENJOY&nbsp;</span></p><p style="text-align: center;"><span style="color: #339966; font-size: 18pt;">Dont Forget to Smile &nbsp;:)&nbsp;</span></p>'
                                        },{
                                            region:'east',
                                            layout: 'fit',
                                            title:"charts",
                                            xtype: 'panel',
                                            margins: '5 0 0 5',
                                            width: 600,
                                            collapsible: true,   // make collapsible
                                            collapsed : true,
                                            items:[Ext.getCmp('LineChartsWin')],
                                            listeners:{
                                                expand:function( p, eOpts ){
                                                    logg('Active History expanded')


                                                }
                                            }
                                        },*/
                                    
                                });                               
                            }catch (err){
                                logg('WIN ERROR ' + err)
                            }

                            store_obj.store.loadPage(1);
                            try{
                                if(!(win.id in winIddic)){
                                    winIddic[win.id] = ''
                                    win.show();
                                }
                            }catch(err){logg(err)}
                            
                        }
                    } else {
                        Ext.getCmp("pwinStatus" + Ext.getCmp("serverTableStr")["title"]).updateText('<span style="color:red"><b>text</b><span>'.replace('text', serverName + ' down'))
                        //Ext.Msg.alert("Status", "dum down".replace("dum", serverName));
                    }
                }
            },
            failure: function() {
                //Ext.getCmp("pwinStatus" + Ext.getCmp("serverTableStr")["title"]).updateText('<span style="color:red"><b>text</b><span>'.replace('text', serverName + ' down'))
                Ext.Msg.alert("Status", "dum down".replace("dum", serverName));;
            }
        });
        gridCount += 1;
    }
});
