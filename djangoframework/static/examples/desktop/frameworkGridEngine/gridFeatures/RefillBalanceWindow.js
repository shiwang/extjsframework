var serverNameTablesSocket;

var response;

var rec;

var createKeyValueForGoip;

var IPofserver;

var command_key;

var command;

var command_func;

var serverName;

var tables;

var aufladenIp;

var htmlWinDimension;

var ForRefillBalanceWindowSIM_IP;

var fRBWF;

var no_of_cards;

var stats;
var columnindex;

var interntDisconnection = true; 

var p;
Ext.define("MyDesktop.frameworkGridEngine.gridFeatures.RefillBalanceWindow", {
    extend: "Ext.ux.desktop.Module",
    init: function() {
        serverNameTablesSocket = this.serverNameTablesSocket;
        response = this.response;
        rec = this.rec;
        createKeyValueForGoip = this.createKeyValueForGoip;
        IPofserver = this.IPofserver;
        aufladenIp = this.aufladenIp;
        serverName = this.serverName;
        tables = this.tables;
        MIMPATH = this.MIMPATH;
        ForRefillBalanceWindowSIM_IP = this.ForRefillBalanceWindowSIM_IP;
        fRBWF = this.fRBWF;
        fRBWF_handler = this.fRBWF_handler;
        columnindex = this.columnindex;
        var html = "";
        var cmdStore;
        var sipStore;
        var simNumber;
        var func_key;
        var return_val_dic = {};
        stats = this.stats;


        Ext.log({
            msg: "RefillBalanceWindow"
        });

        // class containing all the functions


        htmlWinDimension = this.htmlWinDimension;
        if (htmlWinDimension["show"]) {
            html = htmlWinDimension["html"];
            widthInstruction = htmlWinDimension["width"];
            heightInstruction = htmlWinDimension["height"];
        } else {
            widthInstruction = 0;
            heightInstruction = 0;
        }
        Ext.define("State", {
            extend: "Ext.data.Model",
            fields: [{
                type: "string",
                name: "name"
            }]
        });
        function createStore() {
            command = Ext.JSON.decode(response.responseText)["command"];
            command_func = Ext.JSON.decode(response.responseText)["command_func"];
            cards_no = Ext.JSON.decode(response.responseText)["no_of_cards"];
            if(cards_no=='nothing'){
                no_of_cards = ''
            }else{
                no_of_cards = '<span style="color:black"><b>' + ' | Number of cards:'+ "</b></span>"+cards_no
                
                if(parseInt(cards_no) < 30){
                     no_of_cards = '<span style="color:black"><b>' + ' | Number of cards:'+ "</b></span>"+'<span style="color:red"><b>' + cards_no + ' [ critical, upload the cards ]'+ "</b></span>"
                }           
            }
            if (typeof command != "undefined") {
                command_key = Ext.JSON.decode(response.responseText)["command_key"];
                return Ext.create("Ext.data.Store", {
                    autoDestroy: true,
                    model: "State",
                    data: command
                });
            }
        }
        function checkSqlServer(serverName, refillWin, sending) {
            if (refillWin != "undefined") {
                Ext.getCmp("RefillPanelCont"+Ext.getCmp("serverTableStr")["title"]).add(
                    refillWin
                )
                Ext.getCmp("RefillPanelCont"+Ext.getCmp("serverTableStr")["title"]).doLayout();
                //refillWin.show();
            }
            if (sending != "undefined") {
                sending();
            }
            /*
            Ext.Ajax.request({
                url: MIMPATH + "/server/" + serverName + "/table/" + tables + "/refill/" + rec.get("number")[0] + "?option=checkServer",
                success: function(response) {
                    checkServer = Ext.JSON.decode(response.responseText)["checkServer"];
                    mysqlcheckServer = Ext.JSON.decode(response.responseText)["mysqlcheckServer"];
                    if (!mysqlcheckServer) {
                        Ext.Msg.alert("Status", "dum mysql is down, table cannot be updated".replace("dum", serverName));
                        Ext.getCmp("grid_" + Ext.getCmp("serverTableStr")["title"]).store.loadPage(1);
                    }
                    if (!checkServer) {
                        Ext.Msg.alert("Status", "Server down, refresh the table after some time".replace("Server", serverName));
                        Ext.getCmp("grid_" + Ext.getCmp("serverTableStr")["title"]).store.loadPage(1);
                    }
                    if ((checkServer==true) && (mysqlcheckServer==true) && refillWin != "undefined") {
                        refillWin.show();
                    }
                    if ((checkServer==true) && (mysqlcheckServer==true) && sending != "undefined") {
                        sending();
                    }
                    logg("checkserver_RefillBalanceWindow", checkServer);
                    logg("mysqlcheckServer_RefillBalanceWindow", mysqlcheckServer);
                }
            });*/
        }
        var sabKey;
        var idno;
        var countHide = 0;
        try{
            Ext.getCmp("Console"+rec.get("channel")).update("");
        }catch(e){

        }
        // clearing all the subscriptions
        try{
            serverNameTablesSocket[Ext.getCmp("serverTableStr")["title"]].emit("DESTROY_IP", "IP_destroy_token#" + createKeyValueForGoip + "___" + IPofserver);
            //serverNameTablesSocket[Ext.getCmp("serverTableStr")["title"]].emit("subscribe_balanceRefill", "UNSUBSCRIBE " + rec.get("channel").split("/")[1]);
        }catch(e){}

        Ext.getCmp("RefillPanelCont"+Ext.getCmp("serverTableStr")["title"]).removeAll()
        serverNameTablesSocket[Ext.getCmp("serverTableStr")["title"]].emit("DESTROY_IP", "IP_destroy_token#" + createKeyValueForGoip + "___" + IPofserver);
        
        Ext.getCmp("RefillPanelCont"+Ext.getCmp("serverTableStr")["title"]).add({
            //width: 420,
            height: 600 + heightInstruction,
            //modal: true,
            xtype:'panel',
            layout:"fit",
            border:true,
            title: '<span style="color:black"><b>tmp</b></span>'.replace('tmp',"Refill Panel with " + rec.get("channel")),
            id: "Refill Panel" + rec.get("channel"),
            listeners: {
                activate: function(win) {
                    logg('RefillBalanceWindow Activate function ')
                    ////////////////////////////// SOCKET DICONNECT HANDELLING FOR REFILLPANEL
                    serverNameTablesSocket[Ext.getCmp("serverTableStr")["title"]].on('connect', function () {
                        try{
 
                            serverNameTablesSocket[Ext.getCmp("serverTableStr")["title"]].emit("GET_IP", ["get_IP", IPofserver, "undefined"].join('#'));
                            serverNameTablesSocket[Ext.getCmp("serverTableStr")["title"]].emit("DESTROY_IP", "IP_destroy_token#" + createKeyValueForGoip + "___" + IPofserver);
                            
                            Ext.getCmp("pwin" + Ext.getCmp("serverTableStr")["title"] +  rec.get("channel")).reset();
                            Ext.getCmp("pwin" + Ext.getCmp("serverTableStr")["title"] +  rec.get("channel")).updateText(serverName + ' Node1 is <span style="color:green"><b>'  + 'connected</b></span>');
                            Ext.Ajax.request({
                                url: MIMPATH + "/server/" + serverName + "/table/" + tables + "/refill/" + rec.get("number")[0] + "?option=checkServer",
                                success: function(response) {
                                    if(typeof Ext.getCmp("serverpywin" + Ext.getCmp("serverTableStr")["title"] +  rec.get("channel")) != "undefined"){
                                        serverDotPY = Ext.JSON.decode(response.responseText)["checkServer"];
                                        if (serverDotPY) {
                                            Ext.getCmp("serverpywin" + Ext.getCmp("serverTableStr")["title"] +  rec.get("channel")).updateText(serverName + ' Node2 is <span style="color:green"><b>'  + 'connected</b></span>');
                                            Ext.getCmp("serverpywin" + Ext.getCmp("serverTableStr")["title"]).updateText(serverName + ' Node2 is <span style="color:green"><b>'  + 'connected</b></span>');

                                        }
                                    }
                                    logg("serverDotPY", serverDotPY);
                                }
                            });
                        }catch (e){
                            //
                        }
                    });
                    serverNameTablesSocket[Ext.getCmp("serverTableStr")["title"]].on('disconnect', function () {
                        try{
                          Ext.getCmp("pwin" + Ext.getCmp("serverTableStr")["title"] +  rec.get("channel")).wait({text: serverName + ' Node1 is disconnected, \n reconnecting please wait ...'});
                          p.reset()
                        }catch (e){
                            //
                        }
                        //logg('socket is disconnected')
                    });
                    ////////////////////////////////// SOCKET DICONNECT HANDELLING ///
                    Ext.Ajax.request({
                        url: MIMPATH + "/server/" + serverName + "/table/" + tables + "/refill/" + rec.get("number")[0] + "?option=checkServer",
                        success: function(response) {
                            if(typeof Ext.getCmp("serverpywin" + Ext.getCmp("serverTableStr")["title"] +  rec.get("channel")) != "undefined"){
                                serverDotPY = Ext.JSON.decode(response.responseText)["checkServer"];
                                if (serverDotPY) {
                                    Ext.getCmp("serverpywin" + Ext.getCmp("serverTableStr")["title"] +  rec.get("channel")).updateText(
                                        serverName + ' Node2 is <span style="color:green"><b>'  + 'connected</b></span>'
                                    );
                                }else{
                                    Ext.getCmp("serverpywin" + Ext.getCmp("serverTableStr")["title"] +  rec.get("channel")).updateText(
                                        serverName + ' Node2 is <span style="color:red"><b>'  + 'disconnected</b></span>'
                                    );
                                    Ext.getCmp("serverpywin" + Ext.getCmp("serverTableStr")["title"]).updateText(
                                        serverName + ' Node2 is <span style="color:red"><b>'  + 'disconnected</b></span>'
                                    );
                                    try{p.reset()}catch(r){}
                                }
                            }
                            
                            logg("serverDotPY", serverDotPY);
                        }
                    });
                }
            },
            layout: {
                type: "vbox",
                align: "center"
            },
            renderTo: document.body,
            items: [{
                xtype: "panel",
                title: '<span style="color:black"><b>tmp</b></span>'.replace('tmp', "Action"),
                collapsed: false,
                hideCollapseTool: false,
                id: "RefillBalanceWindowAction"+rec.get("channel"),
                layout: {
                    type: "hbox",
                    align: "stretch",
                    padding: 1,
                    border:true
                },
                width: 410,
                items: [ {
                    xtype: "splitter",
                    width: 10
                },{
                    xtype: "button",
                    text: "<b>Yes</b>",
                    scale: "medium",
                    renderTo: Ext.getBody(),
                    handler:function(){
                        logg('return_val_dic', return_val_dic)
                        if(1){
                            fRBWF_handler.func2_action_handler(
                                func_key,
                                serverName,
                                serverNameTablesSocket[Ext.getCmp("serverTableStr")["title"]], 
                                rec.get("channel").split("/")[1],
                                return_val_dic['sabKey'],
                                MIMPATH + "/server/" + serverName + "/table/" + tables + "/refill/" + rec.get("number")[0] + "?option=checkServer",
                                MIMPATH + "/server/" + serverName + "/table/" + tables + "/refill/" + rec.get("number")[0] + "?option=cmdKey&value=YES&sabKey=" + return_val_dic['sabKey'] + "&idno=" + return_val_dic['idno'] + "&channel=" + rec.get("channel").split("/")[1]
                            )
                        }
                        if(1){
                            fRBWF_handler.func5_action_handler(
                                func_key,
                                serverName,
                                serverNameTablesSocket[Ext.getCmp("serverTableStr")["title"]], 
                                rec.get("channel").split("/")[1],
                                return_val_dic['sabKey'],
                                MIMPATH + "/server/" + serverName + "/table/" + tables + "/refill/" + rec.get("number")[0] + "?option=checkServer",
                                MIMPATH + "/server/" + serverName + "/table/" + tables + "/refill/" + rec.get("number")[0] + "?option=cmdKey&value=YES&sabKey=" + return_val_dic['sabKey'] + "&idno=" + return_val_dic['idno'] + "&channel=" + rec.get("channel").split("/")[1]
                            )
                        }
                    }
                },{
                    xtype: "splitter",
                    width: 10
                },{
                    xtype: "button",
                    text: '<span style="color:red"><b>NO</b><span>',
                    renderTo: Ext.getBody(),
                    handler:function(){
                        if(1){
                            fRBWF_handler.func2_action_handler(
                                func_key, 
                                serverName,
                                serverNameTablesSocket[Ext.getCmp("serverTableStr")["title"]],
                                rec.get("channel").split("/")[1],
                                return_val_dic['sabKey'],
                                MIMPATH + "/server/" + serverName + "/table/" + tables + "/refill/" + rec.get("number")[0] + "?option=checkServer",
                                MIMPATH + "/server/" + serverName + "/table/" + tables + "/refill/" + rec.get("number")[0] + "?option=cmdKey&value=NO&sabKey=" + return_val_dic['sabKey'] + "&idno=" + return_val_dic['idno'] + "&channel=" + rec.get("channel").split("/")[1]
                            )
                        }
                        if(1){
                            fRBWF_handler.func5_action_handler(
                                func_key, 
                                serverName,
                                serverNameTablesSocket[Ext.getCmp("serverTableStr")["title"]],
                                rec.get("channel").split("/")[1],
                                return_val_dic['sabKey'],
                                MIMPATH + "/server/" + serverName + "/table/" + tables + "/refill/" + rec.get("number")[0] + "?option=checkServer",
                                MIMPATH + "/server/" + serverName + "/table/" + tables + "/refill/" + rec.get("number")[0] + "?option=cmdKey&value=NO&sabKey=" + return_val_dic['sabKey'] + "&idno=" + return_val_dic['idno'] + "&channel=" + rec.get("channel").split("/")[1]
                            )
                        }
                    }

                },{
                    xtype: "splitter",
                    width: 10
                },{
                    xtype: "button",
                    text: "<b>dont know</b>",
                    renderTo: Ext.getBody(),
                    handler:function(){
                        if(1){
                            fRBWF_handler.func2_action_handler(
                                func_key, 
                                serverName,
                                serverNameTablesSocket[Ext.getCmp("serverTableStr")["title"]], 
                                rec.get("channel").split("/")[1],
                                return_val_dic['sabKey'],
                                MIMPATH + "/server/" + serverName + "/table/" + tables + "/refill/" + rec.get("number")[0] + "?option=checkServer",
                                MIMPATH + "/server/" + serverName + "/table/" + tables + "/refill/" + rec.get("number")[0] + "?option=cmdKey&value=DONTKNOW&sabKey=" + return_val_dic['sabKey'] + "&idno=" + return_val_dic['idno'] + "&channel=" + rec.get("channel").split("/")[1]
                            )
                        }
                        if(1){
                            fRBWF_handler.func5_action_handler(
                                func_key,
                                serverName,
                                serverNameTablesSocket[Ext.getCmp("serverTableStr")["title"]], 
                                rec.get("channel").split("/")[1],
                                return_val_dic['sabKey'],
                                MIMPATH + "/server/" + serverName + "/table/" + tables + "/refill/" + rec.get("number")[0] + "?option=checkServer",
                                MIMPATH + "/server/" + serverName + "/table/" + tables + "/refill/" + rec.get("number")[0] + "?option=cmdKey&value=DONTKNOW&sabKey=" + return_val_dic['sabKey'] + "&idno=" + return_val_dic['idno'] + "&channel=" + rec.get("channel").split("/")[1]
                            )
                        }                        
                    }
                },{
                    xtype: "splitter",
                    width: 10
                },{
                    xtype: "button",
                    text: '<span style="color:yellow"><b>Reset SIM </b><span>',
                    renderTo: Ext.getBody(),
                    handler: function() {
                        /*
                        isOnline(function() {
                            Ext.Msg.alert("status", "Internet connection is lost, please refresh the page");
                        }, function() {});*/
                        Ext.Msg.confirm("Status", "Do you want to reset SIM: " + rec.get("channel"), function(btn) {
                            if (btn == "yes") {
                                //var butt = function() {
                                serverNameTablesSocket[Ext.getCmp("serverTableStr")["title"]].emit("send_module", "reset " + rec.get("channel").split("/")[1] + "___" + IPofserver);
                                serverNameTablesSocket[Ext.getCmp("serverTableStr")["title"]].emit("DESTROY_IP", "IP_destroy_token#" + createKeyValueForGoip + "___" + IPofserver);
                                //};
                                //checkSqlServer(serverName, "undefined", butt);
                            }
                        });
                    }
                },{
                    xtype: "splitter",
                    width: 10
                },{
                    xtype: "button",
                    text: '<span style="color:yellow"><b>Reboot</b><span>',
                    renderTo: Ext.getBody(),
                    handler: function() {
                        /*
                        isOnline(function() {
                            Ext.Msg.alert("status", "Internet connection is lost, please refresh the page");
                        }, function() {});*/
                        Ext.Msg.confirm("Status", "Do you want to reset SIM: " + rec.get("channel"), function(btn) {
                            if (btn == "yes") {
                                //var butt = function() {
                                serverNameTablesSocket[Ext.getCmp("serverTableStr")["title"]].emit("DESTROY_IP", "reboot_token" + createKeyValueForGoip + "___" + IPofserver);
                                //};
                                //checkSqlServer(serverName, "undefined", butt);
                            }
                        });
                    }
                },{
                    xtype: "splitter",
                    width: 10
                },{
                    xtype: "button",
                    text: '<span style="color:red"><b>Exit</b><span>',
                    renderTo: Ext.getBody(),
                    handler: function() {
                        /*
                        isOnline(function() {
                            Ext.Msg.alert("status", "Internet connection is lost, please refresh the page");
                        }, function() {});*/
                        
                        serverNameTablesSocket[Ext.getCmp("serverTableStr")["title"]].emit("DESTROY_IP", "IP_destroy_token#" + createKeyValueForGoip + "___" + IPofserver);
                        serverNameTablesSocket[Ext.getCmp("serverTableStr")["title"]].emit("subscribe_balanceRefill", "UNSUBSCRIBE " + rec.get("channel").split("/")[1]);
      
                        //Ext.getCmp("grid_" + Ext.getCmp("serverTableStr")["title"]).store.loadPage(1);
                        Ext.getCmp("RefillPanel"+Ext.getCmp("serverTableStr")["title"]).collapse();
                        //Ext.getCmp("grid_" + Ext.getCmp("serverTableStr")["title"]).store.loadPage(1);
                        /*
                        Ext.Ajax.request({
                            url: MIMPATH + "/server/" + serverName + "/table/" + tables + "/refill/" + rec.get("number")[0] + "?CHECHSERVERSTATUS=SERVERSTATUS",
                            success: function(response) {
                                serverStatus = Ext.JSON.decode(response.responseText)["serverStatus"];
                                //mysqlcheckServer = Ext.JSON.decode(response.responseText)["mysqlcheckServer"];
                                // TODO why should I need to check if mysql server is on when we send only to server.py
                                if (!(serverStatus==true)) {
                                    //Ext.Msg.alert("Status", "sorry still server down");
                                    //Ext.getCmp("grid_" + Ext.getCmp("serverTableStr")["title"]).store.loadPage(1);
                                } else {
                                    //serverNameTablesSocket[Ext.getCmp("serverTableStr")["title"]].emit("subscribe_balanceRefill", "UNSUBSCRIBE " + rec.get("channel").split("/")[1]);
                                    serverNameTablesSocket[Ext.getCmp("serverTableStr")["title"]].emit("DESTROY_IP", "IP_destroy_token#" + createKeyValueForGoip + "___" + IPofserver);
                                    //Ext.getCmp("grid_" + Ext.getCmp("serverTableStr")["title"]).store.loadPage(1);
                                }
                            },
                            failure: function(response) {
                                Ext.getCmp("grid_" + Ext.getCmp("serverTableStr")["title"]).store.loadPage(1);
                            }
                        });*/
                        //checkSqlServer(serverName, "undefined", butt);
                        
                        

                        //Ext.getCmp("grid_" + Ext.getCmp("serverTableStr")["title"]).store.loadPage(1);
                    }
                }],
                flex: 1
            },{
                    width: 410,
                    header: false,
                    border: false,
                    layout: "fit",
                    //modal:true,
                    items:[{
                        id:"pwinStatus" + Ext.getCmp("serverTableStr")["title"] + rec.get("channel"),
                        xtype:"progressbar",
                        cls:'custom',
                        text: 'Status Window'                    
                    }]
                },{
                    width: 410,
                    header: false,
                    border: false,
                    layout: "fit",
                    //modal:true,
                    items:[{
                        id:"pwin" + Ext.getCmp("serverTableStr")["title"] +  rec.get("channel"),
                        xtype:"progressbar",
                        cls:'custom',
                        text: serverName + ' Node1 is <span style="color:green"><b>'  + 'connected</b></span>'
                    }]
                },{
                    width: 410,
                    header: false,
                    border: false,
                    layout: "fit",
                    //modal:true,
                    items:[{
                        id:"serverpywin" + Ext.getCmp("serverTableStr")["title"] + rec.get("channel"),
                        xtype:"progressbar",
                        cls:'custom',
                        text: serverName + ' Node2 is <span style="color:green"><b>'  + 'connected</b></span>'
                    }]
                },{
                xtype: "panel",
                title: '<span style="color:black"><b>tmp</b></span>'.replace('tmp', "Select Refill Option"),
                layout: {
                    type: "hbox",
                    align: "stretch"
                },
                width: 410,
                items: [{
                    xtype: "combo",
                    displayField: "name",
                    width: 410,
                    store: createStore(),
                    listeners:{
                        scope: this,
                        destroy: function(){
                            // TODO why twice unsubscribe, here and 11 lines below
                            // serverNameTablesSocket[Ext.getCmp("serverTableStr")["title"]].emit("subscribe_balanceRefill", "UNSUBSCRIBE " + rec.get("channel").split("/")[1]);
                            
                            try{
                                serverNameTablesSocket[Ext.getCmp("serverTableStr")["title"]].emit("DESTROY_IP", "IP_destroy_token#" + createKeyValueForGoip + "___" + IPofserver);
                                serverNameTablesSocket[Ext.getCmp("serverTableStr")["title"]].emit("subscribe_balanceRefill", "UNSUBSCRIBE " + rec.get("channel").split("/")[1]);
                            }catch(e){}

                            /*Ext.Ajax.request({
                                url: MIMPATH + "/server/" + serverName + "/table/" + tables + "/refill/" + rec.get("number")[0] + "?CHECHSERVERSTATUS=SERVERSTATUS",
                                success: function(response) {
                                    serverStatus = Ext.JSON.decode(response.responseText)["serverStatus"];

                                    
                                    //mysqlcheckServer = Ext.JSON.decode(response.responseText)["mysqlcheckServer"];
                                    // TODO why should I need to check if mysql server is on when we send only to server.py
                                    // 
                                    
                                    if (!(serverStatus==true)) {
                                        //Ext.Msg.alert("Status", "sorry still server down");
                                        // Ext.getCmp("grid_" + Ext.getCmp("serverTableStr")["title"]).store.loadPage(1);
                                    } else {

                                        try{
                                            serverNameTablesSocket[Ext.getCmp("serverTableStr")["title"]].emit("DESTROY_IP", "IP_destroy_token#" + createKeyValueForGoip + "___" + IPofserver);
                                            serverNameTablesSocket[Ext.getCmp("serverTableStr")["title"]].emit("subscribe_balanceRefill", "UNSUBSCRIBE " + rec.get("channel").split("/")[1]);
                                        }catch(e){}
                                        //Ext.getCmp("grid_" + Ext.getCmp("serverTableStr")["title"]).getView().refreshNode(Ext.getCmp("grid_" + Ext.getCmp("serverTableStr")["title"]).getStore().indexOfId(82))
                                            if(interntDisconnection){
                                                //try{Ext.getCmp("grid_" + Ext.getCmp("serverTableStr")["title"]).store.loadPage(1);}catch(r){}                                                
                                            }
                                        //Ext.getCmp("grid_" + Ext.getCmp("serverTableStr")["title"]).getView().refresh();
                                    }
                                },
                                failure: function(response) {
                                    try{
                                        Ext.getCmp("grid_" + Ext.getCmp("serverTableStr")["title"]).getProxy().clear();
                                        Ext.getCmp("grid_" + Ext.getCmp("serverTableStr")["title"]).store.loadPage(1);
                                    }catch (err){

                                    }
                                    Ext.getCmp("pwinStatus" + Ext.getCmp("serverTableStr")["title"] +  rec.get("channel")).updateText(
                                        serverDown.replace("Server", serverName)
                                    )
                                }
                            });*/
                        },
                        select: function onItemClick(item) {
                            if(countHide == 0){
                                // hide the window only at the first time
                                Ext.getCmp("RefillBalanceWindowAction"+rec.get("channel")).show();
                                Ext.getCmp("transfer"+rec.get("channel")).hide();
                            }                            
                            var msga= Ext.Msg.confirm("Refill", "Are you sure to select: " + '<span style="color:red"><b>' + item.value + "</b></span>" + " ?", function(btn) {
                                if (btn == "yes") {
                                    var checkServer;
                                    //stats.active(6,4,'ALERT!!!',Ext.Msg.ERROR,Ext.getCmp("serverTableStr")["title"])
                                    if(item.value.indexOf('transfer') == -1){
                                        Ext.Ajax.request({
                                            url: MIMPATH + "/server/" + Ext.getCmp("serverTableStr")["title"].split('/')[0] + "/table/" + Ext.getCmp("serverTableStr")["title"].split('/')[1].split('_')[0]+ "/refill/" + rec.get("number")[0],
                                            method: 'GET',
                                            params: {
                                                'USER_ACTIONS':'USER_ACTIONS', 
                                                'username':createKeyValueForGoip.split('_')[0],
                                                'childserver':createKeyValueForGoip.split('_')[1], 
                                                'action': item.value,

                                                'sim': rec.get("channel").split("/")[1]
                                            },
                                            success: function(response) {},
                                            failure: function(response) {}
                                        })
                                    }

                                    Ext.Ajax.request({
                                        url: MIMPATH + "/server/" + Ext.getCmp("serverTableStr")["title"].split('/')[0] + "/table/" + Ext.getCmp("serverTableStr")["title"].split('/')[1].split('_')[0]+ "/refill/" + rec.get("number")[0] + "?CHECHSERVERSTATUS=SERVERSTATUS",
                                        success: function(response) {
                                            serverStatus = Ext.JSON.decode(response.responseText)["serverStatus"];
                                            logg('serverStatus', serverStatus)
                                            //mysqlcheckServer = Ext.JSON.decode(response.responseText)["mysqlcheckServer"];
                                            if (serverStatus==true){ //if ((checkServer==true) && (mysqlcheckServer==true)) {
                                                // here "undefined "" for the transfer server, if tranfer, "undefined" would be replaced
                                                // by a server IP
                                                
                                                //serverNameTablesSocket[Ext.getCmp("serverTableStr")["title"]].emit("GET_IP", ["get_IP", IPofserver, "undefined"].join('#'));
                                                //serverNameTablesSocket[Ext.getCmp("serverTableStr")["title"]].emit("subscribe_balanceRefill", "SUBSCRIBE " + rec.get("channel").split("/")[1] + " " + createKeyValueForGoip + "___" + IPofserver);
                                                
                                                Ext.getCmp("Console"+rec.get("channel")).update("");
                                                p = Ext.getCmp("progressbar"+Ext.getCmp('Console_Refill_win')["title"]);
                                                if (typeof command != "undefined") {
                                                    for (var i = 0; i < command_key.length; i++) {
                                                        if (item.value == Object.keys(command_key[i])) {
                                                            var cmdTosend;
                                                            func_key = command_func[i][item.value];
                                                            cmd = command_key[i][item.value];
                                                            cmdKey = item.value;
                                                            refillType = rec.get("number")[0];
                                                            sim_of_this_server = rec.get("channel").split("/")[1];
                                                            return_val_dic['sim_of_this_server'] = sim_of_this_server
                                                            if (func_key == "1") {
                                                                //var myStore = Ext.getCmp('grid_'+Ext.getCmp("serverTableStr")["title"]).getStore();
                                                                logg(
                                                                    'refiilPanel Connected ',
                                                                    serverNameTablesSocket[Ext.getCmp("serverTableStr")["title"]].io.engine.id
                                                                )
                                                                fRBWF.show_refill_window("RefillBalanceWindowAction", rec) 
                                                                logg('after', serverNameTablesSocket)
                                                                serverNameTablesSocket[Ext.getCmp("serverTableStr")["title"]].emit(
                                                                    "GET_IP", ["get_IP", IPofserver, "undefined"].join('#'));
                                                                if(countHide == 0){
                                                                    serverNameTablesSocket[Ext.getCmp("serverTableStr")["title"]].emit("DESTROY_IP", "IP_destroy_token#" + createKeyValueForGoip + "___" + IPofserver);
                                                                }
                                                                serverNameTablesSocket[Ext.getCmp("serverTableStr")["title"]].emit("subscribe_balanceRefill", "SUBSCRIBE " + rec.get("channel").split("/")[1] + " " + createKeyValueForGoip + "___" + IPofserver);
                                                                fRBWF.open_Progessbar(p)
                                                                //Subscribe to 150 free minutes
                                                                fRBWF.func1(func_key, cmd, sim_of_this_server, IPofserver, serverNameTablesSocket[Ext.getCmp("serverTableStr")["title"]]);
                                                                //serverNameTablesSocket[Ext.getCmp("serverTableStr")["title"]].emit("DESTROY_IP", "IP_destroy_token#" + createKeyValueForGoip + "___" + IPofserver);
                            
                                                            }
                                                            // USSD number *155*refill_cardnumber#____Refill with 30____2"
                                                            if (func_key == "2") {

                                                                fRBWF.show_refill_window("RefillBalanceWindowAction", rec)

                                                                serverNameTablesSocket[Ext.getCmp("serverTableStr")["title"]].emit("GET_IP", ["get_IP", IPofserver, "undefined"].join('#'));
                                                                if(countHide == 0){
                                                                    serverNameTablesSocket[Ext.getCmp("serverTableStr")["title"]].emit("DESTROY_IP", "IP_destroy_token#" + createKeyValueForGoip + "___" + IPofserver);
                                                                }
                                                                serverNameTablesSocket[Ext.getCmp("serverTableStr")["title"]].emit("subscribe_balanceRefill", "SUBSCRIBE " + rec.get("channel").split("/")[1] + " " + createKeyValueForGoip + "___" + IPofserver);
                                                                
                                                                darray = [];
                                                                darray[0] = tables;
                                                                darray[1] = sim_of_this_server;
                                                                darray[2] = IPofserver;
                                                                darray[3] = serverName;
                                                                return_val_dic['sabKey'] = refillType + +cmdKey.match(/\d+/)[0]

                                                                fRBWF.open_Progessbar(p)
                                                                logg('cmdKey',cmdKey)
                                                                // sabKey here for action YES, NO, DONTKNOW
                                                                fRBWF.func2(
                                                                    func_key,
                                                                    cmd,
                                                                    sim_of_this_server,
                                                                    serverNameTablesSocket[Ext.getCmp("serverTableStr")["title"]], 
                                                                    darray,
                                                                    p,
                                                                    return_val_dic,
                                                                    MIMPATH + "/server/" + serverName + "/table/" + tables + "/refill/" + refillType+ "?option=cmdKey&value=" + +cmdKey.match(/\d+/)[0]+ "&refillType=" + refillType+ "&channel=" + sim_of_this_server
                                                                );

                                                            }                                                            
                                                            // OUT OF ODER--------------
                                                            if (func_key == "3") {
                                                                var array= fRBWF.func3(
                                                                    func_key, cmd,
                                                                    sim_of_this_server,
                                                                    rec.get("number").split(refillType)[1].slice(-5)
                                                                );
                                                                cmdStore = array[0];sipStore = array[1];simNumber = array[2];
                                                            }
                                                            // USSD SIM *170*transferNumber*amount*cardid#____transfer amount to another SIM____4"
                                                            // j143
                                                            if (func_key == "4") {
                                                                fRBWF.show_refill_window(null, rec)
                                                                var cmds = fRBWF.func4(
                                                                    func_key, 
                                                                    cmd,
                                                                    sim_of_this_server
                                                                );
                                                                return_val_dic['cmd'] = cmds
                                                                return_val_dic['items'] = item.value
                                                            }
                                                            // "USSD SIM *131*2*2*amount*transferNumber*0000*1*#____transfer amount to another SIM____6"
                                                            if (func_key == "6") {
                                                                fRBWF.show_refill_window(null, rec)
                                                                var cmds = fRBWF.func6(
                                                                    func_key, 
                                                                    cmd,
                                                                    sim_of_this_server                 
                                                                 );
                                                                return_val_dic['cmd'] = cmds
                                                                return_val_dic['items'] = item.value   
                                                            }
                                                            // USSD number *155*refill_cardnumber*cardid#____Refill with 20 SR____5"
                                                            if (func_key == "5") {

                                                                fRBWF.show_refill_window("RefillBalanceWindowAction", rec)

                                                                serverNameTablesSocket[Ext.getCmp("serverTableStr")["title"]].emit("GET_IP", ["get_IP", IPofserver, "undefined"].join('#'));
                                                                if(countHide == 0){
                                                                    serverNameTablesSocket[Ext.getCmp("serverTableStr")["title"]].emit("DESTROY_IP", "IP_destroy_token#" + createKeyValueForGoip + "___" + IPofserver);
                                                                }
                                                                serverNameTablesSocket[Ext.getCmp("serverTableStr")["title"]].emit("subscribe_balanceRefill", "SUBSCRIBE " + rec.get("channel").split("/")[1] + " " + createKeyValueForGoip + "___" + IPofserver);
                                                                
                                                                darray = [];
                                                                darray[0] = tables;
                                                                darray[1] = sim_of_this_server;
                                                                darray[2] = IPofserver;
                                                                darray[3] = serverName;
                                                                darray[4] = rec.get("cardid")
                                                                return_val_dic['sabKey'] = refillType + +cmdKey.match(/\d+/)[0]
                                                                fRBWF.open_Progessbar(p)
                                                                logg('cmdKey',cmdKey)
                                                                // sabKey here for action YES, NO, DONTKNOW
                                                                fRBWF.func5(func_key,
                                                                    cmd,
                                                                    sim_of_this_server,
                                                                    serverNameTablesSocket, 
                                                                    darray,
                                                                    p,
                                                                    return_val_dic,
                                                                    MIMPATH + "/server/" + serverName + "/table/" + tables + "/refill/" + refillType+ "?option=cmdKey&value=" + +cmdKey.match(/\d+/)[0]+ "&refillType=" + refillType+ "&channel=" + sim_of_this_server
                                                                );
                                                            }
                                                            // USSD SIM *130*transferNumber*amount*simNumber#____transfer amount in Refill____7
                                                            if (func_key == "7") {
                                                                fRBWF.show_refill_window(null, rec)
                                                                var cmds= fRBWF.func7(func_key, cmd,
                                                                    sim_of_this_server,
                                                                    rec.get("number").split(refillType)[1].slice(-5)
                                                                );
                                                                return_val_dic['cmd'] = cmds
                                                                return_val_dic['items'] = item.value

                                                            }
                                                        }
                                                    }
                                                }
                                            } else {
                                                Ext.getCmp("pwinStatus" + Ext.getCmp("serverTableStr")["title"] +  rec.get("channel")).updateText(
                                                    serverDown.replace("Server", serverName) + + ', try again !!!'
                                                )
                                                //Ext.Msg.alert("Status", "Server down".replace("Server", serverName));
                                                try{
                                                    //refillWin.close();
                                                    //Ext.getCmp("grid_" + Ext.getCmp("serverTableStr")["title"]).store.loadPage(1);
                                                }catch (err){
                                                    //
                                                }
                                                
                                                
                                            }
                                            countHide += 1 
                                            try{
                                                 Ext.getCmp("pwinStatus" + Ext.getCmp("serverTableStr")["title"] +  rec.get("channel")).updateText(internetSuccessText)
                                                 Ext.getCmp("pwinStatus" + Ext.getCmp("serverTableStr")["title"]).updateText(internetSuccessText)
                                            }catch(e){}
                                        },
                                        failure: function() {
                                            Ext.getCmp("pwinStatus" + Ext.getCmp("serverTableStr")["title"] +  rec.get("channel")).updateText(internetFailureText)
                                            Ext.getCmp("pwinStatus" + Ext.getCmp("serverTableStr")["title"]).updateText(internetFailureText )
                                            //Ext.Msg.alert("status", "checkServer failed");
                                            //

                                            //refillWin.close();
                                            //Ext.getCmp("grid_" + Ext.getCmp("serverTableStr")["title"]).store.loadPage(1);
                                        }
                                    });
                                }
                            });
                            //msga.getDialog().getPositionEl().setTop(50);
                        }
                    },
                    queryMode: "local",
                    typeAhead: true
                }]
            },{
                xtype: "panel",
                collapsed: true,
                hideCollapseTool: true,
                id: "transfer"+rec.get("channel"),
                width: 410,
                items: [ {
                    xtype: "form",
                    title: "Transfer",
                    items: [ {
                        xtype: "numberfield",
                        id: "insertSim"+rec.get("channel"),
                        border: 5,
                        fieldLabel: "Inset SIM",
                        allowBlank: false
                    }, {
                        xtype: "numberfield",
                        id: "insertAmount"+rec.get("channel"),
                        fieldLabel: "Insert amount",
                        allowBlank: false
                    } ],
                    buttons: [ {
                        xtype: "button",
                        text: "Send",
                        handler: function() {
                            // fRBWF_handler is the instance of class RefillBalanceWindowFUNC_handler
                            if(func_key == "7"){
                                var send = function(){
                                    fRBWF_handler.func7_handler(
                                        func_key,
                                        serverName,
                                        tables, 
                                        rec,
                                        ForRefillBalanceWindowSIM_IP,
                                        createKeyValueForGoip,
                                        return_val_dic['sim_of_this_server'],
                                        return_val_dic['cmd'],
                                        IPofserver,
                                        serverNameTablesSocket[Ext.getCmp("serverTableStr")["title"]], // socket,
                                        return_val_dic['items']
                                    )
                                }
                            }
                            // fRBWF_handler is the instance of class RefillBalanceWindowFUNC_handler
                            if(func_key == "4"){
                                var send = function(){
                                    fRBWF_handler.func4_handler(
                                        func_key,
                                        serverName,
                                        tables, 
                                        rec,
                                        ForRefillBalanceWindowSIM_IP,
                                        createKeyValueForGoip,
                                        return_val_dic['sim_of_this_server'],
                                        return_val_dic['cmd'],
                                        IPofserver,
                                        serverNameTablesSocket[Ext.getCmp("serverTableStr")["title"]], // socket,
                                        return_val_dic['items']
                                    )
                                }
                            }
                            // fRBWF_handler is the instance of class RefillBalanceWindowFUNC_handler
                            if(func_key == "6"){
                                var send = function(){
                                    fRBWF_handler.func6_handler(
                                        func_key,
                                        serverName,
                                        tables, 
                                        rec,
                                        ForRefillBalanceWindowSIM_IP,
                                        createKeyValueForGoip,
                                        return_val_dic['sim_of_this_server'],
                                        return_val_dic['cmd'],
                                        IPofserver, //
                                        serverNameTablesSocket[Ext.getCmp("serverTableStr")["title"]], // socket,
                                        return_val_dic['items']
                                    )
                                }
                            }
                            checkSqlServer(serverName, "undefined", send);
                        }
                    }]
                }]
            },{
                xtype: "panel",
                title: '<span style="color:black"><b>' + 'Console'+ "</b></span>" + no_of_cards,
                id: Ext.getCmp('Console_Refill_win')["title"],
                autoScroll: true,
                width: 410,
                flex: 4
            },{
                xtype: "progressbar",
                id: "progressbar"+Ext.getCmp('Console_Refill_win')["title"],
                width: 410
            },{
                xtype: "panel",
                title: '<span style="color:orange"><b>INSTRUCTIONS TO FOLLOW</b><span>',
                id: "Instructions"+rec.get("channel"),
                autoScroll: true,
                width: widthInstruction,
                height: heightInstruction,
                html: html
            } ]
        });
        Ext.getCmp("RefillPanelCont"+Ext.getCmp("serverTableStr")["title"]).doLayout();
        Ext.getCmp("RefillPanel"+Ext.getCmp("serverTableStr")["title"]).expand();
        //checkSqlServer(serverName, refillWin, "undefined");
    }
});
