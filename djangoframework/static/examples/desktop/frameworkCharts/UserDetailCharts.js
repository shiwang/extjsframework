

Ext.define('MyDesktop.frameworkCharts.UserDetailCharts', {
	extend: 'Ext.panel.Panel',  // modul will be extended
	 requires: [
	    'Ext.chart.*',
	    'Ext.Window', 
	    'Ext.fx.target.Sprite', 
	    'Ext.layout.container.Fit', 
	    'Ext.window.MessageBox', 'Ext.data.*',
	    'Ext.button', 'Ext.JSON', 'Ext.Msg', 'Ext.Ajax',
    ],
    xtype:'user_detail_charts',
	region:'east',
	title:'Work Stats',
    //layout: 'fit',                                       
    width: 1000,
    collapsible: true,   // make collapsible
    collapsed : true,
    autoScroll:true,
    listeners:{
        expand:function(){
        	try{
		    	Ext.getCmp('date1' + Ext.getCmp("serverTableStr")["title"]).setValue("rawValue", "")
        		Ext.getCmp('date2' + Ext.getCmp("serverTableStr")["title"]).setValue("rawValue", "")

		    	Ext.getCmp('time1' + Ext.getCmp("serverTableStr")["title"]).setValue(null)
        		Ext.getCmp('time2' + Ext.getCmp("serverTableStr")["title"]).setValue(null)
        		logg('TIME', Ext.getCmp('time1' + Ext.getCmp("serverTableStr")["title"]))

        	}catch(r){}
        }
    },
    initComponent:function(){
        var me = this;       
        Ext.applyIf(me, {
            layout: {
                type: "hbox",
                align: "stretch"
            },
            bodyStyle: {
                "background-color": "#FFF"
            },
            items:[ {
                flex: 1,
                xtype: "container",
                layout: {
                    type: "vbox",
                    align: "stretch"
                },
                items: [me.createWindow(me.chart(me.loadData()), me)]
            },/*{
                flex: 1,
                xtype: "container",
                layout: {
                    type: "vbox",
                    align: "stretch"
                },
                items: [me.button1(3), me.button1(4)]
            }*/]
        });
		

        me.callParent(arguments);
    },
    button1:function(val){
    	return {
    		flex:1,
            xtype: 'button',
            text: 'MyButton' + val,
            height: 200
        }
    },
    chart:function(loadData){
		
		    return Ext.create('Ext.chart.Chart', {
		        animate: true,
		        style: 'background:#fff',
		        shadow: false,
		        id:'chart' + Ext.getCmp("serverTableStr")["title"], 
		        store: storeUsers,
		        listeners:{
		        	beforeshow:function(){
		        		console.log('beforeactivate'	)
		        	}
		        },
		        axes: [{
		            type: 'Numeric',
		            position: 'bottom',
		            fields: ['data1'],
		            label: {
		                renderer: Ext.util.Format.numberRenderer('0,0')
		            },
		            title: 'Number of totals actions',
		            minimum: 0
		        }, {
		            type: 'Category',
		            position: 'left',
		            fields: ['name'],
		            title: 'Employers'
		        }],
		        series: [{
		            type: 'bar',
		            axis: 'bottom',
		            label: {
		                display: 'insideEnd',
		                field: 'data1',
		                renderer: Ext.util.Format.numberRenderer('0'),
		                orientation: 'horizontal',
		                color: '#333',
		                'text-anchor': 'middle',
		                contrast: true
		            },
		            xField: 'name',
		            yField: ['data1'],
		            //color renderer
		            renderer: function(sprite, record, attr, index, store) {
		                var fieldValue = Math.random() * 20 + 10,
		                    value = (record.get('data1') >> 0) % 5,
		                    color = ['rgb(213, 70, 121)', 
		                             'rgb(44, 153, 201)', 
		                             'rgb(146, 6, 157)', 
		                             'rgb(49, 149, 0)', 
		                             'rgb(249, 153, 0)'][value];
		                             
		                return Ext.apply(attr, {
		                    fill: color
		                });
		            }
		        }]
		    });
		

    },
    getData:function(table){
        Ext.Ajax.request({
                url:  MIMPATH + "/server/" + Ext.getCmp("serverTableStr")["title"].split('/')[0] + "/table/" + Ext.getCmp("serverTableStr")["title"].split('/')[1].split('_')[0],
                method:'GET',
                params:{ 
                	table:table,
                	date1:Ext.getCmp('date1' + Ext.getCmp("serverTableStr")["title"])['rawValue'],
                	date2:Ext.getCmp('date2' + Ext.getCmp("serverTableStr")["title"])['rawValue'],
                	time1:Ext.getCmp('time1' + Ext.getCmp("serverTableStr")["title"])['rawValue'],
                	time2:Ext.getCmp('time2' + Ext.getCmp("serverTableStr")["title"])['rawValue'],
                	'USER_ACTIONS_CHARTS':'USER_ACTIONS_CHARTS'
                },
                success: function(response) {
				    
			    	var users = Ext.JSON.decode(response.responseText)["allUsers"]
			    	var dataUser = Ext.JSON.decode(response.responseText)["dataUser"]	
			    	logg('dataUser-->' , dataUser)			    
				    var storeUsers1= Ext.create('Ext.data.JsonStore', {
				        fields: ['name', 'data1'],
				        data: dataUser
				    });	
				    Ext.getCmp("chart" + Ext.getCmp("serverTableStr")["title"]).store.loadData(dataUser)
				    Ext.getCmp("chart" + Ext.getCmp("serverTableStr")["title"]).refresh(); 
                }
        });
    },
    createWindow:function(chart, me){
    	var selectedTable;
		var action = new Ext.Action({
		    text: 'user_actions',
		    handler: function(){
		    	selectedTable = 'user_actions';
	            //Ext.getCmp("STATUS" + Ext.getCmp("serverTableStr")["title"]).updateText('<span style="color:green"><b>' + 'Table:'+selectedTable + "</b></span>" )
		    	Ext.getCmp('date1' + Ext.getCmp("serverTableStr")["title"]).setValue("rawValue", "")
        		Ext.getCmp('date2' + Ext.getCmp("serverTableStr")["title"]).setValue("rawValue", "")
		    	Ext.getCmp('time1' + Ext.getCmp("serverTableStr")["title"]).setValue(null)
        		Ext.getCmp('time2' + Ext.getCmp("serverTableStr")["title"]).setValue(null)
        		Ext.getCmp('SelectTable' + Ext.getCmp("serverTableStr")["title"]).setText('user_actions')

				me.getData('user_actions')
		    },
		    iconCls: 'do-something',
		    itemId: 'UserAction' + Ext.getCmp("serverTableStr")["title"]
		});
		var action2 = new Ext.Action({
		    text: 'user_transfer_details',
		    handler: function(){
		        selectedTable = 'user_transfer_details';
	            //Ext.getCmp("STATUS" + Ext.getCmp("serverTableStr")["title"]).updateText('<span style="color:green"><b>' + 'Table:'+selectedTable + "</b></span>" )
		    	Ext.getCmp('date1' + Ext.getCmp("serverTableStr")["title"]).setValue("rawValue", "")
        		Ext.getCmp('date2' + Ext.getCmp("serverTableStr")["title"]).setValue("rawValue", "")
		    	Ext.getCmp('time1' + Ext.getCmp("serverTableStr")["title"]).setValue(null)
        		Ext.getCmp('time2' + Ext.getCmp("serverTableStr")["title"]).setValue(null)
        		Ext.getCmp('SelectTable' + Ext.getCmp("serverTableStr")["title"]).setText('user_transfer_details')

		        me.getData('user_transfer_details')
		    },
		    iconCls: 'do-something',
		    itemId: 'UserAction2' + Ext.getCmp("serverTableStr")["title"]
		});
	    return {
	        xtape:'window',
	        height: 700,
	        minHeight: 400,
	        minWidth: 550,
	        hidden: false,
	        maximizable: true,
	        title: 'Bar Renderer',
	        layout: 'fit',
	        tbar: [/*{
	        		xtype: "progressbar",
	        		id:'STATUS' + Ext.getCmp("serverTableStr")["title"],
	        		width: 200,
	        		text: 'STATUS WINDOW',
	        	},'-',*/{
			            text: 'Select Table',
			            id:'SelectTable' + Ext.getCmp("serverTableStr")["title"],
			            // Add the Action to a menu as a text item
			            menu: [action, action2]
        	},{
	        	xtype:'panel',
	        	items:[{

				        xtype: 'datefield',
				        format: "Y-m-d",
				        id:'date1' + Ext.getCmp("serverTableStr")["title"],
				        fieldLabel:'From'
				},{

				        xtype: 'datefield',
				        format: "Y-m-d",
				        id:'date2' + Ext.getCmp("serverTableStr")["title"],
				        fieldLabel:'To'
				}]
	        },{
	        	xtype:'panel',
	        	items:[{
			        xtype: 'timefield',
			        id:'time1' + Ext.getCmp("serverTableStr")["title"],
			        minValue: '00:00',
			        maxValue: '23:00',
			        format:'H:i',
			        increment: 30,
			        anchor: '100%'
			    }, {
			        xtype: 'timefield',
			        id:'time2' + Ext.getCmp("serverTableStr")["title"],
			        minValue: '00:00',
			        maxValue: '23:00',
			        format:'H:i',
			        increment: 30,
			        anchor: '100%'
			   }]
	        },{
	            text: 'Load Data',
	            handler: function() {
	                // Add a short delay to prevent fast sequential clicks
	                //window.loadTask.delay(100, function() {
	                    //storeUsers.loadData(Ext.getCmp(dataUser);
	                    logg('DATE1', Ext.getCmp('date1' + Ext.getCmp("serverTableStr")["title"])['rawValue'])
	                    //logg(Ext.getCmp('UserAction' + Ext.getCmp("serverTableStr")["title"]))
	                    if(selectedTable==='undefined'){
	                    	Ext.Msg.alert('First select a table ')
	                    }else{
	                    	 try{me.getData(selectedTable)}catch(e){}
	                    }
	                   
	                //});
	            }
	        },{
	            text: 'Save Chart',
	            handler: function() {
	                Ext.MessageBox.confirm('Confirm Download', 'Would you like to download the chart as an image?', function(choice){
	                    if(choice == 'yes'){
	                        chart.save({
	                            type: 'image/png'
	                        });
	                    }
	                });
	            }
	        }],
	        items: Ext.getCmp("chart" + Ext.getCmp("serverTableStr")["title"])
	    }
    },
    loadData:function(){
    	window.generateDataUsers = function(n, floor){
	    	var users = ['name', 'data1', 'data2', 'data3', 'data4', 'data5', 'data6']
	    	dataUser = []
	    	for(var i = 0; i < users.length; i++){
	    		floor = (!floor && floor !== 0)? 20 : floor;
				dataUser.push({
	                name: users[i],
	                data1: 0
	            });
	    	}
	    	return dataUser
	    }
	    window.storeUsers = Ext.create('Ext.data.JsonStore', {
	        fields: ['name', 'data1', 'data2', 'data3', 'data4', 'data5', 'data6', 'data7', 'data9', 'data9'],
	        data: generateDataUsers()
	    });
    	
    },
    loadDataSencha:function(){
    	
    	window.generateData = function(n, floor){
	        var data = [],
	            p = (Math.random() *  11) + 1,
	            i;
	            
	        floor = (!floor && floor !== 0)? 20 : floor;
	        
	        for (i = 0; i < (n || 12); i++) {
	            data.push({
	                name: Ext.Date.monthNames[i % 12],
	                data1: Math.floor(Math.max((Math.random() * 100), floor)),
	                data2: Math.floor(Math.max((Math.random() * 100), floor)),
	                data3: Math.floor(Math.max((Math.random() * 100), floor)),
	                data4: Math.floor(Math.max((Math.random() * 100), floor)),
	                data5: Math.floor(Math.max((Math.random() * 100), floor)),
	                data6: Math.floor(Math.max((Math.random() * 100), floor)),
	                data7: Math.floor(Math.max((Math.random() * 100), floor)),
	                data8: Math.floor(Math.max((Math.random() * 100), floor)),
	                data9: Math.floor(Math.max((Math.random() * 100), floor))
	            });
	        }
	        return data;
	    };
	    
	    window.generateDataNegative = function(n, floor){
	        var data = [],
	            p = (Math.random() *  11) + 1,
	            i;
	            
	        floor = (!floor && floor !== 0)? 20 : floor;
	            
	        for (i = 0; i < (n || 12); i++) {
	            data.push({
	                name: Ext.Date.monthNames[i % 12],
	                data1: Math.floor(((Math.random() - 0.5) * 100), floor),
	                data2: Math.floor(((Math.random() - 0.5) * 100), floor),
	                data3: Math.floor(((Math.random() - 0.5) * 100), floor),
	                data4: Math.floor(((Math.random() - 0.5) * 100), floor),
	                data5: Math.floor(((Math.random() - 0.5) * 100), floor),
	                data6: Math.floor(((Math.random() - 0.5) * 100), floor),
	                data7: Math.floor(((Math.random() - 0.5) * 100), floor),
	                data8: Math.floor(((Math.random() - 0.5) * 100), floor),
	                data9: Math.floor(((Math.random() - 0.5) * 100), floor)
	            });
	        }
	        return data;
	    };
	    window.generateDataUsers = function(n, floor){
	    	var users = ['name', 'salman', 'shiwang', 'abdul', 'vinay', 'harpal']
	    	dataUser = []
	    	for(var i = 0; i < users.length; i++){
	    		floor = (!floor && floor !== 0)? 20 : floor;
				dataUser.push({
	                name: users[i],
	                data1: Math.floor(Math.max((Math.random() * 100), floor))
	            });
	    	}
	    	return dataUser
	    }
	    window.storeUsers = Ext.create('Ext.data.JsonStore', {
	        fields: ['name', 'data1', 'data2', 'data3', 'data4', 'data5', 'data6', 'data7', 'data9', 'data9'],
	        data: generateDataUsers()
	    });
	    window.store1 = Ext.create('Ext.data.JsonStore', {
	        fields: ['name', 'data1', 'data2', 'data3', 'data4', 'data5', 'data6', 'data7', 'data9', 'data9'],
	        data: generateData()
	    });
	    window.storeNegatives = Ext.create('Ext.data.JsonStore', {
	        fields: ['name', 'data1', 'data2', 'data3', 'data4', 'data5', 'data6', 'data7', 'data9', 'data9'],
	        data: generateDataNegative()
	    });
	    window.store3 = Ext.create('Ext.data.JsonStore', {
	        fields: ['name', 'data1', 'data2', 'data3', 'data4', 'data5', 'data6', 'data7', 'data9', 'data9'],
	        data: generateData()
	    });
	    window.store4 = Ext.create('Ext.data.JsonStore', {
	        fields: ['name', 'data1', 'data2', 'data3', 'data4', 'data5', 'data6', 'data7', 'data9', 'data9'],
	        data: generateData()
	    });
	    window.store5 = Ext.create('Ext.data.JsonStore', {
	        fields: ['name', 'data1', 'data2', 'data3', 'data4', 'data5', 'data6', 'data7', 'data9', 'data9'],
	        data: generateData()
	    });    
	    
	    window.loadTask = new Ext.util.DelayedTask();
    }

});



