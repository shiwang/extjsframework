Ext.define('MyDesktop.frameworkCharts.UserManager', {
	extend: 'Ext.panel.Panel',  // modul will be extended
	 requires: [
	    'Ext.chart.*',
	    'Ext.Window', 
	    'Ext.fx.target.Sprite', 
	    'Ext.layout.container.Fit', 
	    'Ext.window.MessageBox', 'Ext.data.*',
	    'Ext.button', 'Ext.JSON', 'Ext.Msg', 'Ext.Ajax',
    ],
    xtype:'user_manager',
	region:'east',
	title:'User Manager',
    //layout: 'fit',                                       
    width: 1000,
    collapsible: true,   // make collapsible
    collapsed : true,
    autoScroll:true,
    listeners:{
        expand:function(){
        	try{
		    	Ext.getCmp('date1' + Ext.getCmp("serverTableStr")["title"]).setValue("rawValue", "")
        		Ext.getCmp('date2' + Ext.getCmp("serverTableStr")["title"]).setValue("rawValue", "")

		    	Ext.getCmp('time1' + Ext.getCmp("serverTableStr")["title"]).setValue(null)
        		Ext.getCmp('time2' + Ext.getCmp("serverTableStr")["title"]).setValue(null)
        		logg('TIME', Ext.getCmp('time1' + Ext.getCmp("serverTableStr")["title"]))

        	}catch(r){}
        }
    },

    initComponent:function(){
        var me = this; 
        var item1, item2, item3 , item4, users, tables, servers;  

        Ext.applyIf(me, {
            layout: {
                type: "hbox",
                align: "stretch"
            },
            bodyStyle: {
                "background-color": "#FFF"
            },
            items:[ {
                flex: 1,
                xtype: "container",
                layout: {
                    type: "vbox",
                    align: "stretch"
                },
                items: [me.CreateWindow(me.getData()), me.CreateWindow2(),{
                    xtype: "progressbar",
                    id:'STATUS2' + Ext.getCmp("serverTableStr")["title"],
                    width: 200,
                    flex:1,
                    text: 'STATUS WINDOW',
                },{xtype:'panel', flex:1}]
            },/*{
                flex: 1,
                xtype: "container",
                layout: {
                    type: "vbox",
                    align: "stretch"
                },
                items: [me.CreateWindow()]
            }*/]
        });
		

        me.callParent(arguments);
    }, 
    getData:function(){
        Ext.Ajax.request({
                url:  MIMPATH + "/server/" + Ext.getCmp("serverTableStr")["title"].split('/')[0] + "/table/" + Ext.getCmp("serverTableStr")["title"].split('/')[1].split('_')[0],
                method:'GET',
                params:{ 

                    'USER_MANAGER':'USER_MANAGER'
                },
                success: function(response) {
                    
                    users = Ext.JSON.decode(response.responseText)["users"]
                    servers = Ext.JSON.decode(response.responseText)["servers"]
                    tables = Ext.JSON.decode(response.responseText)["tables"] 
                    item1 = []
                    for (i in servers){
                        item1.push({text: servers[i], handler: function(e){Ext.getCmp('splitID'+Ext.getCmp("serverTableStr")["title"]).setText(e.text)}})
                    }
                    item2 = []
                    for (i in tables){
                        item2.push({text: tables[i], handler: function(e){Ext.getCmp('splitTB'+Ext.getCmp("serverTableStr")["title"]).setText(e.text)}})
                    }
                    item3 = []
                    for (i in users){
                        item3.push({text: users[i], handler: function(e){Ext.getCmp('splitRB'+Ext.getCmp("serverTableStr")["title"]).setText(e.text)}})
                    }
                    item4 = []
                    for (i in users){
                        item4.push({text: users[i], handler: function(e){Ext.getCmp('splitRBD'+Ext.getCmp("serverTableStr")["title"]).setText(e.text)}})
                    }  

                    //Ext.getCmp('splitID'+Ext.getCmp("serverTableStr")["title"])
                    CreateWindow1 = Ext.getCmp('panelUM'+Ext.getCmp("serverTableStr")["title"])
                    CreateWindow1.removeAll();
                    CreateWindow1.add({
                            fieldLabel: 'Insert User',
                            name: 'first',
                            allowBlank: false
                        },{
                            xtype:'splitbutton',
                            text: 'Select Server',
                            id:'splitID'+Ext.getCmp("serverTableStr")["title"],
                            // handle a click on the button itself
                            menu: new Ext.menu.Menu({
                                items: item1
                            })
                        },{
                            xtype:'splitbutton',
                            text: 'Select Table', 
                            id:'splitTB'+Ext.getCmp("serverTableStr")["title"],   
                            menu: new Ext.menu.Menu({
                                items: item2
                            })
                        },{
                            xtype:'splitbutton',
                            text: 'Select a UserLike',
                            id:'splitRB'+Ext.getCmp("serverTableStr")["title"],   
                            menu: new Ext.menu.Menu({
                                items: item3
                        })
                    })
                    //panel_3.add(dataItems);
                    CreateWindow1.doLayout();

                    CreateWindow2 = Ext.getCmp('panelUD'+Ext.getCmp("serverTableStr")["title"])
                    CreateWindow2.removeAll();
                    CreateWindow2.add({
                            xtype:'splitbutton',
                            text: 'Select a User for Delete',
                            id:'splitRBD'+Ext.getCmp("serverTableStr")["title"],   
                            menu: new Ext.menu.Menu({
                                items: item4
                        })
                    })
                    //panel_3.add(dataItems);
                    CreateWindow2.doLayout();
                    logg('users', item1)
                }
        });

        item1 = [
            // these will render as dropdown menu items when the arrow is clicked:
            {text: 'Item 1', handler: function(e){Ext.getCmp('splitID'+Ext.getCmp("serverTableStr")["title"]).setText(e.text)}},
            {text: 'Item 2', handler: function(e){ Ext.getCmp('splitID'+Ext.getCmp("serverTableStr")["title"]).setText(e.text)}}
        ]
        item2 = [
            // these will render as dropdown menu items when the arrow is clicked:
            {text: 'Item 1', handler: function(e){Ext.getCmp('splitTB'+Ext.getCmp("serverTableStr")["title"]).setText(e.text)}},
            {text: 'Item 2', handler: function(e){ Ext.getCmp('splitTB'+Ext.getCmp("serverTableStr")["title"]).setText(e.text)}}
        ]
        item3 = [
            // these will render as dropdown menu items when the arrow is clicked:
            {text: 'Item 1', handler: function(e){Ext.getCmp('splitRB'+Ext.getCmp("serverTableStr")["title"]).setText(e.text)}},
            {text: 'Item 2', handler: function(e){ Ext.getCmp('splitRB'+Ext.getCmp("serverTableStr")["title"]).setText(e.text)}}
        ]
        item4 = item3;
    },

    CreateWindow:function(){
        return Ext.create('Ext.form.Panel', {
            title: 'Insert Users to MongoDB',
            bodyPadding: 5,
            id:'panelUM'+Ext.getCmp("serverTableStr")["title"],
            height: 300,
            flex:1,
            buttonAlign:'left',
            // The fields
            defaultType: 'textfield',
            items: [{
                fieldLabel: 'Insert User',
                name: 'first',
                allowBlank: false
            },{
                xtype:'splitbutton',
                text: 'Select Server',
                id:'splitID'+Ext.getCmp("serverTableStr")["title"],
                // handle a click on the button itself
                menu: new Ext.menu.Menu({
                    items: item1
                })
            },{
                xtype:'splitbutton',
                text: 'Select Table', 
                id:'splitTB'+Ext.getCmp("serverTableStr")["title"],   
                menu: new Ext.menu.Menu({
                    items: item2
                })
            },{
                xtype:'splitbutton',
                text: 'Select a UserLike',
                id:'splitRB'+Ext.getCmp("serverTableStr")["title"],   
                menu: new Ext.menu.Menu({
                    items: item3
                })
            }],

            // Reset and Submit buttons
            buttons: [{
                text: 'Give Rights (Insert User, Select Server, Table and UserLike)',
                handler: function() {
                    
                    var me = this;
                    logg('k', me.up('form').getForm().findField('first').getValue())
                    logg('a', Ext.getCmp('splitID'+Ext.getCmp("serverTableStr")["title"]).text)
                    logg('b', Ext.getCmp('splitTB'+Ext.getCmp("serverTableStr")["title"]).text)
                    logg('c', Ext.getCmp('splitRB'+Ext.getCmp("serverTableStr")["title"]).text)
                    

                    Ext.Ajax.request({
                            url:  MIMPATH + "/server/" + Ext.getCmp("serverTableStr")["title"].split('/')[0] + "/table/" + Ext.getCmp("serverTableStr")["title"].split('/')[1].split('_')[0],
                            method:'GET',
                            params:{ 

                                USER_MANAGER:'USER_MANAGER',
                                INSERT:'INSERT',
                                user: me.up('form').getForm().findField('first').getValue(),
                                selcServer:Ext.getCmp('splitID'+Ext.getCmp("serverTableStr")["title"]).text,
                                selcTable:Ext.getCmp('splitTB'+Ext.getCmp("serverTableStr")["title"]).text,
                                selcUser: Ext.getCmp('splitRB'+Ext.getCmp("serverTableStr")["title"]).text

                            },
                            success: function(response) {
                                
                                if (Ext.JSON.decode(response.responseText)["conf_bool"]){
              
                                    item3.push({text: me.up('form').getForm().findField('first').getValue(), handler: function(e){Ext.getCmp('splitRBD'+Ext.getCmp("serverTableStr")["title"]).setText(e.text)}})
                                    item3.sort()
                                    item4 = item3
                                    users.push(me.up('form').getForm().findField('first').getValue())
                                    users.sort()
                                    
                                    CreateWindow2 = Ext.getCmp('panelUD'+Ext.getCmp("serverTableStr")["title"])
                                    CreateWindow2.removeAll();
                                    CreateWindow2.add({
                                            xtype:'splitbutton',
                                            text: 'Select a User for Delete',
                                            id:'splitRBD'+Ext.getCmp("serverTableStr")["title"],   
                                            menu: new Ext.menu.Menu({
                                                items: item4
                                        })
                                    })
                                    CreateWindow2.doLayout();

                                    CreateWindow1 = Ext.getCmp('panelUM'+Ext.getCmp("serverTableStr")["title"])
                                    CreateWindow1.removeAll();
                                    CreateWindow1.add({
                                            fieldLabel: 'Insert User',
                                            name: 'first',
                                            allowBlank: false
                                        },{
                                            xtype:'splitbutton',
                                            text: 'Select Server',
                                            id:'splitID'+Ext.getCmp("serverTableStr")["title"],
                                            // handle a click on the button itself
                                            menu: new Ext.menu.Menu({
                                                items: item1
                                            })
                                        },{
                                            xtype:'splitbutton',
                                            text: 'Select Table', 
                                            id:'splitTB'+Ext.getCmp("serverTableStr")["title"],   
                                            menu: new Ext.menu.Menu({
                                                items: item2
                                            })
                                        },{
                                            xtype:'splitbutton',
                                            text: 'Select a UserLike',
                                            id:'splitRB'+Ext.getCmp("serverTableStr")["title"],   
                                            menu: new Ext.menu.Menu({
                                                items: item3
                                        })
                                    })
                                    //panel_3.add(dataItems);
                                    CreateWindow1.doLayout();

                                    Ext.getCmp('splitID'+Ext.getCmp("serverTableStr")["title"]).setText('Select Server')
                                    Ext.getCmp('splitTB'+Ext.getCmp("serverTableStr")["title"]).setText('Select Table')
                                    Ext.getCmp('splitRB'+Ext.getCmp("serverTableStr")["title"]).setText('Select a UserLike')
                                    me.up('form').getForm().reset();
                                }
                                if(Ext.JSON.decode(response.responseText)["conf_bool"]){
                                        color = '<span style="color:green"><b>'
                                    }else{
                                        color = '<span style="color:red"><b>'
                                }
                                Ext.getCmp("STATUS2" + Ext.getCmp("serverTableStr")["title"]).updateText( color + Ext.JSON.decode(response.responseText)["confirmation"] + "</b></span>" )
           
                            }
                    });

                }
            }, '-', {
                text:'Delete Rights (Insert User, Select Server, Table)',
                handler:function(){
                    var me = this;
                    Ext.Ajax.request({
                            url:  MIMPATH + "/server/" + Ext.getCmp("serverTableStr")["title"].split('/')[0] + "/table/" + Ext.getCmp("serverTableStr")["title"].split('/')[1].split('_')[0],
                            method:'GET',
                            params:{ 

                                USER_MANAGER:'USER_MANAGER',
                                DELETEUSER:'DELETEUSER',
                                user: me.up('form').getForm().findField('first').getValue(),
                                selcServer:Ext.getCmp('splitID'+Ext.getCmp("serverTableStr")["title"]).text,
                                selcTable:Ext.getCmp('splitTB'+Ext.getCmp("serverTableStr")["title"]).text,
                                selcUser: Ext.getCmp('splitRB'+Ext.getCmp("serverTableStr")["title"]).text

                            },
                            success: function(response) {
                                
                                if (Ext.JSON.decode(response.responseText)["conf_bool"]){
              
                                    item3.push({text: me.up('form').getForm().findField('first').getValue(), handler: function(e){Ext.getCmp('splitRBD'+Ext.getCmp("serverTableStr")["title"]).setText(e.text)}})
                                    item3.sort()
                                    item4 = item3
                                    users.push(me.up('form').getForm().findField('first').getValue())
                                    users.sort()
                                    
                                    CreateWindow2 = Ext.getCmp('panelUD'+Ext.getCmp("serverTableStr")["title"])
                                    CreateWindow2.removeAll();
                                    CreateWindow2.add({
                                            xtype:'splitbutton',
                                            text: 'Select a User for Delete',
                                            id:'splitRBD'+Ext.getCmp("serverTableStr")["title"],   
                                            menu: new Ext.menu.Menu({
                                                items: item4
                                        })
                                    })
                                    CreateWindow2.doLayout();

                                    CreateWindow1 = Ext.getCmp('panelUM'+Ext.getCmp("serverTableStr")["title"])
                                    CreateWindow1.removeAll();
                                    CreateWindow1.add({
                                            fieldLabel: 'Insert User',
                                            name: 'first',
                                            allowBlank: false
                                        },{
                                            xtype:'splitbutton',
                                            text: 'Select Server',
                                            id:'splitID'+Ext.getCmp("serverTableStr")["title"],
                                            // handle a click on the button itself
                                            menu: new Ext.menu.Menu({
                                                items: item1
                                            })
                                        },{
                                            xtype:'splitbutton',
                                            text: 'Select Table', 
                                            id:'splitTB'+Ext.getCmp("serverTableStr")["title"],   
                                            menu: new Ext.menu.Menu({
                                                items: item2
                                            })
                                        },{
                                            xtype:'splitbutton',
                                            text: 'Select a UserLike',
                                            id:'splitRB'+Ext.getCmp("serverTableStr")["title"],   
                                            menu: new Ext.menu.Menu({
                                                items: item3
                                        })
                                    })
                                    //panel_3.add(dataItems);
                                    CreateWindow1.doLayout();

                                    Ext.getCmp('splitID'+Ext.getCmp("serverTableStr")["title"]).setText('Select Server')
                                    Ext.getCmp('splitTB'+Ext.getCmp("serverTableStr")["title"]).setText('Select Table')
                                    Ext.getCmp('splitRB'+Ext.getCmp("serverTableStr")["title"]).setText('Select a UserLike')
                                    me.up('form').getForm().reset();
                                }
                                if(Ext.JSON.decode(response.responseText)["conf_bool"]){
                                        color = '<span style="color:green"><b>'
                                    }else{
                                        color = '<span style="color:red"><b>'
                                }
                                Ext.getCmp("STATUS2" + Ext.getCmp("serverTableStr")["title"]).updateText( color + Ext.JSON.decode(response.responseText)["confirmation"] + "</b></span>" )
           
                            }
                    });
                }
            }, '-', {
                text:'Give Rights ALL (Insert User and Select UserLike)',
                handler:function(){
                    var me = this;
                    Ext.Ajax.request({
                            url:  MIMPATH + "/server/" + Ext.getCmp("serverTableStr")["title"].split('/')[0] + "/table/" + Ext.getCmp("serverTableStr")["title"].split('/')[1].split('_')[0],
                            method:'GET',
                            params:{ 

                                USER_MANAGER:'USER_MANAGER',
                                ALL:'ALL',
                                user: me.up('form').getForm().findField('first').getValue(),
                                selcServer:Ext.getCmp('splitID'+Ext.getCmp("serverTableStr")["title"]).text,
                                selcTable:Ext.getCmp('splitTB'+Ext.getCmp("serverTableStr")["title"]).text,
                                selcUser: Ext.getCmp('splitRB'+Ext.getCmp("serverTableStr")["title"]).text

                            },
                            success: function(response) {
                                
                                if (Ext.JSON.decode(response.responseText)["conf_bool"]){
              
                                    item3.push({text: me.up('form').getForm().findField('first').getValue(), handler: function(e){Ext.getCmp('splitRBD'+Ext.getCmp("serverTableStr")["title"]).setText(e.text)}})
                                    item3.sort()
                                    item4 = item3
                                    users.push(me.up('form').getForm().findField('first').getValue())
                                    users.sort()
                                    
                                    CreateWindow2 = Ext.getCmp('panelUD'+Ext.getCmp("serverTableStr")["title"])
                                    CreateWindow2.removeAll();
                                    CreateWindow2.add({
                                            xtype:'splitbutton',
                                            text: 'Select a User for Delete',
                                            id:'splitRBD'+Ext.getCmp("serverTableStr")["title"],   
                                            menu: new Ext.menu.Menu({
                                                items: item4
                                        })
                                    })
                                    CreateWindow2.doLayout();

                                    CreateWindow1 = Ext.getCmp('panelUM'+Ext.getCmp("serverTableStr")["title"])
                                    CreateWindow1.removeAll();
                                    CreateWindow1.add({
                                            fieldLabel: 'Insert User',
                                            name: 'first',
                                            allowBlank: false
                                        },{
                                            xtype:'splitbutton',
                                            text: 'Select Server',
                                            id:'splitID'+Ext.getCmp("serverTableStr")["title"],
                                            // handle a click on the button itself
                                            menu: new Ext.menu.Menu({
                                                items: item1
                                            })
                                        },{
                                            xtype:'splitbutton',
                                            text: 'Select Table', 
                                            id:'splitTB'+Ext.getCmp("serverTableStr")["title"],   
                                            menu: new Ext.menu.Menu({
                                                items: item2
                                            })
                                        },{
                                            xtype:'splitbutton',
                                            text: 'Select a UserLike',
                                            id:'splitRB'+Ext.getCmp("serverTableStr")["title"],   
                                            menu: new Ext.menu.Menu({
                                                items: item3
                                        })
                                    })
                                    //panel_3.add(dataItems);
                                    CreateWindow1.doLayout();

                                    Ext.getCmp('splitID'+Ext.getCmp("serverTableStr")["title"]).setText('Select Server')
                                    Ext.getCmp('splitTB'+Ext.getCmp("serverTableStr")["title"]).setText('Select Table')
                                    Ext.getCmp('splitRB'+Ext.getCmp("serverTableStr")["title"]).setText('Select a UserLike')
                                    me.up('form').getForm().reset();
                                }
                                if(Ext.JSON.decode(response.responseText)["conf_bool"]){
                                        color = '<span style="color:green"><b>'
                                    }else{
                                        color = '<span style="color:red"><b>'
                                }
                                Ext.getCmp("STATUS2" + Ext.getCmp("serverTableStr")["title"]).updateText( color + Ext.JSON.decode(response.responseText)["confirmation"] + "</b></span>" )
           
                            }
                    });
                }
            }],
            renderTo: Ext.getBody()
        })

    },

    CreateWindow2:function(){
        return Ext.create('Ext.form.Panel', {
            title: 'Delete all Rights User',
            bodyPadding: 5,
            height: 300,
            flex:1,
            id:'panelUD'+Ext.getCmp("serverTableStr")["title"],
            buttonAlign:'left',
            // The fields
            defaultType: 'textfield',
            items: [{
                xtype:'splitbutton',
                text: 'Select a User for Delete',
                id:'splitRBD'+Ext.getCmp("serverTableStr")["title"],   
                menu: new Ext.menu.Menu({
                    items: item4
                })
            }],

            // Reset and Submit buttons
            buttons: [{
                text: 'Delete User',
                handler: function() {

                    Ext.Ajax.request({
                            url:  MIMPATH + "/server/" + Ext.getCmp("serverTableStr")["title"].split('/')[0] + "/table/" + Ext.getCmp("serverTableStr")["title"].split('/')[1].split('_')[0],
                            method:'GET',
                            params:{ 

                                USER_MANAGER:'USER_MANAGER',
                                DELETE:'DELETE',
                                selcUser: Ext.getCmp('splitRBD'+Ext.getCmp("serverTableStr")["title"]).text

                            },
                            success: function(response) {
                                
                                
                                if (Ext.JSON.decode(response.responseText)["conf_bool"]){
                                    var index = users.indexOf(Ext.getCmp('splitRBD'+Ext.getCmp("serverTableStr")["title"]).text);
                                    logg('index:'+index)
                                    if (index > -1) {
                                        item4.splice(index, 1);
                                        item3.splice(index, 1);
                                    }
                                    item4.sort()
                                    item3.sort()

                                    CreateWindow2 = Ext.getCmp('panelUD'+Ext.getCmp("serverTableStr")["title"])
                                    CreateWindow2.removeAll();
                                    CreateWindow2.add({
                                            xtype:'splitbutton',
                                            text: 'Select a User for Delete',
                                            id:'splitRBD'+Ext.getCmp("serverTableStr")["title"],   
                                            menu: new Ext.menu.Menu({
                                                items: item4
                                        })
                                    })
                                    CreateWindow2.doLayout();

                                    CreateWindow1 = Ext.getCmp('panelUM'+Ext.getCmp("serverTableStr")["title"])
                                    CreateWindow1.removeAll();
                                    CreateWindow1.add({
                                            fieldLabel: 'Insert User',
                                            name: 'first',
                                            allowBlank: false
                                        },{
                                            xtype:'splitbutton',
                                            text: 'Select Server',
                                            id:'splitID'+Ext.getCmp("serverTableStr")["title"],
                                            // handle a click on the button itself
                                            menu: new Ext.menu.Menu({
                                                items: item1
                                            })
                                        },{
                                            xtype:'splitbutton',
                                            text: 'Select Table', 
                                            id:'splitTB'+Ext.getCmp("serverTableStr")["title"],   
                                            menu: new Ext.menu.Menu({
                                                items: item2
                                            })
                                        },{
                                            xtype:'splitbutton',
                                            text: 'Select a UserLike',
                                            id:'splitRB'+Ext.getCmp("serverTableStr")["title"],   
                                            menu: new Ext.menu.Menu({
                                                items: item3
                                        })
                                    })
                                    CreateWindow1.doLayout();
                                    Ext.getCmp('splitRBD'+Ext.getCmp("serverTableStr")["title"]).setText('Select a User for Delete')
                                    if(Ext.JSON.decode(response.responseText)["conf_bool"]){
                                        color = '<span style="color:green"><b>'
                                    }else{
                                        color = '<span style="color:red"><b>'
                                    }
                                    Ext.getCmp("STATUS2" + Ext.getCmp("serverTableStr")["title"]).updateText( color + Ext.JSON.decode(response.responseText)["confirmation"] + "</b></span>" )
                                }

                            }
                    });
                }
            }],
            renderTo: Ext.getBody()
        })

    }      
});