/**
 * @class
 * @extends Ext.
 * @xtype
 * @mixins Gearbox.mixin.ModelInfo,
 * @mixins Gearbox.mixin.Logger
 * @requires
 */
Ext.define('MyDesktop.frameworkCharts.LineChartsFunc', {
	extend: 'Ext.ux.desktop.Module',
	//logLevel: 'debug',

	runcharts: function(store, fieldsObj, names, url_store) {

        Ext.Ajax.request({
            url: url_store,
            success: function(response) {
                gs = Ext.JSON.decode(response.responseText)["chartsDict"];
                logg("chartsDict", gs)
                storegs = gs;
                logg("names", names);


                function replaceKey( fieldsObj, names, gs, storegs) {
                	for (var i = 0; i < gs.length; i++) {
                    	var date = new Date(gs[i]["clock"] * 1e3);
                    	gs[i]["clock"] = Ext.Date.format(date, "G:i a");
                        logg('gslllll '+  JSON.stringify(gs[i]))
	                	for (k in fieldsObj){
	                		if (fieldsObj[k]!='clock'){
		                		logg('k'+fieldsObj)
		                		if (names.length == 1) {
			                		if (names[0] == fieldsObj[k] && names[0] != "firstRun") {
				                		gs[i][fieldsObj[k]] = +storegs[i][names[0]];

				                	}else{
				                		gs[i][fieldsObj[k]] = +-100;
				                	}
				                }
				                if (names.length > 1){
				                	gs[i][fieldsObj[k]] = +storegs[i][fieldsObj[k]];
				                }
				                if (names[0] == "firstRun"){
				                	gs[i][fieldsObj[k]] = +storegs[i][fieldsObj[k]];
				                }
				                if (names.length == 0){
				                	gs[i][fieldsObj[k]] = +-100;
				                }
				            }
	                	}


	                }
	                return gs

                }

                            //var start = new Date().getTime();
                            gs = replaceKey(fieldsObj, names, gs, storegs);
                            //var end = new Date().getTime();
                            //var time = end - start;
                            //Ext.Msg.alert('Execution time: ' + time);


                store.loadData(gs);
            }
        });

		//this.debug(arguments);
	},
    seriesGen: function(keyObject) {
        var dictAppend = [];
        for (i in keyObject) dictAppend.push({

            type: "line",
            highlight: {
                size: 7,
                radius: 7
            },
            showMarkers: false,
            axis: "left",
            smooth: true,
            xField: "clock",
            yField: keyObject[i],
            /*
            tips: {
                trackMouse: true,
                width: 140,
                height: 28,
                renderer: function(storeItem, item) {
                    this.setTitle(
                    	storeItem.get(keyObject[i]) + ' ' + keyObject[i]
                    );
                }
            },
            markerConfig: {
                type: "circle",
                size: 4,
                radius: 4,
                "stroke-width": 0
            }*/
        });
        return dictAppend;
    }

});
