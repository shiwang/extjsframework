var children;

var serverName;

var tables;

var count = 0;

var MIMPATH;

Ext.define("MyDesktop.frameworkCharts.CheckTree", {
    extend: "Ext.tree.Panel",
    requires: [ "Ext.data.Store", "Ext.data.TreeStore" ],
    xtype: "checktree_custom",
    rootVisible: false,
    useArrows: true,
    width: 250,
    height: 300,
    initComponent: function() {

        Ext.define("Model", {
            extend: "Ext.data.Model",
            fields: [ {
                name: "text",
                type: "string"
            }, {
                name: "leaf",
                type: "boolean"
            }, {
                name: "checked",
                type: "boolean"
            }, {
                name: "expanded",
                type: "boolean"
            } ]
        });
        var store_tree = Ext.create("Ext.data.TreeStore", {
            model: "Model",
            scope: this,
            proxy: {
                headers: {
                    "X-CSRFToken": Ext.select("meta[name='csrf-token']").elements[0].getAttribute("content")
                },
                type: "rest",
                url: MIMPATH + "/charts/"
            }
        });
        Ext.apply(this, {
            store: store_tree,
            columns: [ {
                xtype: "treecolumn",
                text: "Charts Menu",
                flex: 2,
                sortable: true,
                dataIndex: "text"
            } ]
        });
        this.callParent();
        count += 1;
    }
});
