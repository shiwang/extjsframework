var url_store;

var gs;

var MIMPATH;

var serverName;

var tables;

var children;

// var keyObject = [ "upcalls", "ringcalls", "totalcalls", "chs", "acs", "ep", "rud", "sf", "viu" ];

// var fieldsObj = [ "clock", "upcalls", "ringcalls", "totalcalls", "chs", "acs", "ep", "rud", "sf", "viu" ];

 var keyObject = [ "upcalls"];

 var fieldsObj = [ "clock", "upcalls" ];

var timesecond = 10;

var names = []

var destroy_val=[false]

Ext.define("MyDesktop.frameworkCharts.LineCharts", {
    extend: "Ext.ux.desktop.Module",
    requires: [ "Ext.chart.*", "Ext.Window", "Ext.fx.target.Sprite", "Ext.layout.container.Fit", "Ext.window.MessageBox", "Ext.tree.Panel", "MyDesktop.frameworkCharts.CheckTree", "Ext.button", "Ext.JSON", "Ext.Msg", "Ext.Ajax" ],
    //id: "linecharts",
    init: function() {
        this.launcher = {
            text: "charts",
            iconCls: "notepad"
        };
        MIMPATH = this.MIMPATH;
        serverName = this.serverName;
        tables = this.tables;
        lcf = new MyDesktop.frameworkCharts.LineChartsFunc()

        var url = ['nothing']
        var store = Ext.create("Ext.data.JsonStore", {
            fields: fieldsObj,
            data: Ext.Ajax.request({
                url: MIMPATH + "/server/" + serverName + "/table/" + tables + "/zabbix?option=data",
                success: function(response) {
                    url_store = response.request.options.url;
                    gs = Ext.JSON.decode(response.responseText)["chartsDict"];
                    storegs = gs;
                    logg("ich bin here");

                    for (var i = 0; i < gs.length; i++) {
                        var date = new Date(gs[i]["clock"] * 1e3);
                        gs[i]["clock"] = Ext.Date.format(date, "G:i a");
                        for (k in fieldsObj){
                            if (fieldsObj[k]!='clock'){
                                gs[i][fieldsObj[k]] = +storegs[i][fieldsObj[k]];
                            }
                        }
                    }
                    return gs;
                },
                failure: function() {
                    Ext.Msg.show({
                        title: "Store Load Callback",
                        msg: "zServer is off, please contact adminstrator",
                        icon: Ext.Msg.INFO,
                        buttons: Ext.Msg.OK
                    });
                }
            })
        });

        var chart = Ext.create("Ext.chart.Chart", {
            id: "charts",
            style: "background:#fff",
            animate: false,
            store: store,
            shadow: false,
            //theme: "DailyStockChart",
            theme: "Category1",
            legend: {
                position: "right"
            },
            axes: [ {
                type: "Numeric",
                minimum: 0,
                position: "left",
                fields: keyObject,
                title: "Number of calls",
                minorTickSteps: 1,
                grid: {
                    odd: {
                        opacity: 1,
                        fill: "#ddd",
                        stroke: "#bbb",
                        "stroke-width": .5
                    }
                }
            }, {
                type: "Category",
                position: "bottom",
                fields: "clock",
                dateFormat: "ga",
                title: "Time",
                constrain: true
            } ],
            series: lcf.seriesGen(keyObject)
        });

        function looping_function(destroy_val, url_store, setBoolArr){
            var ix = setBoolArr.indexOf(false);
            var rsintv = setInterval(function() {

                var destroy_val_n = [true];
                var destroy_val_60= [true];
                var destroy_val_120= [true];
                var destroy_val_360 = [true];
                var destroy_val_720 = [true];
                var destroy_val_1440 = [true];

                if(ix==0){
                    destroy_val_n[0] = setBoolArr[0]
                    var inval = destroy_val_n[0]
                }else if (ix==1){
                    destroy_val_60[0] = setBoolArr[1]
                    var inval = destroy_val_60[0]
                }else if (ix==2){
                    destroy_val_120[0] = setBoolArr[2]
                    var inval = destroy_val_120[0]

                }else if(ix==3){
                    destroy_val_360[0] = setBoolArr[3]
                    var inval = destroy_val_360[0]

                }else if(ix==4){
                    destroy_val_720[0] = setBoolArr[4]
                    var inval = destroy_val_720[0]

                }else if(ix==5){
                    destroy_val_1440[0] = setBoolArr[5]
                    var inval = destroy_val_1440[0]

                }
                if(inval || destroy_val){
                    logg('destroy_valkkkkkkkk')
                    clearInterval(rsintv);
                }


                gs = lcf.runcharts(store, fieldsObj, tmp, url_store)
                //runchart(tmp);
            }, 1e3 * timesecond);
        }

        Ext.create("Ext.Window", {
            title: "Charts of " + this.serverName,
            width: 1e3,
            height: 600,
            hidden: false,
            autoShow: true,
            layout: "fit",
            border: true,
            animCollapse: false,
            constrainHeader: true,
            headerPosition: "top",
            listeners: {
                destroy: function() {
                    logg('destroyed')
                        destroy_val[0] = true
                }
            },
            items: [ {
                xtype: "panel",
                border: false,
                layout: {
                    type: "hbox",
                    align: "stretch"
                },
                items: [ {
                    html: "top, with no title",
                    width: 800,
                    minHeight: 400,
                    minWidth: 550,
                    hidden: false,
                    maximizable: true,
                    renderTo: Ext.getBody(),
                    layout: "fit",
                    tbar: [{
                        text: "Save Chart",
                        handler: function() {
                            Ext.MessageBox.confirm("Confirm Download", "Would you like to download the chart as an image?", function(choice) {
                                if (choice == "yes") {
                                    chart.save({
                                        type: "image/png"
                                    });
                                }
                            });
                        }
                    },{
                        text: "Normal",
                        handler: function() {
                            url_store = url_store+"&url_values=20"
                            lcf.runcharts(store, fieldsObj, names, url_store)

                            //looping_function(destroy_val[0], url_store, [false, true, true, true, true, true])

                            /*
                            var rsintv = setInterval(function() {

                                var destroy_val_n = [false];
                                var destroy_val_60= [true];
                                var destroy_val_120= [true];
                                var destroy_val_360 = [true];
                                var destroy_val_720 = [true];
                                var destroy_val_1440 = [true];
                                if(destroy_val_n[0]){
                                    logg('destroy_valkkkkkkkk')
                                    clearInterval(rsintv);
                                }
                                gs = lcf.runcharts(store, fieldsObj, tmp, url_store)
                                //runchart(tmp);
                            }, 1e3 * timesecond);

                            if(destroy_val[0]){
                                logg('destroy_valkkkkkkkk')
                                clearInterval(rsintv);
                            }*/

                        }
                    },{
                        text: "1h",
                        handler: function() {
                            url_store = url_store+"&url_values=60"
                            lcf.runcharts(store, fieldsObj, names, url_store)

                            /*var rsintv = setInterval(function() {

                                var destroy_val_n = [true];
                                var destroy_val_60= [false];
                                var destroy_val_120= [true];
                                var destroy_val_360 = [true];
                                var destroy_val_720 = [true];
                                var destroy_val_1440 = [true];
                                if(destroy_val_60[0]){
                                    logg('destroy_valkkkkkkkk')
                                    clearInterval(rsintv);
                                }
                                gs = lcf.runcharts(store, fieldsObj, tmp, url_store)
                                //runchart(tmp);
                            }, 1e3 * timesecond);
                            if(destroy_val[0]){
                                logg('destroy_valkkkkkkkk')
                                clearInterval(rsintv);
                            }*/

                        }
                    },{
                        text: "2h",
                        handler: function() {
                            url_store = url_store+"&url_values=120"
                            lcf.runcharts(store, fieldsObj, names, url_store)

                            /*var rsintv = setInterval(function() {

                                var destroy_val_n = [true];
                                var destroy_val_60= [true];
                                var destroy_val_120= [false];
                                var destroy_val_360 = [true];
                                var destroy_val_720 = [true];
                                var destroy_val_1440 = [true];
                                if(destroy_val_120[0]){
                                    logg('destroy_valkkkkkkkk')
                                    clearInterval(rsintv);
                                }
                                gs = lcf.runcharts(store, fieldsObj, tmp, url_store)
                                //runchart(tmp);
                            }, 1e3 * timesecond);
                            if(destroy_val[0]){
                                logg('destroy_valkkkkkkkk')
                                clearInterval(rsintv);
                            }*/

                        }
                    },{
                        text: "6h",
                        handler: function() {
                            url_store = url_store+"&url_values=360"
                            lcf.runcharts(store, fieldsObj, names, url_store)

                            /*var rsintv = setInterval(function() {

                                var destroy_val_n = [true];
                                var destroy_val_60= [true];
                                var destroy_val_120= [true];
                                var destroy_val_360 = [false];
                                var destroy_val_720 = [true];
                                var destroy_val_1440 = [true];
                                if(destroy_val_360[0]){
                                    logg('destroy_valkkkkkkkk')
                                    clearInterval(rsintv);
                                }
                                gs = lcf.runcharts(store, fieldsObj, tmp, url_store)
                                //runchart(tmp);
                            }, 1e3 * timesecond);
                            if(destroy_val[0]){
                                logg('destroy_valkkkkkkkk')
                                clearInterval(rsintv);
                            }*/
                        }
                    },{
                        text: "12h",
                        handler: function() {
                            url_store = url_store+"&url_values=720"
                            lcf.runcharts(store, fieldsObj, names, url_store)


                            /*var rsintv = setInterval(function() {

                                var destroy_val_n = [true];
                                var destroy_val_60= [true];
                                var destroy_val_120= [true];
                                var destroy_val_360 = [true];
                                var destroy_val_720 = [false];
                                var destroy_val_1440 = [true];
                                if(destroy_val_720[0]){
                                    logg('destroy_valkkkkkkkk')
                                    clearInterval(rsintv);
                                }
                                gs = lcf.runcharts(store, fieldsObj, tmp, url_store)
                                //runchart(tmp);
                            }, 1e3 * timesecond);
                            if(destroy_val[0]){
                                logg('destroy_valkkkkkkkk')
                                clearInterval(rsintv);
                            }*/

                        }
                    },{
                        text: "24h",
                        handler: function() {
                            url_store = url_store+"&url_values=1440"
                            lcf.runcharts(store, fieldsObj, names, url_store)



                            /*var rsintv = setInterval(function() {

                                var destroy_val_n = [true];
                                var destroy_val_60= [false];
                                var destroy_val_120= [true];
                                var destroy_val_360 = [true];
                                var destroy_val_720 = [true];
                                var destroy_val_1440 = [false];
                                if(destroy_val_1440[0]){
                                    logg('destroy_valkkkkkkkk')
                                    clearInterval(rsintv);
                                }
                                gs = lcf.runcharts(store, fieldsObj, tmp, url_store)
                                //runchart(tmp);
                            }, 1e3 * timesecond);
                            if(destroy_val[0]){
                                logg('destroy_valkkkkkkkk')
                                clearInterval(rsintv);
                            }*/
                        }
                    }],
                    items: [ chart ],
                    margin: "0 0 5 0"
                }, {
                    xtype: "panel",
                    items: [ {
                        id: "CheckTree",
                        xtype: "checktree",
                        listeners: {
                            checkchange: function(node, rec) {
                                var records = this.getView().getChecked();

                                Ext.Array.each(records, function(rec) {
                                    if(rec.get("text").match("server")==null){
                                        names.push(rec.get("text"));
                                    }
                                });
                                lcf.runcharts(store, fieldsObj, names, url_store)

                                //Ext.getCmp('charts')['series'] = seriesGen(keyObject)
                                //runchart(names);
                                tmp = names;


                                console.log(rsintv)



                                INDICATOR = false;
                            }
                        }
                    } ],
                    flex: 1
                } ]
            } ]
        });


    }
});
