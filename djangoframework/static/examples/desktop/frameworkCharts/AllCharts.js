


if(document.URL.split("//")[0] =="http:"){
    var port = /8000/;
    if (document.URL.match(port) != null) {
        var MIMPATH = "/work";
        MIMPATH = "http://" + document.URL.split("//")[1].split('/')[0] + MIMPATH

    } else {

        var MIMPATH = "/en/work";
        MIMPATH = "http://" + document.URL.split("//")[1].split('/')[0] + MIMPATH

    }
}else{
    var port = /8444/;
    if (document.URL.match(port) != null) {
        var MIMPATH = "/work";
        MIMPATH = "https://" + document.URL.split("//")[1].split('/')[0] + MIMPATH
    } else {
        var MIMPATH = "/en/work";
        MIMPATH = "https://" + document.URL.split("//")[1].split('/')[0] + MIMPATH
    }
}

function nurl1(MIMPATH, server, graphName){
	return [
		MIMPATH,
		'/charts/data?',
		'server='+ server,
		'&graphName='+ graphName
	].join('')
}
function nurl2(stime, period, zoom){
	return [
		'&period='+ period,
		'&stime=' + stime,
		'&zoom='+ zoom
	].join('')

}
var htmlChecktree;
var valSelected;
var dateExist=false;
var dateValue;
var ifDateTimeSelected = false;

Ext.define("MyDesktop.frameworkCharts.AllCharts", {
    extend: "Ext.ux.desktop.Module",
    requires: [ "Ext.form.field.HtmlEditor"],
    id: "allcharts",
    init: function() {
        this.launcher = {
            text: "Charts",
            iconCls: "charts"
        };
    },
    createWindow:function(){
    	try{
	        var desktop = this.app.getDesktop();
	        var win = desktop.getWindow("allcharts");
	        if (!win) {
		    	var me = this
		    	win = desktop.createWindow( {
		            title: "Charts",
	                width: 800,
	                height: 700,
	                animCollapse: false,
	                border: false,
	                maximized:true,
	                hideMode: "offsets",
	                layout: {
	                    type: "hbox",
	                    align: "stretch"
                	},
		            listeners: {
		                destroy: function() {
		                    logg('destroyed')
		                }
		            },
		            items:[ {
		                flex: 4,
		                xtype: "container",
		                layout: {
		                    type: "vbox",
		                    align: "stretch"
		                },
		                items: [ me.tBar(me),
		                {
		                	flex:5,
		                	id:'html_id',
		                	xtype:'panel',
		                	html:""
		                } ]
		            }, {
		                flex: 1,
		                xtype: "container",
		                layout: {
		                    type: "vbox",
		                    align: "stretch"
		                },
		                items: [me.checkTree()]
		            }]
		        });
		    }
		    return win
    	}catch (e){
    		logg('FEHLERRRRR createWindow', e)
    	}
    },

    dateField: function(){
		return Ext.create('Ext.form.Panel', {
		    renderTo: Ext.getBody(),
		    width:300,
		    items: [{
		        xtype: 'datefield',
		        anchor: '100%',
		        fieldLabel: 'From',
		        id:"dateFieldButt",
		        name: 'from_date',
		        maxValue: new Date(),  // limited to the current date or prior
		        listeners:{
		        	select:function( field, value, eOpts ){
		        		//logg("checked:", Ext.getCmp("checkboxButt"))
		        		Ext.getCmp("checkboxButt").setValue("checked", false)
		        		//logg("seconds " + Math.round(seconds))
				    	try{
				    		if(valSelected){
				    			dateExist = true
				    			ifDateTimeSelected = true
								dateValue = new Date(value);
								logg("dateValue " + dateValue)
						    }else{
						    	Ext.Msg.alert('message', "please select one option from server list")
						    }
				    	}catch (e){
				    		logg('FEHLERRRRR', e)
				    	}
		        	}
		        }
		    },{
		        xtype: 'timefield',
		        fieldLabel: 'Time In',
		        minValue: '0:00 AM',
		        maxValue: '8:00 PM',
		        increment: 30,
		        id:"timefieldButt",
		        anchor: '100%',
	        	listeners:{
		        	select:function( field, value, eOpts ){
		        		//logg("seconds " + Mund(seconds))
		        		logg("timefield field ", field)
		        		//Ext.getCmp("checkboxButt").set("checked", "false")
				    	try{
				    		if(valSelected){
				    			dateExist = true
				    			ifDateTimeSelected = true
				    			logg("new date " + new Date())
				    			var val = new Date(dateValue)
				    			val.setHours(value.getHours(), value.getMinutes(), value.getSeconds(),value.getMilliseconds());

								dateValue = val
								logg("timefield " + dateValue)
						    }else{
						    	Ext.Msg.alert('message', "please select one option from server list")
						    }
				    	}catch (e){
				    		logg('FEHLERRRRR', e)
				    	}
		        	}
	        	}
    		}]
		});
	},

    charts:function(){
    	try{
	        return Ext.create("Ext.chart.Chart", {
	            id: "charts",
	            style: "background:#fff",
	            animate: false,
	            store: store,
	            shadow: false,
	            //theme: "DailyStockChart",
	            theme: "Category1",
	            legend: {
	                position: "right"
	            },
	            axes: [ {
	                type: "Numeric",
	                minimum: 0,
	                position: "left",
	                fields: keyObject,
	                title: "Number of calls",
	                minorTickSteps: 1,
	                grid: {
	                    odd: {
	                        opacity: 1,
	                        fill: "#ddd",
	                        stroke: "#bbb",
	                        "stroke-width": .5
	                    }
	                }
	            }, {
	                type: "Category",
	                position: "bottom",
	                fields: "clock",
	                dateFormat: "ga",
	                title: "Time",
	                constrain: true
	            } ],
	            //series: lcf.seriesGen(keyObject)
	        });
    	}catch (e){
    		logg('FEHLERRRRR charts', e)
    	}
    },
    showChart:function(value){
    	try{

    		if(valSelected){


    			Ext.getCmp('html_id').update("")
    			logg('htmlChecktree-' + htmlChecktree)
    			var timeStamp = Math.round(dateValue/1000)
    			if (value.match(/d/)!=null){
    				if(1){
    					period = value.match(/\d+/)*3600*24
    					stime = timeStamp - period
    					var htmlPeriodZoom = nurl2(String(stime), String(period) , '2')
    				}

    			}else{
    				if(1){
    					period = value.match(/\d+/)*3600
    					stime = timeStamp - period
    					var htmlPeriodZoom = nurl2(String(stime), String(period) , '2')

    				}
    			}

    			logg('htmlPeriodZoom-' + htmlPeriodZoom)
    			logg('url-' + htmlChecktree+htmlPeriodZoom)
				logg("DHDHDHD", value.match(/d/))
				logg("IMAGEG URl : " + "<img src='" + htmlChecktree+htmlPeriodZoom + "' width=100% height=100%>")
				Ext.getCmp('html_id').update("<img src='" + htmlChecktree+htmlPeriodZoom + "' width=100% height=80%>")

				dateExist = false


		    	//logg("baseHtml",  outHtml)
		    }else{
		    	Ext.Msg.alert('message', "please select one option from server list")
		    }
    	}catch (e){
    		logg('FEHLERRRRR', e)
    	}
    },

    tBar:function(me){
    	try{

    		return {
    			xtype:"panel",
    			flex:1,
    			scope:this,
    			items:[	me.dateField()],
    			tbar:[{
				    text: "Save Chart",
				    xtype:"button",
				    handler: function() {
				        Ext.MessageBox.confirm("Confirm Download", "Would you like to download the chart as an image?", function(choice) {
				            if (choice == "yes") {
				                me.chart.save({
				                    type: "image/png"
				                });
				            }
				        });
				    }
				},'-',{
				    text: "1h",
				    xtype:"button",
				    handler: function(){

				    	me.showChart("1h")
				    }
				},'-',{
				    text: "2h",
				    xtype:"button",
				    handler: function(){
				    	me.showChart("2h")
				    }
				},'-',{
				    text: "3h",
				    xtype:"button",
				    handler: function(){
				    	me.showChart("3h")
					}
				},'-',{
				    text: "6h",
				    xtype:"button",
				    handler: function(){
				    	me.showChart("6h")
				    }
				},'-',{
				    text: "12h",
				    xtype:"button",
				    handler: function(){
				    	me.showChart("12h")
				    }
				},'-',{
				    text: "1d",
				    xtype:"button",
				    handler: function(){
				    	me.showChart("1d")
				    }
				},'-',{
				    text: "3d",
				    xtype:"button",
				    handler: function(){
				    	me.showChart("3d")
				    }
				},'-',{
				    text: "7d",
				    xtype:"button",
				    handler: function(){
				    	me.showChart("7d")
				    }
				},'-',{
				    text: "14d",
				    xtype:"button",
				    handler: function(){
				    	me.showChart("14d")
				    }

				},'-',{
					location:"bottom",
                    boxLabel  : 'Now',
                    xtype: "checkbox",
                    inputValue: true,
                    id:"checkboxButt",
                    listeners:{
                    	change:function(newValue, oldValue, eOpts){
                    		if(newValue['lastValue']){
                    			dateValue = new Date();
                    			Ext.getCmp("dateFieldButt").setValue("rawValue", "")
                    			logg("new value is ", Ext.getCmp("timefieldButt"))
                    			Ext.getCmp("timefieldButt").setValue("")

                    		}

                    	}
                    }
                }

				/*{
					width:300,
					fieldLabel: 'From',
					xtype:'datetimefield',
			        listeners:{
			        	select:function( field, value, eOpts ){
			        		//logg("seconds " + Math.round(seconds))
					    	try{
					    		if(valSelected){
					    			dateExist = true
									dateValue = new Date(value);
									logg("dateValue " + dateValue)
					    			//logg('htmlPeriodZoom-' + htmlPeriodZoom)
					    			//logg('url-' + htmlChecktree+htmlPeriodZoom)
									//logg("IMAGEG URl : " + "<img src='" + htmlChecktree+htmlPeriodZoom + "' width=100% hight=100%>")
									//Ext.getCmp('html_id').update("<img src='" + htmlChecktree+htmlPeriodZoom + "' width=100% hight=100%>")
							    }else{
							    	Ext.Msg.alert('message', "please select one option from server list")
							    }
					    	}catch (e){
					    		logg('FEHLERRRRR', e)
					    	}
			        	}
			        }
				}*/

				]
    		}

    	}catch (e){
    		logg('FEHLERRRRR', e)
    	}
    },

    checkTree:function(){
    	try{
			return {

	            id: "CheckTree3",
	            flex:1,
	            xtype: "checktree_custom",
	            listeners: {
	                checkchange: function(node, rec) {

	                	Ext.getCmp('html_id').update("")

	                	if(node.data["checked"]){
	                		valSelected = true
	                	}
	                    var records = this.getView().getChecked();

	                    //orgstr = "<img src='https://mimastel.com:8444/work/charts/data/?graphName=total_calls&server=server_8&zoom=2&period=7200' width=100% hight=100%>"
	                    //logg('records', records)
	                    logg('node', node)
	                    //logg('rec',rec)
                    	for (i in records){
                    		//records[i].set('parentId',records[i].raw['parentId'])
                    		if (records[i]==node){

                    			//records[i].set('checked',true);
                    			if (records[i].data['parentId'] != "root"){

                    				htmlChecktree = nurl1(MIMPATH, records[i].parentNode.data['text'], records[i].data['text'])
				                    //outHtml = baseHtml.replace('graphNameVal', records[i].data['text'])
				                    //outHtml = outHtml.replace('serverVal', records[i].parentNode.data['text'])
                    				//Ext.Msg.alert('message', [records[i].parentNode.data['text'],records[i].data['text']].join(','))
                    			}

                    		}else{
                    			valSelected[0] = false
                    			records[i].set('checked',false);
                    		}
                    	}
	                    //Ext.Msg.alert('message', rec.get("text"))
	                    logg('node:',node)

	                    /*Ext.Array.each(records, function(rec) {

	                    	for (i in records){
	                    		records[i].set('parentId',records[i].raw['parentId'])
	                    		if (records[i]==rec){
	                    			records[i].set('checked',true);
	                    			Ext.Msg.alert('message', rec.get("text"))
	                    		}else{
	                    			records[i].set('checked',false);
	                    		}
	                    	}

	                    	//if (!rec.get('checked')){
	                    	//	rec.set('checked',false);
	                    	//}

	                    	logg('rec:',rec)

	                        //if(rec.get("text").match("server")==null){
	                        //	Ext.Msg.alert('message', rec.get("text"))
	                            //names.push(rec.get("text"));
	                        //}
	                    });*/
	                    //lcf.runcharts(store, fieldsObj, names, url_store)

	                    //Ext.getCmp('charts')['series'] = seriesGen(keyObject)
	                    //runchart(names);
	                }
	            }
			}

    	}catch (e){
    		logg('FEHLERRRRR', e)
    	}
    }






})

