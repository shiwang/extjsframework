Ext.Loader.setConfig({
    enabled: true
});

Ext.Loader.setPath("Ext.ux", "../ux");

Ext.require([ "Ext.form.*", "Ext.KeyNav", "*", "Ext.ux.DataTip" ]);

Ext.onReady(function() {
    Ext.QuickTips.init();
    var bd = Ext.getBody();
    if(document.URL.split("//")[0] =="http:"){
        var port = /8000/
    }else{
        var port = /8444/
    }
    if (document.URL.match(port) != null) {
        var MIMPATH = "/work";
    } else {
        var MIMPATH = "/en/work";
    }
    var required = '<span style="color:red;font-weight:bold" data-qtip="Required">*</span>';
    var simple = Ext.widget({
        xtype: "form",
        layout: "form",
        collapsible: true,
        id: "loginForm",
        x: 200,
        y: 200,
        url: MIMPATH + "/auth/",
        headers: {
            "X-CSRFToken": Ext.select("meta[name='csrf-token']").elements[0].getAttribute("content")
        },
        frame: true,
        title: "Login",
        bodyPadding: "5 5 0",
        width: 350,
        fieldDefaults: {
            msgTarget: "side",
            labelWidth: 75
        },
        plugins: {
            ptype: "datatip"
        },
        defaultType: "textfield",
        items: [ {
            fieldLabel: "Username",
            afterterLabelTextTpl: required,
            name: "username_name",
            allowBlank: false,
            tooltip: "Enter your username"
        }, {
            fieldLabel: "Password",
            afterLabelTextTpl: required,
            name: "password_name",
            inputType: "password",
            listeners: {
                specialkey: function(field, e) {
                    var usr = simple.getForm("form").findField("username_name").getValue();
                    var pass = simple.getForm("form").findField("password_name").getValue();
                    if (usr.length > 0 && pass.length > 0) {
                        if (e.getKey() == e.ENTER) {
                            Ext.Ajax.request({
                                url: MIMPATH + "/auth/",
                                success: function() {
                                    if (simple.getForm("form").findField("username_name").getValue() == "administrator") {
                                        window.location.href = MIMPATH + "/adminPage/";
                                    } else {
                                        window.location.href = MIMPATH + "/desktop";
                                    }
                                },
                                failure: function() {
                                    Ext.Msg.alert("Status", "Username or Password is wrong, please try again");
                                },
                                headers: {
                                    "X-CSRF-Token": Ext.select("meta[name='csrf-token']").elements[0].getAttribute("content")
                                },
                                params: {
                                    username_name: this.up("form").getForm().findField("username_name").getValue(),
                                    password_name: this.up("form").getForm().findField("password_name").getValue(),
                                    csrfmiddlewaretoken: Ext.select("meta[name='csrf-token']").elements[0].getAttribute("content")
                                }
                            });
                        }
                    }
                }
            },
            allowBlank: false,
            tooltip: "Enter your Password"
        }, {
            name: "csrfmiddlewaretoken",
            fieldLabel: "csrf",
            afterLabelTextTpl: required,
            hidden: true,
            value: Ext.select("meta[name='csrf-token']").elements[0].getAttribute("content")
        } ],
        buttons: [ {
            text: "Login",
            xtype: "button",
            handler: function() {
                Ext.Ajax.request({
                    url: MIMPATH + "/auth/",
                    success: function() {
                        Ext.log({
                            level: "debug"
                        }, "debug message");
                        if (simple.getForm("form").findField("username_name").getValue() == "administrator") {
                            window.location.href = MIMPATH + "/adminPage/";
                        } else {
                            window.location.href = MIMPATH + "/desktop";
                        }
                    },
                    failure: function() {
                        Ext.Msg.alert("Status", "Username or Password is wrong, please try again");
                    },
                    headers: {
                        "X-CSRF-Token": Ext.select("meta[name='csrf-token']").elements[0].getAttribute("content")
                    },
                    params: {
                        username_name: this.up("form").getForm().findField("username_name").getValue(),
                        password_name: this.up("form").getForm().findField("password_name").getValue(),
                        csrfmiddlewaretoken: Ext.select("meta[name='csrf-token']").elements[0].getAttribute("content")
                    }
                });
            }
        }, {
            text: "Forget Password!",
            handler: function() {
                Ext.Msg.alert("Status", "Please contact to your administrator");
            }
        } ]
    });
    var mainPanel = new Ext.Viewport({
        frame: false,
        layout: {
            type: "vbox",
            align: "center",
            pack: "center"
        },
        items: [ simple ]
    });
});